var version = document.currentScript.getAttribute("attr-cache-version");

function loadCss(url) {
    var link = document.createElement("link");
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = url;
    document.getElementsByTagName("head")[0].appendChild(link);
}

loadCss("/css/app.css?v=" + version);
loadCss("/css/bootstrap.min.css?v=" + version);
loadCss("https://pro.fontawesome.com/releases/v5.10.0/css/all.css");
loadCss("/js/swiper/swiper-bundle.min.css");
loadCss("/js/jquery.fancybox/dist/jquery.fancybox.min.css");
loadCss("/js/materialize/css/materialize.min.css");
// loadCss("/js/bootstrap-5.1.3-dist/css/bootstrap.css")
// loadCss("/js/isotope-docs/css/isotope-docs.css");


requirejs.config({
    waitSeconds: 200, paths: {
        functions: "/js/functions.js?v=" + version,
        jquery: "/js/jquery/dist/jquery.min.js?v=" + version,
        fancybox: "/js/jquery.fancybox/dist/jquery.fancybox.min.js?v=" + version,
        swipejs: "/js/swiper/swiper-bundle.min.js?v=" + version,
        canvasjs: "/js/canvasjs/canvasjs.min.js?v=" + version,
        materialize: "/js/materialize/css/materialize.min.js?v=" + version,
        isotope: "/js/isotope-docs/js/isotope-docs.min.js?v=" + version,
        // bootstrap: "js/bootstrap-5.1.3-dist/js/bootstrap.bundle.js?v=" + version,
    },

    shim: {
        functions: {
            deps: ["jquery", "fancybox"],
        }, fancybox: {
            deps: ["jquery"],
        },

        swipejs: {
            deps: ["jquery"],
        },

        canvasjs: {
            deps: ["jquery"],
        },

        materialize: {
            deps: ["jquery"],
        },

        isotope: {
            deps: ["jquery"],
        },

    },
});


//Define dependencies and pass a callback when dependencies have been loaded
require(["jquery"], function ($) {
    jQuery.event.special.touchstart = {
        setup: function (_, ns, handle) {
            this.addEventListener("touchstart", handle, {passive: true});
        },
    };

    $('form input[type="submit"],form button[type="submit"], form button').on('click', function () {

        $(this).parents('form').addClass('recheck')
    });

    $("#toTop").click(function () {
        $("html, body").animate({scrollTop: 0}, 0);
    });

});

require(["functions"], function () {

    window.addEventListener("scroll", InitializeMenuScroll);
    InitializeMenuScroll();

    try {
        NotificationFunction();
    } catch (e) {
        console.log(e);
    }

    try {
        modals();
    } catch (e) {
        console.log(e);
    }

    try {
        burgermenu();
    } catch (e) {
        console.log(e);
    }

    try {
        mobilemenu();
    } catch (e) {
        console.log(e);
    }

    // try {
    //     isoptope();
    // } catch (e) {
    //     console.log(e);
    // }


});


require(["swipejs"], function (Swiper) {

    var swiper_products = new Swiper(".swiper-slideshow", {
        // Optional parameters
        direction: "horizontal", slidesPerView: 1, loop: false, spaceBetween: 0, breakpoints: {
            1199: {
                slidesPerView: 1, spaceBetween: 0,
            },
        }, pagination: {
            el: ".products-pagination", clickable: true,
        }, navigation: {
            nextEl: ".next-product", prevEl: ".prev-product",
        }, grabCursor: true,
    });

});


require(["fancybox"], function () {
    $(".fancybox").fancybox({
        helpers: {
            title: "HELLO", thumbs: {
                width: 50, height: 50,
            },
        },
    });
    $(".fancybox-media").fancybox({
        openEffect: "none", closeEffect: "none", helpers: {
            media: {},
        },
    });

    $.fancyConfirm = function (opts) {
        opts = $.extend(true, {
            title: "Are you sure?", message: "", okButton: "OK", noButton: "Cancel", callback: $.noop,
        }, opts || {});

        $.fancybox.open({
            type: "html",
            src: '<div class="fc-content">' + "<h3>" + opts.title + "</h3>" + "<p>" + opts.message + "</p>" + '<div class="text-right confirmation">' + '<a data-value="0" data-fancybox-close>' + opts.noButton + "</a>" + '<button data-value="1" data-fancybox-close class="rounded">' + opts.okButton + "</button>" + "</div>" + "</div>",
            opts: {
                animationDuration: 350,
                animationEffect: "material",
                modal: true,
                baseTpl: '<div class="fancybox-container fc-container" role="dialog" tabindex="-1">' + '<div class="fancybox-bg"></div>' + '<div class="fancybox-inner">' + '<div class="fancybox-stage"></div>' + "</div>" + "</div>",
                afterClose: function (instance, current, e) {
                    var button = e ? e.target || e.currentTarget : null;
                    var value = button ? $(button).data("value") : 0;

                    opts.callback(value);
                },
            },
        });
    };
});

// require(["materialize"], function () {
//     $('.modal').modal();
// });


require(["canvasjs"], function (CanvasJS) {
    var clients = $("#keyfiguresclients").val();
    var clients_dec = JSON.parse(clients);
    var dbs = $("#keyfiguresdbs").val();
    var dbs_dec = JSON.parse(dbs);
    var users = $("#keyfiguresusers").val();
    var users_dec = JSON.parse(users);
    // console.log(clients_dec[0]);

    var chart = new CanvasJS.Chart("keyfigures", {
        animationEnabled: true,
        // title:{
        //     text: "Crude Oil Reserves vs Production, 2016"
        // },
        axisY: {
            // title: "Billions of Barrels",
            titleFontWeight: "normal",
            titleFontColor: "#00000059",
            lineColor: "#00000059",
            labelFontColor: "#00000059",
            tickColor: "#00000059",
            gridColor: "#00000059"
        },
        // axisY2: {
        //     title: "Millions of Barrels/day",
        //     titleFontColor: "#C0504E",
        //     lineColor: "#C0504E",
        //     labelFontColor: "#C0504E",
        //     tickColor: "#C0504E"
        // },
        interactivityEnabled: true,
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeries
        },
        data: [{
            type: "column",
            name: "Clients",
            legendText: "Clients",
            showInLegend: true,
            color: "#147AD6",
            dataPoints: [
                // for(i=0; i<=clients.length;i++)
                {label: clients_dec[0].year, y: parseInt(clients_dec[0].quantity)},
                {label: clients_dec[1].year, y: parseInt(clients_dec[1].quantity)},
                {label: clients_dec[2].year, y: parseInt(clients_dec[2].quantity)},
                {label: clients_dec[3].year, y: parseInt(clients_dec[3].quantity)},
                {label: clients_dec[4].year, y: parseInt(clients_dec[4].quantity)},
                // {label: "2018", y: 150},
                // {label: "2019", y: 250},
                // {label: "2020", y: 300},
                // {label: "2021", y: 1000},
            ]
        },
            {
                type: "column",
                name: "DataBases",
                legendText: "DBs",
                color: "#37353599",
                showInLegend: true,
                dataPoints: [
                    {label: dbs_dec[0].year, y: parseInt(dbs_dec[0].quantity)},
                    {label: dbs_dec[1].year, y: parseInt(dbs_dec[1].quantity)},
                    {label: dbs_dec[2].year, y: parseInt(dbs_dec[2].quantity)},
                    {label: dbs_dec[3].year, y: parseInt(dbs_dec[3].quantity)},
                    {label: dbs_dec[4].year, y: parseInt(dbs_dec[4].quantity)},
                    // {label: "2018", y: 220},
                    // {label: "2019", y: 300},
                    // {label: "2020", y: 400},
                    // {label: "2021", y: 500},
                ]
            },
            {
                type: "column",
                name: "Users",
                legendText: "Users",
                color: "#000000",
                // axisYType: "secondary",
                showInLegend: true,
                dataPoints: [
                    {label: users_dec[0].year, y: parseInt(users_dec[0].quantity)},
                    {label: users_dec[1].year, y: parseInt(users_dec[1].quantity)},
                    {label: users_dec[2].year, y: parseInt(users_dec[2].quantity)},
                    {label: users_dec[3].year, y: parseInt(users_dec[3].quantity)},
                    {label: users_dec[4].year, y: parseInt(users_dec[4].quantity)},
                    // {label: "2018", y: 500},
                    // {label: "2019", y: 1000},
                    // {label: "2020", y: 2000},
                    // {label: "2021", y: 3500},
                ]
            }]
    });
    chart.render();

    function toggleDataSeries(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        chart.render();
    }
});


document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, options);
});
var collapsibleElem = document.querySelector('.collapsible');
var collapsibleInstance = M.Collapsible.init(collapsibleElem, options);
