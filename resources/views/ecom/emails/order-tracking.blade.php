<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

</head>

<?php

$order = App\Purchase::where('id', $order_id)->where('cancelled',0)->first();
//$user = App\User::find($order->users_id);



$logo_url = '';
?>

<body style="font-family: arial,sans-serif; padding:0; margin:0; font-family:'Raleway',sans-serif">
    <div style="background-color:#f2f2f2">
        <div
            style="background-color:#fff; box-shadow:0 0 10px #EEE; width:100%; max-width:600px; padding:40px; margin:auto; overflow:hidden;">


            <table style="border-collapse: collapse; width:100%; margin-bottom:20px">
                <tr>
                    <td style="padding:10px 0px ; font-size:26px ; font-weight:bolder ; text-align:left ">
                        <img src="{{$logo_url}}" alt="" />
                    </td>
                    <td
                        style="padding:10px 0px ; font-size:20px ;font-family:'Raleway',sans-serif; font-weight:bolder ; text-align:right ">
                        ORDER #{{$order->id}}
                    </td>

                </tr>

            </table>




            <table style="width:100%; border-collapse: collapse;  margin-bottom:50px">

                <tr>
                    <td style="padding:10px 0px ; font-size:20px ; font-weight:bolder "> NEW ORDER WAYBILL! </td>
                </tr>
                <tr>
                    <td style="padding:10px 0px ; font-size:12px ; font-weight:bolder "> Dear Admin,
                    </td>
                </tr>
                <tr>
                    <td style="padding:10px 0px ; font-size:12px ">
                        Please find Attached the order Waybill.
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>

</html>