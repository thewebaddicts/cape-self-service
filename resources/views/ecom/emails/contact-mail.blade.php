@component('mail::message')
# Contact Mail

Received the following mail:

<strong>Name:</strong>
{{$data['first_name']}} {{$data['last_name']}}
<strong>E-Mail:</strong>
{{$data['email']}}
<strong>Query:</strong>
{{$data['query']}}

@endcomponent
