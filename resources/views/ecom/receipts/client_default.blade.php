<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

</head>

<style type="text/css">
    @media print {
        body {
            -webkit-print-color-adjust: exact;
        }
    }
</style>

<body style="font-family: arial,sans-serif; padding:0px; margin:0; font-family:'Raleway',sans-serif">

<?php

    $first_name =$user['first_name']." ".$user['last_name'];

     $items ='';
     foreach ($data['items']['items']['items'] as $item){
         $item_row = view('ecom.components.receipts.item-card',['item'=>$item]);

         $items .= $item_row;
     }

     $checkout_details ='';
     foreach ($data["items"]['breakdown'] as $key => $value){
         $detail_row =  view('ecom.components.receipts.checkout-details',['key'=>$key ,'value'=>$value]);

         $checkout_details.= $detail_row;
     }


     $order_summary ="<table style='text-align: left;width: 80%;border-collapse: collapse; margin: auto;'>
           <tr class='bottom-border-line' style='border-bottom: 1px solid #E9E9E9'>
                <th class='label t-md' style='padding: 20px;'>".__('cart.order_summary')."</th>
            </tr>
                ".$items.$checkout_details."
            <tr class='top-border-line' style=' border-top: 1px solid #E9E9E9;'>
                <th colspan='2' class='row-label' style='padding: 20px; font-size: 16px;font-weight: 600;'>".__('checkout.shipping_method')."</th>
            </tr>
            <tr>
                <th class='row-sublabel' style='padding: 0 20px 20px 20px ;'>".$data['ecom_stores_delivery_details']['label']."</th>
            </tr>
            <tr>
                <th class='row-sublabel' style='padding: 20px'>".__('checkout.delivery_date')."</th>
            </tr>
            <tr>
                <th class='row-desc' style='padding: 0 20px 20px 20px ;font-weight: 400;'>".$data['date_delivery']."</th>
            </tr>
             <tr>
                <th class='row-sublabel' style='padding: 20px'>".__('checkout.delivery_note')."</th>
            </tr>
            <tr>
                <th class='row-desc'  style='padding: 0 20px 20px 20px ;font-weight: 400;'>".$data['delivery_note']."</th>
            </tr>
            <tr >
                <th colspan='2' class='row-label' style='padding: 20px; font-size: 16px;font-weight: 600;'>".__('checkout.shipping_address')."</th>
            </tr>
            <tr>
                <th colspan='2' class='row-sublabel' style='padding: 0 20px;'>".$data['shipping_address_details']['label']."</th>
            </tr>
            <tr>
                <th colspan='2'>
                <div style='padding: 0 20px ;font-weight: 400;'>".$data['shipping_address_details']['floor']." ".$data['shipping_address_details']['building']." ".$data['shipping_address_details']['street']."</div>
                <div style='padding: 0 20px ;font-weight: 400;'>".$data['shipping_address_details']['state']." ".$data['shipping_address_details']['city']."</div>
                <div style='padding: 0 20px ;font-weight: 400;'>".$data['shipping_address_details']['country']['name']."</div>
                </th>
            </tr>
            <tr >
                <th colspan='2'  class='row-label' style='padding: 20px; font-size: 16px;font-weight: 600;'>".__('checkout.billing_address')."</th>
            </tr>
            <tr >
                <th colspan='2' class='row-sublabel' style='padding: 0 20px 20px 20px ;'>".$data['billing_address_details']['label']."</th>
            </tr>
            <tr>
                <th colspan='2' class='row-desc'>
                <div style='padding: 0 20px ;font-weight: 400;'>".$data['billing_address_details']['floor']." ".$data['billing_address_details']['building']." ".$data['billing_address_details']['street']."</div>
                <div style='padding: 0 20px ;font-weight: 400;'>".$data['billing_address_details']['state']." ".$data['billing_address_details']['city']."</div>
                <div style='padding: 0 20px ;font-weight: 400;'>".$data['billing_address_details']['country']['name']."</div>
                </th>
            </tr>
            <tr class='top-border-line'>
                <th colspan='2' class='row-label' style='padding: 20px; font-size: 16px;font-weight: 600;'>".__('checkout.payment_method')."</th>
            </tr>
            <tr>
                <th class='row-sublabel' style='padding: 0 20px 20px 20px'>".$data['ecom_payment_methods_details']['label']."</th>
            </tr>
            <tr class='top-border-line'>
                <th colspan='2' class='row-label' style='padding: 20px; font-size: 16px;font-weight: 600;'>".__('checkout.discount_code')."</th>
            </tr>
            <tr>
                <th class='row-desc' style='padding: 0 20px 20px 20px'>".$data['items']['voucher_response']."</th>
            </tr></table>";

    $order_summary= str_replace("\n","",$order_summary);

    $dictionary = [
        "first_name"=>$first_name,
        "order_summary"=>$order_summary,
    ];

    foreach ($dictionary AS $key=>$value){
        $Search[]   =   "*".$key."*";
        $Replace[]  =   $value;
    }

    $email = str_replace( $Search, $Replace,$template->email_form );


?>

<style>
    .template-footer{
        width: 100%;
        background-color: black;
        color: white;
        padding: 20px;
        margin-top: 40px;
    }
</style>

<div style="background-color:#fff;  width:100%; max-width:750px; padding: 0 40px; margin:auto; overflow:hidden;">
    {!! nl2br($email) !!}
</div>

</body>

</html>
