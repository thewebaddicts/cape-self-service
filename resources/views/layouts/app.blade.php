

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="locale" content="{{ app()->getLocale() }}">

{{--    <meta name="description"--}}
{{--          content="Run Business One Cloud with Cloudtaktiks Managed Cloud and benefit from Best Price, Best Performance , Best Security and Best Support. Get up and running with SAP Business One Login Access within hours. No minimum users required. Powered by Azure and optimized for SAP Business One, Cloudtaktiks is the cloud choice for SAP Business One HANA and SQL clients and partners Globally.">--}}
{{--    <meta name="keywords"--}}
{{--          content="Sap business one cloud, Azure cloud systems, Sap B1 hana, Sap B1 SQL, Optimized cloud for sap, Sap business one and TSPlus, SLES, Suse Linux systems, Microsoft Azure, O365.">--}}
{{--    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>--}}
{{--    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"> </script>--}}
    {{-- <meta name="description" --}}
    {{-- content="Find your study buddy and join a collaborative community of peers to connect, communicate and exchange knowledge within a safe and dedicated learning space."> --}}
    {{-- <meta name="keywords" --}}
    {{-- content="learning support,collaborative learning,elearning solutions,e learning tools,collaborative classroom,e learning program,lms elearning,peer to peer learning,e learning system,e learning online,tutoring apps,collaborative learning strategies,e learning websites,personalised learning,e learning sites,e learning in education,online tutor app,personalized learning,electronic learning,best e learning platform,collaborative teaching,e learning companies,cooperative and collaborative,e education,studying together,cooperative learning examples,math tutor app,enterprise e learning,e learning services,nimble elearning"> --}}

    {{-- <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/assets/favicon/apple-touch-icon-57x57.png" /> --}}
    {{-- <link rel="apple-touch-icon-precomposed" sizes="114x114" --}}
    {{-- href="/assets/favicon/apple-touch-icon-114x114.png" /> --}}
    {{-- <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/assets/favicon/apple-touch-icon-72x72.png" /> --}}
    {{-- <link rel="apple-touch-icon-precomposed" sizes="144x144" --}}
    {{-- href="/assets/favicon/apple-touch-icon-144x144.png" /> --}}
    {{-- <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/assets/favicon/apple-touch-icon-60x60.png" /> --}}
    {{-- <link rel="apple-touch-icon-precomposed" sizes="120x120" --}}
    {{-- href="/assets/favicon/apple-touch-icon-120x120.png" /> --}}
    {{-- <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/assets/favicon/apple-touch-icon-76x76.png" /> --}}
    {{-- <link rel="apple-touch-icon-precomposed" sizes="152x152" --}}
    {{-- href="/assets/favicon/apple-touch-icon-152x152.png" /> --}}
    {{-- <link rel="icon" type="image/png" href="/assets/favicon/favicon-196x196.png" sizes="196x196" /> --}}
    {{-- <link rel="icon" type="image/png" href="/assets/favicon/favicon-96x96.png" sizes="96x96" /> --}}
    {{-- <link rel="icon" type="image/png" href="/assets/favicon/favicon-32x32.png" sizes="32x32" /> --}}
    {{-- <link rel="icon" type="image/png" href="/assets/favicon/favicon-16x16.png" sizes="16x16" /> --}}
    {{-- <link rel="icon" type="image/png" href="/assets/favicon/favicon-128.png" sizes="128x128" /> --}}
    {{-- <meta name="application-name" content="{{ env('APP_NAME') }}" /> --}}
    {{-- <meta name="msapplication-TileColor" content="#FFFFFF" /> --}}
    {{-- <meta name="msapplication-TileImage" content="/assets/favicon/mstile-144x144.png" /> --}}
    {{-- <meta name="msapplication-square70x70logo" content="/assets/favicon/mstile-70x70.png" /> --}}
    {{-- <meta name="msapplication-square150x150logo" content="/assets/favicon/mstile-150x150.png" /> --}}
    {{-- <meta name="msapplication-wide310x150logo" content="/assets/favicon/mstile-310x150.png" /> --}}
    {{-- <meta name="msapplication-square310x310logo" content="/assets/favicon/mstile-310x310.png" /> --}}


    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
{{--    <link--}}
{{--        href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"--}}
{{--        rel="stylesheet">--}}

    <title>Cape Self Service</title>


    <style>
        .common-loader-initial {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            width: 100vw;
            height: 100vh;
            margin: auto;
            background-color: white;
            z-index: 9999;
             background-image: url("{{ env('APP_URL') }}/assets/gifs/loader3.gif");
            background-position: center;
            background-repeat: no-repeat;
            background-size: 500px;
            display: block;
        }

        /* body,
        html {
            margin: 0;
            padding: 0;
            max-width: 100%;
            overflow-x: hidden;
        } */

    </style>

    @yield('title')

</head>


<body>



<input type="hidden" id="publicpath" value="{{env('APP_URL')}}">

    <div class="common-loader-initial common-loader-hide"></div>

    @yield('content')
{{--    @include("components.footer")--}}

</body>

<script language="javascript" src="/js/require.js?v={{ env('CACHE_VERSION') }}"></script>
<script language="javascript" src="/js/init.js?v={{ env('CACHE_VERSION') }}"
attr-cache-version="{{ env('CACHE_VERSION', NOW()) }}" attr-lang="{{ app()->getLocale() }}"></script>

@yield('scripts')
<script language="javascript">
    function NotificationFunction() {
        @if (request()->input('notification'))
            ShowMessage("", "{{ request()->input('notification') }}");
            removeQueryNotificationString();
        @endif
        @if (request()->input('notification_title') && request()->input('notification_message'))
            ShowMessage("{{ request()->input('notification_title') }}", "{{ request()->input('notification_message') }}");
            removeQueryNotificationString();
        @endif
        @if (request()->input('notification_id'))
            <?php $notification = \App\Models\NotificationModel::getNotification(request()->input('notification_id'));9
            ?>
            ShowMessage("{{ $notification->title_en }}", "{{ $notification->notification_en }}");
            removeQueryNotificationString();
        @endif
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY')}}" async defer type="text/javascript" data-pagespeed-orig-index="4"></script>

</html>
