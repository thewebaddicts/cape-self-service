<div class="cookiespopup" id="cookiespopup">
    <div class="popup">
        <div class="popupcontent">
            <div class="popuptext">
                <div class="popuptitle">
                    Cookies Policy
                </div>
                <div class="popuptxt">
                    We use cookies to enhance your CloudTaktiks experience; they help us to analyse our website performance, to
                    enhance and customise content and for ads. By continuing to use our site, clicking "Yes, I understand" or
                    closing this window, you consent to our use of cookies. <a href="{{route('cookies')}}">LEARN MORE</a>
                </div>
            </div>
            <div class="popupbtn">
                <a id="acceptcookiebtn">YES, I Understand</a>
            </div>
        </div>
    </div>
</div>
