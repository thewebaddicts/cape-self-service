{{--@php--}}
{{--    $information = App\Http\Controllers\PersonelInformationController->getPersonalInformation();--}}
{{--@endphp--}}
@php
$loggeduserid = session('login')->id;
    $information = App\Models\PersonalInformationModel::where('cancelled', 0)->where('id',$loggeduserid)->first();
@endphp
<div class="topbar">
    <div class="topbarcontentdiv">
        <div class="topbarcontent">
            <div class="notificationdiv">
                <div class="notification">
                <img src="assets/imgs/svg/notification_icon.svg" alt="">
                </div>
            </div>
            <div class="profilediv">
                <div class="profileicondiv">
                    <div class="profileicon">
                        <img src="assets/imgs/svg/profile_icon.svg" alt="">
                    </div>
                </div>
                <div class="profileinfo">
                    <div class="profilename">
                        {{$information->first_name}}
                    </div>
                    <div class="profilemail">
                        {{$information->email}}
                    </div>
                </div>
{{--                <div class="arrowdiv">--}}
{{--                    <div class="arrow">--}}
{{--                        @include('components.arrow_down')--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </div>
    </div>
</div>
