
@php
    $contact = App\Models\FooterModel::where('cancelled', 0)->first();
@endphp

<div class="footer">
    <div class="footercontent">
        <div class="footercolumn">
            <div class="footercolumntitle">
                Site Map
            </div>
            <a href="{{route('about')}}" class="footercolumncontent hoverthis">
                About Us
            </a>
            <a href="{{route('cloudofferings')}}" class="footercolumncontent hoverthis">
                Cloud Offerings
            </a>
            <a href="{{route('customers')}}" class="footercolumncontent hoverthis">
                Customers
            </a>
            <a href="{{route('contact')}}" class="footercolumncontent hoverthis">
                Contact Us
            </a>
        </div>
        <div class="footercolumn">
            <a href="{{route('products')}}" class="footercolumntitle hoverthis">
                Our Products
            </a>
            <div class="footercolumncontent hoverthis">
                Suze Linux
            </div>
            <div class="footercolumncontent hoverthis">
                Citrix
            </div>
            <div class="footercolumncontent hoverthis">
                Cisco Meraki Firewall
            </div>
            <div class="footercolumncontent hoverthis">
                Office 365
            </div>
            <div class="footercolumncontent hoverthis">
                Anti Virus
            </div>
        </div>
        <div class="footercolumn">
            <a href="{{route('services')}}" class="footercolumntitle hoverthis">
                Our services
            </a>
            <div class="footercolumncontent hoverthis">
                SAP B1
            </div>
            <div class="footercolumncontent hoverthis">
                Office 365 Setup
            </div>
            <div class="footercolumncontent hoverthis">
                Linux Suse setup and optimization
            </div>
            <div class="footercolumncontent hoverthis">
                Infrastructure SLA
            </div>
        </div>
        <div class="footercolumn">
            <a href="{{route('contact')}}" class="footercolumntitle hoverthis">
                Contact us
            </a>
            <a href = "mailto: sales@cloudtaktiks.com" class="footercolumncontent hoverthis">
                <img src="assets/imgs/svg/mail_icon.svg" alt=""> {{$contact->contact_us_email}}
            </a>
            <div class="footercolumncontent">
                <div class="withpoint">
                    <div class="point"></div>
                    <div class="withpointtxt">
                        Evagorou, 31 Evagoras Complex, 2nd Floor, Flat/Office 24 1066, Nicosia, Cyprus
                    </div>
                </div>
                <a href="tel:+34-689-872-690" class="footercolumncontentnumb hoverthis">
                    <img src="assets/imgs/svg/phone_icon.svg" alt="">
                    +34 689 872 690
                </a>
            </div>
            <div class="footercolumncontent">
                <div class="withpoint">
                    <div class="point"></div>
                    <div class="withpointtxt">
                        Justinian St, Clemenceau Justinian Center, 9th Floor Beirut, Lebanon
                    </div>
                </div>
                <a href="tel:+961-81-935-241"  class="footercolumncontentnumb hoverthis">
                    <img src="assets/imgs/svg/phone_icon.svg" alt="">
                    +961 81 935 241
                </a>
            </div>
            <hr style="border-color: #FFFFFF0F; width: 100%;">
            <div class="followus">
                <div class="followustxt">
                    Follow us
                </div>
                <div class="followusicon">
                    <a href="{{$contact->contact_us_linkedin}}">
                        <img src="assets/imgs/linkedin-logo.png" alt="">
                    </a>
                </div>
{{--                <div class="followusicon">--}}
{{--                    <img src="assets/imgs/svg/fb_icon.svg" alt="">--}}
{{--                </div>--}}
            </div>
        </div>
    </div>
    <div class="underfooter">
        <div class="terms">
            <a href="{{route('terms')}}" class="termsitem hoverthis">
                Terms & Condition
            </a>
            <a href="{{route('privacy')}}" class="termsitem hoverthis">
                Privacy Policy
            </a>
            <a href="{{route('cookies')}}" class="termsitem hoverthis">
                Cookies Policy
            </a>
        </div>
        <div class="copyright">
            ©️ {{ date('Y') }} Cloud Taktiks - Web Design and Development by <a href="https://thewebaddicts.com/"> The Web Addicts</a>
        </div>
    </div>
</div>
