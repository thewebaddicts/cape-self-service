<div class="topnav">
    {{--   <div class="content-fluid">--}}
    <div class="navbar">
        <a href="{{route('home')}}" class="logodiv">
            <img src="assets/imgs/logowhite.png" class="topnavwhitelogo" style="color:white" alt="CLOUDTAKTIKS LOGO">
            <img src="assets/imgs/logoblack.png" class="topnavblacklogo" style="display:none" alt="CLOUDTAKTIKS LOGO">
        </a>
        <div class="navcontent">
            <div class="navitems">

                <div class="navitem">
                    <a href="{{route('about')}}" class="innave ">
                        About us
                    </a>
                </div>

                <div class="navitem">
                    <a class="innave" href="{{route('cloudofferings')}}">
                        Cloud Offerings
                    </a>
                </div>

                <div class="navitem ourproductsnavitem ">
                    <a href='{{route('products')}}' class="innave ">Our Products
                        <div class="navitemarrow notvisible">
                            @include("components.arrow_down")
                        </div>
                    </a>
                    <div class="dropdowncontent" id="ourproductsdropdown">
                        <a href="{{route('citrix')}}" class="dropdownitem">
                            Citrix
                        </a>
                        <div class="dropdownitem">
                            Product2
                        </div>
                    </div>
                </div>

                <div class="navitem ourservicesnavitem ">
                    <a href="{{route('services')}}" class="innave ">Our Services
                        <div class="navitemarrow notvisible">
                            @include("components.arrow_down")
                        </div>
                    </a>
                    <div class="dropdowncontent" id="ourservicesdropdown">
                        <div class="dropdownitem">
                            Service1
                        </div>
                        <div class="dropdownitem">
                            Service2
                        </div>
                    </div>
                </div>
                <div class="navitem">
                    <a class="innave" href="{{route('customers')}}">
                        Customers
                    </a>
                </div>
                <div class="navitem">
                    <a class="innave" href="http://support.cloudtaktiks.com/">
                        Support
                    </a>
                </div>
                <a href="{{route('quote')}}" class="navbtn">Get Started</a>
            </div>
        </div>
        <div class="mobilenavmenubtn" style="display: none">
            @include('components.menu_icon')
        </div>
    </div>
    @include('components.mobile_navbar');
    {{--        </div>--}}
</div>
