<div class="mobilenavbarbox">
    <div class="mobilenavbar">
        <div class="mobilenavcontent">
            <div class="mobilenavlink">
                <a href="{{route('about')}}" class="innave ">
                    About us
                </a>
            </div>

            <div class="mobilenavlink">
                <a class="innave" href="{{route('cloudofferings')}}">
                    Cloud Offerings
                </a>
            </div>

            <div class="mobilenavlink">
                <a href='{{route('products')}}' class="innave ">Our Products
                    <div class="navitemarrow notvisible">
                        @include("components.arrow_down")
                    </div>
                </a>
{{--                <div class="dropdowncontent" id="ourproductsdropdown">--}}
{{--                    <a href="{{route('citrix')}}" class="dropdownitem">--}}
{{--                        Citrix--}}
{{--                    </a>--}}
{{--                    <div class="dropdownitem">--}}
{{--                        Product2--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>

            <div class="mobilenavlink">
                <a href="{{route('services')}}" class="innave ">Our Services
                    <div class="navitemarrow notvisible">
                        @include("components.arrow_down")
                    </div>
                </a>
{{--                <div class="dropdowncontent" id="ourservicesdropdown">--}}
{{--                    <div class="dropdownitem">--}}
{{--                        Service1--}}
{{--                    </div>--}}
{{--                    <div class="dropdownitem">--}}
{{--                        Service2--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
            <div class="mobilenavlink">
                <a class="innave" href="{{route('customers')}}">
                    Customers
                </a>
            </div>
            <div class="mobilenavlink">
                <a class="innave" href="http://support.cloudtaktiks.com/">
                    Support
                </a>
            </div>
            <a href="{{route('quote')}}" class="navbtn">Get Started</a>
        </div>
    </div>
</div>
