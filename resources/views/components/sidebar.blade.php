<div class="sidebar">
    <div class="sidebarcontent">
        <div class="toplogodiv">
            <div class="logodiv">
            <img src="assets/imgs/svg/cape_white_logo.svg" alt="">
            </div>
        </div>
        <div class="bottomsidecontent">
            <div class="sidecontent">
                <div class="sideitem">
                    <a href="{{route('dashboard')}}" class="item">
                        <div class="itemicon">
                            <img src="assets/imgs/svg/home_icon.svg" alt="">
                        </div>
                        <div class="itemtxt">
                            Dashboard
                        </div>
                    </a>
                </div>
                <div class="sideitem">
                    <a href="{{route('policies')}}" class="item">
                        <div class="itemicon">
                            <img src="assets/imgs/svg/policies_icon.svg" alt="">
                        </div>
                        <div class="itemtxt">
                            My policies
                        </div>
                    </a>
                </div>
                <div class="sideitem">
                    <a href="{{route('address')}}" class="item">
                        <div class="itemicon">
                            <img src="assets/imgs/svg/addresses_icon.svg" alt="">
                        </div>
                        <div class="itemtxt">
                            My addresses
                        </div>
                    </a>
                </div>
                <div class="sideitem">
                    <a href="{{route('account')}}" class="item">
                        <div class="itemicon">
                            <img src="assets/imgs/svg/account_icon.svg" alt="">
                        </div>
                        <div class="itemtxt">
                            My account
                        </div>
                    </a>
                </div>
                <div class="sideitem">
                    <a href="{{route('logout')}}" class="item">
                        <div class="itemicon">
                            <img src="assets/imgs/svg/logout_icon.svg" alt="">
                        </div>
                        <div class="itemtxt">
                            Logout
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="menu-btn" style="display:none">
    <div class="menu-btn__burger">
    </div>
</div>
