

@extends('layouts.app')
@section('title')
    <title>HomePage - {{ env('WEBSITE_NAME') }}</title>
@endsection

@section('content')
    <div class="pagecontent">
        @include('components.sidebar')

        <div class="pagerightside">
            @include('components.topbar')

            <div class="rightsidepagecontent">
                <div class="pagetitle">
                    <div class="title">
                        Dashboard
                    </div>
                </div>
                <div class="dashboardpagecontent">
                    <div class="leftside">
                        <div class="leftsiderow">
                            <div class="widebox">
                                <div class="widediv">
                                    <div class="widecontent">
                                        <div class="title">
                                            Total Due Amount
                                        </div>
                                        <div class="amount">
                                            100.25 USD
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="squarebox">
                                <div class="squarediv">
                                    <div class="squarecontent">
                                        <div class="amount">
                                            50
                                        </div>
                                        <div class="title">
                                            Active Policies
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="leftsiderow">
                            <div class="squarebox">
                                <div class="squarediv">
                                    <div class="squarecontent">
                                        <div class="amount">
                                            60
                                        </div>
                                        <div class="title">
                                            Policies Expires
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="widebox">
                                <div class="widediv">
                                    <div class="widecontent">
                                        <div class="title">
                                            Total Due Amount
                                        </div>
                                        <div class="amount">
                                            100.25 USD
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="rightside">
                        <div class="tablebox">
                            <div class="tablediv">
                                <div class="tablecontent">
                                    <div class="toppart">
                                        <div class="expiredpolicies">
                                            {{count($policies)}} Policies Expired
                                        </div>
                                        <a href="{{route('policies')}}" class="seemorebtn">
                                            <div class="txt">
                                                See More
                                            </div>
                                            <div class="iconbox">
                                                <img src="assets/imgs/svg/see_more_arrow.svg" alt="">
                                            </div>
                                        </a>
                                    </div>
                                    <div class="table">
                                        @foreach($policies as $policie)
                                            <div class="tablerow">
                                                <div class="rowcontent">
                                                    <div class="number">
                                                        {{$policie->policy_nb}} - {{$policie->endorsement}}
                                                    </div>
                                                    <div class="amount">
                                                        {{$policie->paid_premium}} USD
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
