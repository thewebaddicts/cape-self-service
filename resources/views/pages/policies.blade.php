@extends('layouts.app')
@section('title')
    <title>HomePage - {{ env('WEBSITE_NAME') }}</title>
@endsection

@section('content')
    <div class="pagecontent">
        @include('components.sidebar')

        <div class="pagerightside">
            @include('components.topbar')

            <div class="rightsidepagecontent">
                <div class="pagetitle">
                    <div class="title">
                        My Policies
                    </div>
                </div>
                <div class="policiespagecontent">
                    <table class="policiestable">
                        <thead class="policiesthead">
                            <th>Policy Number</th>
                            <th>Insurance Date</th>
                            <th>Due Date</th>
                            <th>Policy Type</th>
                            <th>Status</th>
                            <th style="text-align:center">Actions</th>
                        </thead>
                        <tbody class="policiesbody">
                        @foreach($policies as $policie)
                            <tr class="policiestablerow">
                                <td>{{$policie->policy_nb}}</td>
                                <td>{{$policie->issuance}}</td>
                                <td>{{$policie->expiry}}</td>
                                <td>{{$policie->type}}</td>
                                <td>{{$policie->o_s}}</td>
                                <td class="actionbtns">
                                    <a data-id="{{$policie->id}}" class="saveicon">
                                        <img src="assets/imgs/svg/save1.svg" alt="">
                                    </a>
                                    <a data-id="{{$policie->id}}" class="editicon">
                                        <img src="assets/imgs/svg/edit1.svg" alt="">
                                    </a>
                                        <button type="submit"  data-id="{{$policie->id}}" class="dlticon">
                                            <img src="assets/imgs/svg/dlt1.svg" alt="">
                                        </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Structure -->
    <div id="deletepoliciemodal" class="modal">
        <form method="POST" action="{{route('policies_dlt')}}">
            @csrf
            <input type="hidden" name="id" value="{{$policie->id}}">
            <div class="modal-content">
                <h4>Delete Policy</h4>
                <p>Are you sure you want to delete this policy?</p>
            </div>
            <div class="modal-footer">
                <a onclick="document.getElementById('deletepoliciemodal').style.display = 'none'"
                   class="modal-close waves-effect waves-green btn-flat">No</a>
                <button type="submit" class="modal-close waves-effect waves-green btn-flat">Yes</button>
            </div>
        </form>
    </div>

@endsection
