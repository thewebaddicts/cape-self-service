@extends('layouts.app')
@section('title')
    <title>HomePage - {{ env('WEBSITE_NAME') }}</title>
@endsection

@section('content')
    <div class="pagecontent">
        @include('components.sidebar')

        <div class="pagerightside">
            @include('components.topbar')

            <div class="rightsidepagecontent">
                <div class="pagetitle">
                    <div class="title">
                        Addresses
                    </div>
                </div>
                <div class="addresspagecontent">
                    <div class="addresses">
                        @foreach($addresses as $address)
                            <div class="addressbox">
                                <div class="addresscontent">
                                    <input type="hidden" name="id" value="{{$address->id}}">
                                    <div class="title">
                                        {{$address->address_name}}
                                    </div>
                                    <div class="info">
                                        <div class="name">
                                            {{$information->first_name}} {{$information->last_name}}
                                        </div>
                                        <div class="address">
                                            {{$address->state}}, {{$address->city}}, {{$address->street}}
                                            , {{$address->building}}, floor
                                            number {{$address->floor}}, {{$address->state}}, {{$address->postal_code}}
                                        </div>
                                    </div>
                                    <div class="btns">
                                        {{--                                        <form method="POST" action="{{route('address_post')}}">--}}

                                        <button type="submit" class="editbtn">
                                            <img src="assets/imgs/svg/edit_icon.svg"
                                                 alt="">
                                            Edit
                                        </button>
                                        <button type="button" class="dltbtn modal-trigger">
                                            <img src="assets/imgs/svg/dlt_icon.svg"
                                                 alt="">
                                            Delete
                                        </button>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Structure -->
    <div id="deleteaddressmodal" class="modal">
        <form method="POST" action="{{route('address_dlt')}}">
            @csrf
            <input value="{{$address->id}}" name="deleteaddress" type="hidden">
            <div class="modal-content">
                <h4>Delete Address</h4>
                <p>Are you sure you want to delete this address?</p>
            </div>
            <div class="modal-footer">
                <a onclick="document.getElementById('deleteaddressmodal').style.display = 'none'"
                   class="modal-close waves-effect waves-green btn-flat">No</a>
                <button type="submit" class="modal-close waves-effect waves-green btn-flat">Yes</button>
            </div>
        </form>
    </div>
@endsection
