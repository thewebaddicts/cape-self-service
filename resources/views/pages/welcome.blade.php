@extends('layouts.app')
@section('title')
    <title>HomePage - {{ env('WEBSITE_NAME') }}</title>
@endsection
@section('content')

{{--    @include('components.navbar')--}}

    {{--    @include('components.topwhitenav')--}}
<div class="loginpage">
    <div class="loginpagediv" style="background-image:url('/assets/imgs/loginpage_background.png')">
        <div class="sidedwhitediv"></div>
        <div class="loginpagecontent">
            <form method="POST" action="{{route('loginto')}}" class="logincontent">
                @csrf
                <div class="logintitle">
                    <div class="title">
                        You are required to Login
                    </div>
                </div>
                <div class="loginemail">
                    <input type="text" name="email" placeholder="Email/Username" class="loginemailinput">
                </div>
                <div class="loginpassword">
                    <input type="password" name="password" placeholder="Password" class="loginpassinput">
                </div>
                <div class="loginforgotpassword">
                    <div class="forgotpasstxt">
                        Forgot you password? <a class="forgotpassbtn">Reset</a>
                    </div>
                </div>
                <div class="loginbtndiv">
                    <button type="submit" class="loginbtn">Login <img src="assets/imgs/svg/paper_send.svg" alt=""></button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Structure -->
<div id="resetpasswordmodal" class="modal">
    <form action="{{route('forgot')}}" method="POST">
        @csrf
        <div class="modal-content">
            <h4>Password Reset</h4>
            <label>Email Address</label>
            <input type="text" name="email">
        </div>
        <div class="modal-footer">
            <a onclick="document.getElementById('resetpasswordmodal').style.display = 'none'" class="modal-close waves-effect waves-green btn-flat">Close</a>
            <button type="submit" class="modal-close waves-effect waves-green btn-flat">Reset</button>
        </div>
    </form>
</div>

@endsection
