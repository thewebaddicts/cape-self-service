


@extends('layouts.app')
@section('title')
    <title>HomePage - {{ env('WEBSITE_NAME') }}</title>
@endsection

@section('content')
    <div class="pagecontent">
        @include('components.sidebar')

        <div class="pagerightside">
            @include('components.topbar')

            <div class="rightsidepagecontent">
                <div class="pagetitle">
                    <div class="title">
                        Account
                    </div>
                </div>
                <div class="accountpagecontent">
                    <div class="accountboxs">
                        <div class="accountbox">
                            <form method="POST" action="{{route('account_post')}}" class="accountcontent">
                                @csrf
                                <div class="title">
                                    Personal Information
                                </div>
                                <input type="hidden" name="id" value="{{$information->id}}">
                                <div class="infoinput">
                                    <input type="text" value="{{$information->first_name}}" name="fname" placeholder="First Name">
                                </div>
                                <div class="infoinput">
                                    <input type="text" value="{{$information->last_name}}" name="lname" placeholder="Last Name">
                                </div>
                                <div class="infoinput">
                                    <input type="text" value="{{$information->email}}" name="email_address" placeholder="Email/Username">
                                </div>
                                <div class="infoinput">
                                    <input type="text" value="{{$information->phone}}" name="phone_number" placeholder="Phone Number">
                                </div>
                                <div class="savebtnbox">
                                    <button type="submit" data-id="{{$information->id}}" class="savebtn">
                                        Save <img src="assets/imgs/svg/save_icon.svg" alt="">
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="accountbox">
                            <div class="accountcontent">
                                <div class="title">
                                    Change Password
                                </div>
                                <div class="infoinput">
                                    <input type="text" placeholder="Old Password">
                                </div>
                                <div class="infoinput">
                                    <input type="text" placeholder="New Password">
                                </div>
                                <div class="infoinput">
                                    <input type="text" placeholder="Confirm Password">
                                </div>
                                <div class="savebtnbox">
                                    <div class="savebtn">
                                        Change <img src="assets/imgs/svg/save_icon.svg" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
