<?php

namespace App\Http\Controllers;

use App\Models\PoliciesModel;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class PoliciesController extends Controller
{
    //
    public function getPoliciesData(){
        $loggeduserid = session('login')->id;
        $policies = PoliciesModel::where('canceled', 0)->where('client_code',$loggeduserid)->get();
        return view('pages.policies', compact('policies'));
    }

    public function dltPoliciesData(Request $request){
        $policie_info = PoliciesModel::where('canceled', 0)->where('id', $request->id)->first();
        $policie_info->canceled = 1;
        $policie_info->save();

        return redirect()->route('policies', ['notification_title' => 'Successfully', 'notification_message' => 'Policy Deleted']);
    }
}
