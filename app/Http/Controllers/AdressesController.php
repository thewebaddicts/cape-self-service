<?php

namespace App\Http\Controllers;

use App\Models\AdressesModel;
use App\Models\PersonalInformationModel;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class AdressesController extends Controller
{
    //
    public function getAddresses(){
        $loggeduserid = session('login')->id;
        $addresses = AdressesModel::where('cancelled' , 0)->where('ecom_users_id', $loggeduserid)->get();
        $information = PersonalInformationModel::where('cancelled', 0)->where('id',$loggeduserid)->first();

        return view('pages.address' , compact('addresses', 'information'));
    }

    public function deleteAddress(Request $address){
        $address = AdressesModel::where('cancelled' , 0)->where('id', $address->deleteaddress)->first();
        $address->cancelled = 1;
        $address->save();

        return redirect()->route('address', ['notification_title' => 'Successfully', 'notification_message' => 'Address Deleted']);
    }
}
