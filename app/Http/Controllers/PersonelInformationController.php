<?php
use App\Models\PersonalInformationModel;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class PersonelInformationController extends Controller
{
    //
    public function getPersonalInformation(){
        $loggeduserid = session('login')->id;
        return App\Models\PersonalInformationModel::where('canceled', 0)->where('id',$loggeduserid)->first();

    }
}
