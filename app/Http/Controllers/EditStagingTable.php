<?php

namespace App\Http\Controllers;

use App\Models\DataTable;
use App\Models\PoliciesModel;
use App\Models\StagingTable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class EditStagingTable extends Controller
{
    public function insertIntoDatabase()
    {
        $stagingdata = StagingTable::get();


        foreach($stagingdata AS $stage){
            //we check if there's a matching policy
            $policy = PoliciesModel::where('canceled', 0)->where('policy_nb', $stage->policy_nb)->where('endorsement', $stage->endorsement)->first();

            if(!$policy){ $policy = new PoliciesModel();  }

            //insert or update
            $policy->policy_nb = $stage->policy_nb;
            $policy->endorsement = $stage->endorsement;
            $policy->insurer = $stage->insurer;
            $policy->insurer_policy_nb = $stage->insurer_policy_nb;
            $policy->insurer_endorsement = $stage->insurer_endorsement;
            $policy->client_code = $stage->client_code;
            $policy->product_code = $stage->product_code;
            $policy->type = $stage->type;
            $policy->issuance = $stage->issuance;
            $policy->effective = $stage->effective;
            $policy->expiry = $stage->expiry;
            $policy->insured_risk = $stage->insured_risk;
            $policy->currency = $stage->currency;
            $policy->sum_insured = $stage->sum_insured;
            $policy->premium = $stage->premium;
            $policy->paid_premium = $stage->paid_premium;
            $policy->o_s = $stage->o_s;
            $policy->save();
        }
        StagingTable::truncate();

//        dd($stagingdata);
//        foreach ($stagingdata as $stage) {
//            $newpolicynb = $stage->policy_nb;
//            $newendorsement = $stage->endorsement;
//
//            foreach ($policies as $policy) {
//
//                if (($policy->policy_nb == $newpolicynb) && ($policy->endorsement == $newendorsement)) {
//                    PoliciesModel::where('canceled', 0)->where('policy_nb', $newpolicynb)->where('endorsement', $newendorsement)->update([
//                        'updated_at' => $stage->updated_at,
////                     'policy_nb' => $stage->policy_nb,
////                     'endorsement' => $stage->endorsement,
//                        'insurer' => $stage->insurer,
//                        'insurer_policy_nb' => $stage->insurer_policy_nb,
//                        'insurer_endorsement' => $stage->insurer_endorsement,
//                        'client_code' => $stage->client_code,
//                        'product_code' => $stage->product_code,
//                        'type' => $stage->type,
//                        'issuance' => $stage->issuance,
//                        'effective' => $stage->effective,
//                        'expiry' => $stage->expiry,
//                        'insured_risk' => $stage->insured_risk,
//                        'currency' => $stage->currency,
//                        'sum_insured' => $stage->sum_insured,
//                        'premium' => $stage->premium,
//                        'paid_premium' => $stage->paid_premium,
//                        'o_s' => $stage->o_s,
//                        'canceled' => $stage->canceled
//                    ]);
//                } else {
//                    PoliciesModel::where('canceled', 0)->insert([
//                        'updated_at' => $stage->updated_at,
//                        'policy_nb' => $stage->policy_nb,
//                        'endorsement' => $stage->endorsement,
//                        'insurer' => $stage->insurer,
//                        'insurer_policy_nb' => $stage->insurer_policy_nb,
//                        'insurer_endorsement' => $stage->insurer_endorsement,
//                        'client_code' => $stage->client_code,
//                        'product_code' => $stage->product_code,
//                        'type' => $stage->type,
//                        'issuance' => $stage->issuance,
//                        'effective' => $stage->effective,
//                        'expiry' => $stage->expiry,
//                        'insured_risk' => $stage->insured_risk,
//                        'currency' => $stage->currency,
//                        'sum_insured' => $stage->sum_insured,
//                        'premium' => $stage->premium,
//                        'paid_premium' => $stage->paid_premium,
//                        'o_s' => $stage->o_s,
//                        'canceled' => $stage->canceled
//                    ]);
//                }
//
//            }
//        }
//        StagingTable::truncate();
//        return redirect()->route('dashboard');
    }
}
