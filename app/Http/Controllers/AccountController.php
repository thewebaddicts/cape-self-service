<?php

namespace App\Http\Controllers;

use App\Models\PersonalInformationModel;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class AccountController extends Controller
{
    //
    public function accountPage()
    {
        $loggeduserid = session('login')->id;
        $information = PersonalInformationModel::where('cancelled', 0)->where('id', $loggeduserid)->first();
//        dd($information);
        return view('pages.account', compact('information'));
    }

    public function updateProfile(Request $request)
    {

        $user_info = PersonalInformationModel::where('cancelled' , 0)->where('id', $request->id)->first();
        $user_info->first_name = $request->fname;
        $user_info->last_name = $request->lname;
        $user_info->email = $request->email_address;
        $user_info->phone = $request->phone_number;
        $user_info->save();

        return redirect()->route('account', ['notification_title' => 'Successfully', 'notification_message' => 'updated']);
    }
}
