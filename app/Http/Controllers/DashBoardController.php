<?php

namespace App\Http\Controllers;

use App\Models\PoliciesModel;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class DashBoardController extends Controller
{
    //
    public function dashboardInfo(){
        $loggeduserid = session('login')->id;
        $policies = PoliciesModel::where('canceled', 0)->where('client_code', $loggeduserid)->get();
        return view('pages.dashboardtest', compact('policies'));
    }
}
