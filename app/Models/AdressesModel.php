<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdressesModel extends Model
{
    use HasFactory;

    protected $table = 'ecom_users_addresses';
}
