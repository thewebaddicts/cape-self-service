<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PoliciesModel extends Model
{
    use HasFactory;

    protected $table = 'policies';
}
