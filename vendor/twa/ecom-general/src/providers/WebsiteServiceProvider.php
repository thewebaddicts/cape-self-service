<?php

namespace twa\ecomgeneral\providers;

use Illuminate\Routing\Events\RouteMatched;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\View\View;
use App\Http\Kernel;
Class WebsiteServiceProvider extends ServiceProvider{
    public function boot(){
        $this->publishes([
            __DIR__.'/../config/ecom.php' => config_path('ecom.php'),
        ], 'config');
        $this->loadViewsFrom(__DIR__.'/../views/', 'EcomGeneralViews');

        self::checkKernel();
    }

    public function register(){
        include_once(__DIR__.'/../helpers/helpers.php');
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->loadRoutesFrom(__DIR__.'/../routes/api.php');
//        $this->app->make('twa\ecomAuth\');
    }


    public function checkKernel(){
        $values = file_get_contents(__DIR__.'../../../../../../app/Http/Kernel.php');
        $values = str_replace(['\n','\r','<?php','?>','protected ',''],' ',$values);
        $values = substr($values,strpos($values,'class Kernel extends HttpKernel')+35);
        $values = substr($values,0,strlen($values) - 3);
        eval($values);
//        $required = [];
        $required = ['Illuminate\Session\Middleware\StartSession','App\Http\Middleware\EncryptCookies'];
        $missing = [];
        foreach ($required AS $row) {
            if(!in_array($row,$middleware)){
                $missing[] = "\\".$row."::class";
            }
        }
        if(count($missing)>0){
            $error = [ "status" => "error", "message" => "The ecommerce package requires the following modification. Please navigate to \"/app/Http/kernel.php\" and add the following class(es) to your 'middleware' variable ".implode(' , ',$missing)];
            echo json_encode($error); exit();
        }
    }

}
