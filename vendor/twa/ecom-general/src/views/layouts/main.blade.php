<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>

<style type="text/css">
    *{ box-sizing: border-box; }
    @media print {
        body {
            -webkit-print-color-adjust: exact;
        }
    }

    select{
        border:1px solid #000; padding: 10px;
    }
    label.black{
        background-color: #000; color:#FFF; width: 100%; padding: 5px 10px; text-align: center; display: block;
    } label{
        background-color: transparent; color:#000; width: 100%; padding: 5px 10px; text-align: center; display: block;
    }
    .button{
        color:#fff; width: 100%; padding: 10px; text-align: center; display: block; background-color: #008000; text-transform: uppercase; font-weight: 700; border:0px;
    }

</style>

<body style="font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',sans-serif; font-weight:300; font-size: 13px;">

<div style="background-color:#fff;  width:100%; max-width:750px; padding: 0 40px; margin:auto; overflow:hidden;">
    @yield('content')
</div>

</body>

</html>

