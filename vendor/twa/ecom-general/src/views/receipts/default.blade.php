<?php
use \Milon\Barcode\DNS1D;

if(!isset($hide_prices)){ $hide_prices = 0; }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Order Details</title>
</head>

<style type="text/css">
    @media print {
        body {
            -webkit-print-color-adjust: exact;
        }
    }
</style>

@php
    $store = ecom('stores')->getCurrent();
   $order_number =  $data['id'];
       if(isset($store)){
           $order_id = '#'.$store->ref_prefix .'-'.$order_number;
       }else{
           $order_id = '#'.$data['id'];
       }
@endphp

<body
    style="font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',sans-serif; font-weight:300;">

    <div style="background-color:#fff;  width:100%; max-width:750px; padding: 0 40px; margin:auto; overflow:hidden;">
        <table style="border-collapse: collapse; width:100%; margin-bottom:10px">
            <tr>
                <td style="padding:10px 0px ; font-size:26px ; font-weight:bolder ; text-align:left ">
                    <img src="{{config('ecom.receipt_data.logo')}}" alt="LOGO"
                        width="{{ config('ecom.receipt_data.logo_width', 170) }}" height="{{ config('ecom.receipt_data.logo_height', 170) }}" />
                </td>
                <td
                    style="padding:10px 0px ; font-size:15px ;font-family:'Raleway',sans-serif; font-weight:bolder ; text-align:right ">
                </td>
            </tr>
        </table>

        <table style="border-collapse: collapse; width:100%; margin-bottom:20px;  font-size:12px">
            <tr>
                <td style="width : 70%;vertical-align: top;">
                    <table style="border-collapse: collapse; width:100%; margin-bottom:20px;  font-size:12px">
                        <tr>
                            <th colspan="2" style="text-align: left;font-size: 18px;  padding: 5px 0; margin: 0 5px">
                                {{config('ecom.receipt_data.company_name')}}</th>
                        </tr>
                        <tr>
                            <th style="text-align: left;font-size: 12px;" width="100">Account # </th>
                            <td>:{{env('WEBSITE_ACCOUNT_NUMBER', '-')}}</td>
                        </tr>
                        <tr>
                            <th style="text-align: left;font-size: 12px;">Email </th>
                            <td>: {{env('WEBSITE_ACCOUNT_EMAIL', '-')}}</td>
                        </tr>
                        <tr>
                            <th style="text-align: left;font-size: 12px;">Address </th>
                            <td>:
                                {{env('WEBSITE_ACCOUNT_ADDRESS', '-')}}</td>
                        </tr>
                        <tr>
                            <th style="text-align: left;font-size: 12px;">Order Date</th>
                            <td>: {{\Illuminate\Support\Carbon::parse($data["created_at"])->format('d-m-Y')}}</td>
                        </tr>
                    </table>
                </td>
                <td style="width : 30%;vertical-align: top;">

                    <table style="border-collapse: collapse; width:100%; margin-bottom:10px;  font-size:12px">
                        <tr>
                            <th
                                style="text-align: left; font-weight: 600; font-size: 13px; background-color:#eee; padding: 10px; margin: 0 5px; text-transform: uppercase;">
                                Shipping Service</th>
                        </tr>
                    </table>

                    @if($data["ecom_stores_delivery_details"])
                    <table style=" width:100%;">
                        <tr style="padding:10px 0">
                            <td style="padding:0px 0">
                                @if($data['ecom_stores_delivery_details']["label"])<div> <b>Shipping Service: </b> {{$data['ecom_stores_delivery_details']["label"] }} </div> @endif
                                @if($data['ecom_stores_delivery_details']["provider"])<div> <b>Shipping Provider: </b> {{$data['ecom_stores_delivery_details']["provider"] }} </div>@endif
                                @if($data["tracking_number"])<div> <b> Tracking Number:</b>{{ $data["tracking_number"]}} </div>@endif
                                @if($data["delivery_note"])<div> <b> Delivery Note:</b>{{ $data["delivery_note"]}} </div>@endif
                                @if($data["date_delivery"])<div> <span style="color:red"><b> Delivery Date:</b></span> {{ date("d-m-Y", strtotime($data["date_delivery"]))}} </div>@endif
                            </td>
                        </tr>
                        @if(isset($data["tracking_number"]) &&
                        $data["tracking_number"] && $data["tracking_number"] !=
                        '')
                        <tr style="padding:10px 0">
                            <td style="padding:10px 0"> <img
                                    src="data:image/png;base64,{{ (new DNS1D)->getBarcodePNG($data["tracking_number"], 'C128' ) }}" alt="barcode" />
                                <div style="text-align: center">{{$data["tracking_number"]}}</div>
                            </td>
                        </tr>
                        @endif

                    </table>
                    @endif
                </td>
            </tr>
        </table>

        <table style="border-collapse: collapse; width:100%; margin-bottom:20px;  font-size:12px">
            <tr>
                <td style="width : 50%;vertical-align: top;">

                    <table style="border-collapse: collapse; width:95%; margin-bottom:20px;  font-size:12px">
                        <tr>
                            <th
                                style="text-align: left; font-weight: 600; font-size: 13px; background-color:#eee; padding: 10px; margin: 0 5px; text-transform: uppercase;">
                                Shipping details</th>
                        </tr>
                    </table>

                    <table style="border-collapse: collapse; width:95%; margin-bottom:20px;  font-size:12px">
                        <tr>
                            <th style="text-align: left;font-size: 12px; width:100px">Full Name</th>
                            <td>: {{$data['shipping_address_details']['first_name']." ".$data['shipping_address_details']['last_name']}}</td>
                        </tr>
                        <tr>
                            <th style="text-align: left;font-size: 12px">Phone </th>
                            <td>:{{$data['shipping_address_details']["phone"]}}</td>
                        </tr>
                        @if(isset($data['shipping_address_details']["country"]) && isset($data['shipping_address_details']["country"]["name"]) )
                            <tr>
                                <th style="text-align: left;font-size: 12px">Country</th>
                                <td>: {{$data['shipping_address_details']["country"]["name"]}}</td>
                            </tr>
                        @endif
                        @if(isset($data['shipping_address_details']["state_info"]) && isset($data['shipping_address_details']["state_info"]["label"]) )
                            <tr>
                                <th style="text-align: left;font-size: 12px">State</th>
                                <td>: {{$data['shipping_address_details']["state_info"]["label"]}}</td>
                            </tr>
                        @endif
                        @if(isset($data['shipping_address_details']["city"]) && $data['shipping_address_details']["city"] != '')
                        <tr>
                            <th style="text-align: left;font-size: 12px">City </th>
                            <td>:  {{$data['shipping_address_details']["city"]}}</td>
                        </tr>
                        @endif
                        @if(isset($data['shipping_address_details']["street"]) && $data['shipping_address_details']["street"] != '')
                        <tr>
                            <th style="text-align: left;font-size: 12px">Street </th>
                            <td>: {{$data['shipping_address_details']["street"]}}</td>
                        </tr>
                        @endif
                        @if(isset($data['shipping_address_details']['postal_code']) && $data['shipping_address_details']['postal_code'] != '')
                        <tr>
                            <th style="text-align: left;font-size: 12px">Postal Code </th>
                            <td>: {{$data['shipping_address_details']['postal_code']}}</td>
                        </tr>
                        @endif
                        @if(isset($data['shipping_address_details']["building"]) && $data['shipping_address_details']["building"] != '')
                        <tr>
                            <th style="text-align: left;font-size: 12px">Building </th>
                            <td>: {{$data['shipping_address_details']["building"]}}</td>
                        </tr>
                        @endif
                        @if(isset($data['shipping_address_details']["delivery_note"]) && $data['shipping_address_details']["delivery_note"] != '')
                        <tr>
                            <th style="text-align: left;font-size: 12px">Delivery Note </th>
                            <td>: {{$data['shipping_address_details']["delivery_note"]}}</td>
                        </tr>
                        @endif

                    </table>
                </td>

                @if(isset($data['billing_address_details']))

                    <td style="width : 50%;vertical-align: top;">

                    <table style="border-collapse: collapse; width:95%; margin-bottom:20px;  font-size:12px">
                        <tr>
                            <th
                                style="text-align: left; font-weight: 600; font-size: 13px; background-color:#eee; padding: 10px; margin: 0 5px; text-transform: uppercase;">
                                Billing details</th>
                        </tr>
                    </table>

                    <table style="border-collapse: collapse; width:95%; margin-bottom:20px;  font-size:12px">
                        <tr>
                            <th style="text-align: left;font-size: 12px; width:100px">Full Name</th>
                            <td>: {{$data['billing_address_details']['first_name']." ".$data['billing_address_details']['last_name']}}</td>
                        </tr>
                        <tr>
                            <th style="text-align: left;font-size: 12px">Phone </th>
                            <td>:{{$data['billing_address_details']["phone"]}}</td>
                        </tr>
                        @if(isset($data['billing_address_details']["country"]) && isset($data['billing_address_details']["country"]["name"]) )
                            <tr>
                                <th style="text-align: left;font-size: 12px">Country</th>
                                <td>: {{$data['billing_address_details']["country"]["name"]}}</td>
                            </tr>
                        @endif
                        @if(isset($data['billing_address_details']["state_info"]) && isset($data['billing_address_details']["state_info"]["label"]) )
                            <tr>
                                <th style="text-align: left;font-size: 12px">State</th>
                                <td>: {{$data['billing_address_details']["state_info"]["label"]}}</td>
                            </tr>
                        @endif
                        @if(isset($data['billing_address_details']["city"]) && $data['billing_address_details']["city"] != '')
                            <tr>
                                <th style="text-align: left;font-size: 12px">City </th>
                                <td>:  {{$data['billing_address_details']["city"]}}</td>
                            </tr>
                        @endif
                        @if(isset($data['billing_address_details']["street"]) && $data['billing_address_details']["street"] != '')
                            <tr>
                                <th style="text-align: left;font-size: 12px">Street </th>
                                <td>: {{$data['billing_address_details']["street"]}}</td>
                            </tr>
                        @endif
                        @if(isset($data['billing_address_details']['postal_code']) && $data['billing_address_details']['postal_code'] != '')
                            <tr>
                                <th style="text-align: left;font-size: 12px">Postal Code </th>
                                <td>: {{$data['billing_address_details']['postal_code']}}</td>
                            </tr>
                        @endif
                        @if(isset($data['billing_address_details']["building"]) && $data['billing_address_details']["building"] != '')
                            <tr>
                                <th style="text-align: left;font-size: 12px">Building </th>
                                <td>: {{$data['billing_address_details']["building"]}}</td>
                            </tr>
                        @endif
                        @if(isset($data['billing_address_details']["delivery_note"]) && $data['billing_address_details']["delivery_note"] != '')
                            <tr>
                                <th style="text-align: left;font-size: 12px">Delivery Note </th>
                                <td>: {{$data['billing_address_details']["delivery_note"]}}</td>
                            </tr>
                        @endif

                    </table>
                </td>

                @endif
            </tr>
        </table>

        <table style="border-collapse: collapse; width:50%; margin-bottom:20px;  font-size:12px">
            <tr>
                <td style="width : 100%;vertical-align: top;">
                    <table style="border-collapse: collapse; width:100%; margin-bottom:10px;  font-size:12px">
                        <tr>
                            <th
                                style="text-align: left; font-weight: 600; font-size: 13px; background-color:#eee; padding: 10px; margin: 0 5px; text-transform: uppercase;">
                                Client Details</th>
                        </tr>
                    </table>
                    <table style=" width:100%;">
                        <tr>
                            <td> {{ $User["first_name"]." ".$User["last_name"] }}</td>
                        </tr>
                        <tr>
                            <td> {{ $User["email"] }}</td>
                        </tr>
                        <tr>
                            <td> {{ $User["phone"] }}</td>
                        </tr>

                        <tr>
                            <td>
                                <h3 style="margin: 15px 0 0px 0"> ORDER : {{ $order_id }}
                                </h3>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h3 style="margin: 5px 0 0px 0"> NB ITEMS: {{ $data["items"]["count"]
                                    }}</h3>
                            </td>
                        </tr>
                        @if(isset($hide_prices) && $hide_prices != 1)
                            <tr>
                                <td>
                                    <h3 style="margin-top:5px">COD: {{ $data["items"]["grandTotal_formated"] }}</h3>
                                </td>
                            </tr>
                        @endif
                    </table>
                </td>
            </tr>
        </table>

        <table style="border-collapse: collapse; width:100%; margin-bottom:0px;  font-size:12px">
            <tr style="">

                <th style="text-align: left;font-size: 12px; background-color: #eee; padding: 10px 5px; margin: 0 5px">
                    Image</th>


                <th style="text-align: left;font-size: 12px; background-color: #eee; padding: 10px 0px; margin: 0 5px">
                    Details</th>

                <th style="text-align: left;font-size: 12px; background-color: #eee; padding: 10px 0px; margin: 0 5px">
                    Quantity
                </th>
                @if(isset($hide_prices) && $hide_prices != 1)
                <th style="text-align: left;font-size: 12px; background-color: #eee; padding: 10px 0px; margin: 0 5px">
                    Total
                </th>
                @endif
            </tr>
            @foreach($data["items"]["items"]["items"] as $item)
            <tr>
                <td style="border-bottom: 1px solid #eee">
                    <img height='100' width='100' src='{{$item['details']['thumb']}}' alt='thumbnail' />
                </td>
                <td style="border-bottom: 1px solid #eee">
                    <div style="font-weight: 600;margin-bottom: 5px;text-align: left;">{{$item['details']['label']}}</div>
                    <div style="font-weight: 400;margin-bottom: 5px;text-align: left;">{{$item['details']['sku']}}</div>

                    <div style="font-size:10px;margin-bottom: 5px;text-align: left;">
                        @if($item['details']['unit_price_beforediscount_formatted'] )
                            <span style="text-decoration: line-through;opacity: 0.5; margin-right: 5px;">{{ $item['details']['unit_price_beforediscount_formatted'] }}</span>
                        @endif

                        {{ $item['details']['unit_price_formatted'] }}
                    </div>

                    @if(isset($item['modifier_details']))
                        <div style="color: rgba(15,11,12,.5);font-size: 10px;margin-top: 5px;font-weight: 500;text-align: left;">
                            <div class="ribons" style="text-align: left;">
                                @foreach($item['modifier_details'] as $modifier)
                                    @php
                                        $price = intval($modifier['price_formatted']);
                                    @endphp
                                    <div class="ribon"> {{ $modifier['label'] }} : {{$modifier['message']}} @if(isset($price) && $price != 0) ({{$modifier['price_formatted']}}) @endif</div>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    @if(isset($item['cms_attributes']) && isset($item['cms_attributes']['type']) == 'buildyourbox' && isset($item['other_details']))
                        <div style="color: rgba(15,11,12,.5);font-size: 10px;margin-top: 5px;font-weight: 500;text-align: left;">
                            {!! $item['other_details'] !!}
                        </div>
                    @endif



                    @if(isset($item['cms_attributes']['preorder']) && $item['cms_attributes']['preorder'])
                        <div style="font-size: 12px;font-weight: 500">Pre Ordered</div>
                            @if(isset($item['cms_attributes']['preorder_notification']))
                                <div style="font-size: 12px;font-weight: 500;color:rgba(15,11,12,.5)">{{$item['cms_attributes']['preorder_notification']}}</div>
                            @endif
                    @endif

                </td>
                <td style="border-bottom: 1px solid #eee">
                    {{$item["quantity"]}}
                </td>
                @if(isset($hide_prices) && $hide_prices != 1)
                <td style="border-bottom: 1px solid #eee">
                    {{$item["final_price_formatted"]}}
                </td>
                @endif
            </tr>
            @endforeach
        </table>

        <table style="border-collapse: collapse; width:100%; margin-bottom:30px;  font-size:12px">
            <tr>
                <th
                    style="text-align: left;font-size: 16px; height:14px ;background-color:#eee; padding: 10px; margin: 0 5px">
                </th>
            </tr>
        </table>
        @if(isset($hide_prices) && $hide_prices != 1)
        <table style="border-collapse: collapse; width:100%; margin-bottom:20px;  font-size:12px">
            <tr>
                <td style="width : 60%;vertical-align: top;">

                    <table style="border-collapse: collapse; width:46.6%; margin-bottom:10px;  font-size:12px">
                        <tr>
                            <th
                                style="text-align: left;font-size: 16px; background-color:#eee; padding: 10px; margin: 0 5px">
                                Payment details</th>
                        </tr>
                    </table>

                    <table style="width : 60%;border-collapse: collapse;  margin-bottom:20px;  font-size:12px">

                        @if(isset($data['ecom_payment_methods_details']['label']) && $data['ecom_payment_methods_details']['label'])
                        <tr>
                            <td style="padding: 10px 0">
                                <b> Payment method: </b> {{ $data['ecom_payment_methods_details']['label'] }}
                            </td>
                        </tr>
                        @endif

                        @if(isset($data['ecom_payment_methods_details']['provider']) && $data['ecom_payment_methods_details']['provider'])
                        <tr>
                            <td style="padding: 10px 0">
                                <b> Payment Provider: </b> {{ $data['ecom_payment_methods_details']['provider'] }}
                            </td>
                        </tr>
                        @endif
                    </table>
                </td><td style="width : 40% ;vertical-align: top;">
                    <table style="border-collapse: collapse; width:100%; margin-bottom:20px;  font-size:12px">
                        @foreach($data["items"]['breakdown'] as $key => $value)
                            <tr>
                                <th style="text-align: left; @if($loop->even) background-color: #eee @endif">
                                    {{$key}}</th>
                                <td style="text-align: right; @if($loop->even) background-color: #eee @endif">
                                    {{$value}}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <th style="text-align: left;background-color: #eee" >
                                Grand Total</th>
                            <td style="text-align: right; background-color: #eee">
                                {{ $data["items"]['grandTotal_formated']}}</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>@endif

    </div>

</body>

</html>
@if(request()->has('print'))
    <script language="javascript">
        window.print();
    </script>
@endif
