@extends('EcomGeneralViews::layouts.main')

@section('content')
    @foreach($ids AS $id)
        <iframe src="/ecom/order/fulfill/single/{{ $id }}/{{ $token }}" width="100%" height="240" marginwidth="0" marginheight="0" frameborder="0"></iframe>
        <hr noshade="" size="0" style="height:0px; composer require twa/omni-smscomposer require twa/omni-smsborder-top:1px solid grey">
    @endforeach
@endsection
