@extends('EcomGeneralViews::layouts.main')

@section('content')
    <?php
        $Purchase = \twa\ecomcart\models\EcomPurchasesModel::where('id',$id)->where('cancelled',0)->first();
        if(!$Purchase){ exit(); }
        $statuses = [  "Accepted" ,  "Being Prepared", "Ready for shipping", "Fulfilled / Shipped" ];
        $payment_statuses = [  "Payment Not Received" ,  "Payment partially received", "Payment completely received"];
    ?>
    <form method="post">
    <table width="100%" cellspacing="0" cellpadding="10" style="margin: 30px -40px; width:calc(100% + 80px)">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr><td style="padding-right: 10px;">Order #</td><td><b>{{ $Purchase->id }}</b></td></tr>
                    <tr><td style="padding-right: 10px;">Order Date</td><td><b>{{ $Purchase->created_at }}</b></td></tr>
                    <tr><td style="padding-right: 10px;">Order Total</td><td><b>{{ $Purchase->items['grandTotal_formated'] }}</b></td></tr>
                    <tr><td style="padding-right: 10px;">Current Status</td><td><b>@if($Purchase->status != ''){{ $Purchase->status }} @else no status yet @endif</b></td></tr>
                </table>
            </td>
            <td>
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr><td align="right"><label class="">Change order status</label></td></tr>
                    <tr>
                        <td align="right" style="padding: 0px 0;">
                            <select name="status" style="width: 100%">
                                @foreach($statuses AS $status)
                                    <option value="{{ strtolower($status) }}" @if(strtolower($status) == $Purchase->status) selected @endif>{{ $status }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>


                    <tr><td align="right"><label class="">Change payment status</label></td></tr>
                    <tr>
                        <td align="right" style="padding: 0px 0;">
                            <select name="status_payment" style="width: 100%">
                                @foreach($payment_statuses AS $status)
                                    <option value="{{ strtolower($status) }}" @if(strtolower($status) == $Purchase->status) selected @endif>{{ $status }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="submit" value="Save Status" class="button">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr><td colspan="2"></td></tr>
        <tr><td colspan="2" align="center">suggested operations: <a href="{{ env('APP_URL') }}/ecom/order/{{ $Purchase->id }}/{{ md5($Purchase->id)  }}" target="_blank">Print Pickslip</a> | <a href="{{ $Purchase->waybill_link }}" target="_blank">Print Waybill</a> | <a href="#">Cancel Order</a></td></tr>
    </table>
    </form>
@endsection

