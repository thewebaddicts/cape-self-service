<?php

$dictionary = json_decode(json_encode($dictionary));

$form_data = "<table style='margin:auto;text-align:center;'>";
foreach($dictionary->form_data as $key=>$value ){
    $form_data .=  "<tr><td  style='width: 100px ; padding:5px 30px 5px 30px; font-size:12px ; font-weight:bolder ; text-transform: uppercase'>".  $key ."</td><td   style='padding:5px 30px 5px 30px; font-size:12px ; '> " . $value ."</td></tr>";
}
$form_data .= "</table>";
$email=$template->{'email_form_'.app()->getLocale()};
$dictionary->form_data =  $form_data;
$Search = [];
$Replace = [];
foreach ($dictionary AS $key=>$value){
    $Search[]   =   "*".$key."*";
    $Replace[]  =   $value;
}

$email = str_replace( $Search, $Replace,$email );
?>

<div style="background-color:#fff;  width:100%; max-width:750px; padding: 0 40px; margin:auto; overflow:hidden;">
    {!! nl2br($email) !!}
</div>
