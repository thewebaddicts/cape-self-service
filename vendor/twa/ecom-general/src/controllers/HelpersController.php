<?php

namespace twa\ecomgeneral\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class HelpersController extends Controller
{
    public static function processAPIError($error){
        $backURL = parse_url(redirect()->back()->getTargetUrl());
        if(isset($backURL['query'])){ parse_str($backURL['query'],$backParams); }else{ $backParams = []; }

        if(isset($error['error'])){
            $object = $error['error'];
        }elseif($error['success']){
            $object = $error['success'];
        }
        foreach ($object AS $key => $value){
            unset($backParams[$key]);
        }
//        Session::flash('_old_input');

        if(isset($backURL['port'])){ $port = ":".$backURL['port']; }else{ $port = ""; }
        $url = $backURL['scheme'].'://'.$backURL['host'].$port.$backURL['path'].'?'.http_build_query($object);
//        foreach (request()->post() AS $key => $value){
//            session(['_old_input.'.$key => $value]);
//        }

        return redirect($url);
    }
}
