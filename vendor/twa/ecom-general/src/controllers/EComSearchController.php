<?php

namespace twa\ecomgeneral\controllers;

use App\Http\Controllers\Controller;
use twa\apilibs\traits\APITrait;
use twa\ecomauth\facades\EcomUsersRegistrationFacade;
use twa\ecomauth\models\EComTemporaryUsersModel;
use twa\ecomgeneral\models\EComCurrencyModel;
use twa\ecomauth\controllers\EcomUsersController;
use App\Http\Repositories\API;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use twa\ecomgeneral\models\EcomStoreModel;
use twa\ecomgeneral\models\EcomUsersModel;
use twa\ecomproducts\models\EcomProductModel;

class EComSearchController extends Controller
{
    use APITrait;


    public static function productsExportJson(){
        $Languages = lang()->list();
        $TranslatableFields = ["label",'small_description'];
        if(env('CMS_LANGUAGES_MODE') == "translated"){
            $PossibleTranslations = collect($TranslatableFields)->crossJoin($Languages->pluck('prefix'));
            foreach ($PossibleTranslations AS $key => $row){
                $PossibleTranslations[$key] = implode('_',$row);
            }
        }else{
            $PossibleTranslations = $TranslatableFields;
        }

        $Products = EcomProductModel::select(implode(',',$PossibleTranslations),'id','sku','ecom_products_filters_id','ecom_subcategories_id','keywords')->where('cancelled',0)->where('display',1)->get();
        dd($Products);
    }

}
