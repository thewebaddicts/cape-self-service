<?php

namespace twa\ecomgeneral\controllers;

use App\Http\Controllers\Controller;
use twa\apilibs\traits\APITrait;
use twa\ecomauth\facades\EcomUsersRegistrationFacade;
use twa\ecomauth\models\EComTemporaryUsersModel;
use twa\ecomgeneral\models\EComCurrencyModel;
use twa\ecomauth\controllers\EcomUsersController;
use App\Http\Repositories\API;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use twa\ecomgeneral\models\EcomStoreModel;
use twa\ecomgeneral\models\EcomUsersModel;
use twa\ecomgeneral\models\EComItemsRestockedModel;
use twa\ecomproducts\models\EcomProductModel;

class EComStockController extends Controller
{
    use APITrait;


    public function storeUser($values=[])
    {
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,['productID','email']);

        if($Check!==true){ return $Check; }

        $product_id = $values->get('productID');
        $product = EcomProductModel::where('cancelled',0)->where('id',$product_id)->first();

        if(!$product){
            return response()->json(['error' => true, 'message' => 'Invalid Product'], 200);
        }

        $store_id = $product->ecom_stores_id;
        $user_id = null;
        $email = $values->get('email');

        if(!$email){
            return response()->json(['error' => true, 'message' => __('cart.update_email')], 200);
        }

        $token = false;
        if($values->get('token')){
            $token = $values->get('token');
        }elseif (session('login') && session('login')['token']){
            $token = session('login')['token'];
        }

        $User = false;
        if($token){
            $User = EcomUsersModel::where('token',$token)->where('cancelled',0)->first();
        }

        if($User){
            $user_id = $User->id;
        }

        $already_saved = EComItemsRestockedModel::where('user_email',$email)->where('ecom_stores_id' , $store_id)->where('ecom_products_id' ,  $values->get('productID'))->where('sent',0)->first();

        if($already_saved){
            return response()->json(['success' => true, 'message' => __('cart.email_restocked_already_saved')], 200);
        }
        $notification = new EcomItemsRestockedModel();
        $notification->ecom_users_id = $user_id;
        $notification->ecom_stores_id = $store_id;
        $notification->ecom_products_id = $values->get('productID');
        $notification->user_email = $email;
        $notification->sent = 0;

        $notification->save();

        return response()->json(['success' => true, 'message' => __('cart.email_once_restocked')], 200);

    }
}
