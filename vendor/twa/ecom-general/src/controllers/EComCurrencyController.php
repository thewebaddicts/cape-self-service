<?php

namespace twa\ecomgeneral\controllers;

use App\Http\Controllers\Controller;
use twa\apilibs\traits\APITrait;
use twa\ecomauth\facades\EcomUsersRegistrationFacade;
use twa\ecomauth\models\EComTemporaryUsersModel;
use twa\ecomgeneral\models\EComCurrencyModel;
use twa\ecomauth\controllers\EcomUsersController;
use App\Http\Repositories\API;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use twa\ecomgeneral\models\EcomStoreModel;
use twa\ecomgeneral\models\EcomUsersModel;

class EComCurrencyController extends Controller
{
    use APITrait;

    public function list($values = [],$debug = false){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ ],[ 'store' ]);
        if($Check!==true){ return $Check; }

        $Store = EcomStoreModel::select('*');
        if(env('CMS_LANGUAGES_MODE') == "translated"){
            $Store = $Store->selectRaw('header_message_'.app()->getLocale().' AS header_message, label_'.app()->getLocale().' AS label, group_'.app()->getLocale().' AS `group` , contact_us_store_information_'.app()->getLocale().' AS contact_us_store_information');
        };
        $Store = $Store->where('id',$values->get('store'))->where('cancelled',0)->first();
        if(!$Store){
            return $this->APIDisplayResponse(42);
        }

        $currencies = \twa\ecomproducts\models\EComCurrencyModel::select('*');
        if(env('CMS_LANGUAGES_MODE') == "translated"){
            $currencies = $currencies->selectRaw('name_'.app()->getLocale().' AS name, label_'.app()->getLocale().' AS label, symbol_'.app()->getLocale().' AS symbol');
        };


        $User = false;
        if(isset(request()->user['user']['id'])){
            $User = EcomUsersModel::where('id',request()->user['user']['id'])->first();
        }

        $currencies = $currencies->whereIn('id',$Store->ecom_currencies_id)->get();
        foreach ($currencies AS $currency){
            if($User && isset($User['ecom_currencies_id'])){
                if($User['ecom_currencies_id'] == $currency->id){
                    $currency->selected = true;
                }else{
                    $currency->selected = false;
                }
            }else{
                if($currency->id == $Store->default_ecom_currencies_id){
                    $currency->selected = true;
                }else{
                    $currency->selected = false;
                }
            }
        }
        return $currencies;
    }


    public function switch($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ 'currencyID' ],[ 'store' ]);
        if($Check!==true){ return $Check; }

        $Store = EcomStoreModel::where('id',$values->get('store'))->where('cancelled',0)->first();
        if(!$Store){ return $this->APIDisplayResponse(42); }


        if(!$Store->ecom_currencies_id->contains($values->get('currencyID'))){ return $this->APIDisplayCustomResponse(0,"error","not found","currency was not found"); }

        $currencies = EComCurrencyModel::select('*')->selectRaw('symbol_'.app()->getLocale().' AS symbol')->where('cancelled',0)->where('id',$values->get('currencyID'))->first();
        if(!$currencies){  return $this->APIDisplayCustomResponse(0,"error","not found","currency was not found"); }
        try{
            DB::table(str_replace('_id','',request()->user->column))->where('id',request()->user->user['id'])->update([ 'ecom_currencies_id' => $currencies->id ]);
            //also update the request
        }catch (\Throwable $e){
            dd($e);
        }
        return self::list($values,true);
    }



    public static function getPreferredCurrency(EcomStoreModel $Store, $User = false,$values = []){
        if(!$User && !$values->get('draft_cart_id')){
            //we should remove all cookies, as we notice that this will stop the ecom functionality when an invalid token exists in the session
            $UsersFacade = new EcomUsersRegistrationFacade();
            $UsersFacade->flushCookies();
            $UsersFacade->logout();
            return false;
        }

        //we get the user info
        $Preferred = false;
        try{ $Preferred = request()->user->user->ecom_currencies_id; }catch (\Throwable $e){  }

        if($Preferred){
            //if the user has a preferred, we match if this preferred is valid for this store
            if(!$Store->ecom_currencies_id->contains($Preferred)){
                $Preferred = false;
            }
        }

        if(!$Preferred){
            //we get the store default currency
            $Preferred = $Store->default_ecom_currencies_id;
        }

        if(!$Preferred){
            //we get the store default currency
            $Preferred = $Store->default_ecom_currencies_id;
        }

        if(!$Preferred){
            //we get the store default currency
            $Preferred = $Store->ecom_currencies_id->first();
        }
        $Currency = EComCurrencyModel::where('id',$Preferred)->where('cancelled',0)->first();
        if(!$Currency){
            $Currency = EComCurrencyModel::where('cancelled',0)->orderBy('id')->first();
        }


        return $Currency;
    }

    public static function getCODCurrency(EcomStoreModel $Store, $PreferredCurrency){
        $Store = collect($Store);
        if(!$Store || !$PreferredCurrency){
            return false;
        }
        if(isset($Store['default_cod_ecom_currencies_id']) && $Store['default_cod_ecom_currencies_id']!=$PreferredCurrency['id']){
            return EComCurrencyModel::select('*')->selectRaw('symbol_'.app()->getLocale().' AS symbol')->where('id',$Store['default_cod_ecom_currencies_id'])->where('cancelled',0)->first();
        }else{
            return false;
        }
    }

    #[Pure] public static function formatterArray($price, $currency, $rounded = true): Array
    {
        if(!$currency){
            return [ "price" => $price, "price_not_formatted" => (isset($price_not_formatted)) ? $price_not_formatted : $price, "currency" => false ];
        }
        try{
            $price = $price * $currency->rate;
        }catch (\Throwable $e) {
            dd($e);
             return [ "price" => $price, "price_not_formatted" => (isset($price_not_formatted)) ? $price_not_formatted : $price, "currency" => isset($currency->symbol) ? $currency->symbol : "" ];
            // return $price;
        }

        if(!$currency->rounding || $currency->rounding == NULL || $currency->rounding == 0){ $currency->rounding = 0; }

        if($rounded){
            $price = self::roundUpToAny($price, $currency->rounding);
        }

        if($currency->separator_decimal == NULL || $currency->separator_decimal == ""){ $currency->separator_decimal = "."; }
        if($currency->separator_thousands == NULL || $currency->separator_thousands == ""){ $currency->separator_thousands = " "; }

        $price_not_formatted = $price;
        $price = number_format($price ,$currency->nb_decimal ,$currency->separator_decimal, $currency->separator_thousands)." ".$currency->symbol;

//        if(app()->getLocale() == "ar"){
//            $price = self::number2farsi($price);
//        }

        return [ "price" => $price, "price_not_formatted" => (isset($price_not_formatted)) ? $price_not_formatted : $price, "currency" => $currency->symbol ];
    }

    #[Pure] public static function formatter($price, $currency, $nounits = false, $convert = true, $debug = false , $rounded=true): float|int|string
    {

        $resonse = self::formatterArray($price, $currency, $rounded);

        if($nounits) {
            return $resonse["price_not_formatted"];
        }

        return $resonse["price"];
    }


    #[Pure] public static function roundUpToAny($n, $x=0): float|int
    {
        if($x > 0){
            try{
                $result = (round($n)%$x === 0) ? round($n) : round(($n+$x/2)/$x)*$x;
            }catch (\Throwable $e){
                $result = $n;
            }
        }else{
            $result = $n;
        }


        return $result;
    }


    public static function number2farsi($srting)
    {
        $western_arabic = array('0','1','2','3','4','5','6','7','8','9');
        $eastern_arabic = array('٠','١','٢','٣','٤','٥','٦','٧','٨','٩');

        return str_replace($western_arabic, $eastern_arabic, $srting);

    }

}
