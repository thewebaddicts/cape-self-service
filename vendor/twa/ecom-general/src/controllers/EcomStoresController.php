<?php

namespace twa\ecomgeneral\controllers;

use App\Http\Controllers\Controller;
use twa\apilibs\traits\APITrait;
use twa\ecomgeneral\models\EComPaymentMethodsModel;
use twa\ecomgeneral\models\EcomScheduledDelivery;
use twa\ecomgeneral\models\EComShipppingMethodsModel;
use twa\ecomgeneral\models\EcomStoreModel;
use twa\ecomprofile\models\EcomCountriesModel;
use twa\ecomprofile\models\EcomStatesModel;
use twa\ecomprofile\models\EcomUsersAddressesModel;

class EcomStoresController extends Controller
{
    use APITrait;

    public function getByID($values = []){

        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ 'storeID' ]);
        if($Check!==true){ return $Check; }

        
        $Stores = EcomStoreModel::select("*");
        if(env('CMS_LANGUAGES_MODE') == "translated"){
            $Stores = $Stores->selectRaw('group_'.app()->getLocale().' AS `group`, header_message_'.app()->getLocale().' AS header_message, label_'.app()->getLocale().' AS label ');
        }

        $Stores = $Stores->where('cancelled',0)->where('id',$values->get('storeID'))->orderBy('orders','ASC')->first();
        if(!$Stores){
            return self::APIDisplayCustomResponse(0,  "error",  "not found", "",  "The Store with ID = ".$values->get("storeID")." was not found.");
        }

        return collect($Stores)->recursive();
    }


    public function getByPrefix($values = []){

        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ 'webPrefix' ]);
        if($Check!==true){ return $Check; }

        $Stores = EcomStoreModel::select("*");
        if(env('CMS_LANGUAGES_MODE') == "translated"){
            $Stores = $Stores->selectRaw('group_'.app()->getLocale().' AS `group`, header_message_'.app()->getLocale().' AS header_message, label_'.app()->getLocale().' AS label ');
        };

        $Stores = $Stores->where('cancelled',0)->where('web_prefix',$values->get('webPrefix'))->orderBy('orders','ASC')->first();
        if(!$Stores){
            return self::APIDisplayCustomResponse(0,  "error",  "not found", "",  "The Store with prefix = ".$values->get("webPrefix")." was not found.");
        }

        return collect($Stores)->recursive();
    }

    public function list($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ ]);
        if($Check!==true){ return $Check; }


        $Stores = EcomStoreModel::select("*");
        if(env('CMS_LANGUAGES_MODE') == "translated"){
            $Stores = $Stores->selectRaw('group_'.app()->getLocale().' AS `group`, header_message_'.app()->getLocale().' AS header_message, label_'.app()->getLocale().' AS label ');
        };
        $Stores = $Stores->where('cancelled',0)->orderBy('orders','ASC')->get();

        $Stores = self::ProcessAttributes($Stores,$values);

        return collect($Stores)->recursive();
    }


    public function find($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ 'web_prefix' ]);
        if($Check!==true){ return $Check; }

        $Stores = EcomStoreModel::select("*");
        if(env('CMS_LANGUAGES_MODE') == "translated"){
            $Stores = $Stores->selectRaw('header_message_'.app()->getLocale().' AS header_message, label_'.app()->getLocale().' AS label ');
        };
        $Stores = $Stores->where('cancelled',0)->where('web_prefix',$values->get('web_prefix'))->orderBy('orders','ASC')->first();

        if(!$Stores){
            return $this->APIDisplayCustomResponse(0,  "error",  "not found", "",  "The Store with ID = ".$values->get("storeID")." was not found.");
        }

        return collect($Stores)->recursive();
    }

    public function getPaymentMethods($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[  ],[ 'store' ]);
        if($Check!==true){ return $Check; }

        $PaymentMethods = EComPaymentMethodsModel::select("*");
        if(env('CMS_LANGUAGES_MODE') == "translated"){
            $PaymentMethods = $PaymentMethods->selectRaw('label_'.app()->getLocale().' AS label, description_'.app()->getLocale().' AS description ');
        };
        $PaymentMethods = $PaymentMethods->where('ecom_stores_id',$values->get('store'))->where('cancelled',0)->orderBy('orders')->get();
        return response(collect($PaymentMethods),200);
    }

    public function getPaymentMethod($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ 'paymentMethodID' ],[ 'store' ]);
        if($Check!==true){ return $Check; }

        $PaymentMethods = EComPaymentMethodsModel::select("*");
        if(env('CMS_LANGUAGES_MODE') == "translated"){
            $PaymentMethods = $PaymentMethods->selectRaw('label_'.app()->getLocale().' AS label, description_'.app()->getLocale().' AS description ');
        };
        $PaymentMethods = $PaymentMethods->where('ecom_stores_id',$values->get('store'))->where('id',$values->get('paymentMethodID'))->where('cancelled',0)->orderBy('orders')->first();
        if(!$PaymentMethods){ return $this->APIDisplayCustomResponse(0,  "error",  "not found", "",  "paymennt method not found"); }
        return response(collect($PaymentMethods),200);
    }

    public function getShippingMethods($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[  ],[ 'store' ]);
        if($Check!==true){ return $Check; }

        $Country = false;
        if($values->get('addressID') && $values->get('addressID') != "false"){
            $Address = EcomUsersAddressesModel::where('id',$values->get('addressID'))->where('cancelled',0)->first();
            if($Address){
                $Country = $Address['countries_id'];
            }
        }elseif($values->get('countryID') && $values->get('countryID') != "false"){
            $Country = EcomCountriesModel::where('code',$values->get('countryID'))->where('cancelled',0)->first();
            $Country = $Country['id'];
        }

        $ShippingMethod = EComShipppingMethodsModel::select("*");
        if(env('CMS_LANGUAGES_MODE') == "translated"){
            $ShippingMethod = $ShippingMethod->selectRaw('label_'.app()->getLocale().' AS label, description_'.app()->getLocale().' AS description ');
        };
        $ShippingMethod = $ShippingMethod->where('ecom_stores_id',$values->get('store'));

        if($Country){
            $ShippingMethod = $ShippingMethod->where(function($query) use ($Country){
                $query->orwhereNULL('available_countries_id');
                $query->orWhere('available_countries_id','');
                $query->orWhere('available_countries_id','[]');
                $query->orWhere('available_countries_id','LIKE','%"'.$Country.'"%');
            });
        }


        $ShippingMethod = $ShippingMethod->orderBy('orders')->where('cancelled',0)->get()->map(function($row) use ($values){
        $row->charge_extra_formatted = \twa\ecomgeneral\controllers\EComCurrencyController::formatter($row->charge_extra, EComCurrencyController::getPreferredCurrency(EcomStoreModel::select("*")->where('id',$values->get('store'))->first(),isset(request()->user->user) ? request()->user->user : NULL ,$values)); return $row;
        });
        return response(collect($ShippingMethod),200);
    }

    public function getShippingMethod($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ 'shippingMethodID' ],[ 'store' ]);
        if($Check!==true){ return $Check; }

        $ShippingMethod = EComShipppingMethodsModel::select('*')->selectRaw('CONCAT(charge_extra,"USD") AS charge_extra_formatted');
        if(env('CMS_LANGUAGES_MODE') == "translated"){
            $ShippingMethod = $ShippingMethod->selectRaw('label_'.app()->getLocale().' AS label, description_'.app()->getLocale().' AS description ');
        };
        $ShippingMethod = $ShippingMethod->where('id',$values->get('shippingMethodID'))->where('ecom_stores_id',$values->get('store'))->orderBy('orders')->where('cancelled',0)->first();
        if(!$ShippingMethod){ return $this->APIDisplayCustomResponse(0,  "error",  "not found", "",  "shipping method not found"); }
        return response(collect($ShippingMethod),200);
    }

    public function listGroupedByRegion($values = []){

        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ ]);
        if($Check!==true){ return $Check; }
        $regions = EcomStoreModel::select("*")->where('published',1)->where('show_menu',1);
        if(env('CMS_LANGUAGES_MODE') == "translated"){
            $regions = $regions->selectRaw('COALESCE(NULLIF(group_'.app()->getLocale().',""),group_en) AS `group`, header_message_'.app()->getLocale().' AS header_message, label_'.app()->getLocale().' AS label ');
        };
        $regions = $regions->where('cancelled',0)->orderBy('orders','ASC');

        if(env('CMS_LANGUAGES_MODE') == "translated"){
            $regions = $regions->whereNotNull('group_en');
        }else{
            $regions = $regions->whereNotNull('group');
        }


        $regions = $regions->get();
        if(env('CMS_LANGUAGES_MODE') == "translated"){
            $regions = $regions->groupBy('group_en');
        }else{
            $regions = $regions->groupBy('group');
        }

        $regions_arr=[];
        if(isset($regions) && sizeof($regions)>0){
            foreach ($regions as $key=>$value) {
                $items_arr=[];
                $i=0;
                foreach ($value as $store){
                    $store_label = $store->label;
                    $sub_stores = explode('/', $store->label);
                    $sub_stores_arr = [];
                    $found_parent=false;

                    if(isset($store->redirection_link)){
                        $redirection_link = $store->redirection_link;
                    }else{
                        $redirection_link = false;
                    }

                    if (isset($sub_stores) && sizeof($sub_stores) > 1 && trim($sub_stores[0]!="") && trim($sub_stores[1]!="")) {
                        $store_label = $sub_stores[0];
                        if (sizeof($items_arr) > 0) {
                            foreach ($items_arr as $key2 => $old_store) {
                                if (trim(strtolower($old_store['label'])) == trim(strtolower($store_label))) {
                                    $found_parent=true;
                                    $old_store_found_id=$old_store['store_id'];
                                    $sub_stores_arr=$old_store['items'];
                                    $items_arr[$key2]['items'][]=['store_id' => $store->id, 'label' => $sub_stores[1], 'web_prefix' => $store->web_prefix, 'default_language' => $store->default_language, 'link' => '/' . $store->web_prefix . '/' . $store->default_language, 'redirection_link' => $redirection_link ];
                                }
                            }
                        }

                        if(!$found_parent) {
                            $sub_stores_arr[] = ['store_id' => $store->id, 'label' => $sub_stores[1], 'web_prefix' => $store->web_prefix, 'default_language' => $store->default_language, 'available_languages' => $store->languages, 'link' => '/' . $store->web_prefix . '/' . $store->default_language , 'redirection_link' => $redirection_link];
                        }
                    }

                    if(!$found_parent && isset($store_label) && $store_label!=NULL && trim($store_label)!="") {
                        $store_info = ['store_id' => $store->id, 'label' => $store_label, 'web_prefix' => $store->web_prefix, 'default_language' => $store->default_language,  'available_languages' => $store->languages, 'link' => '/' . $store->web_prefix . '/' . $store->default_language, 'items' => $sub_stores_arr , 'redirection_link' => $redirection_link ];
                        $items_arr[] = $store_info;
                    }

                }
                $regions_arr[]=[
                    "region_name"=>$key,
                    "items" =>$items_arr
                ];
            }
        }
        $Stores = self::ProcessAttributes($regions_arr,$values);
        return collect($regions_arr)->recursive();
    }

    public function getScheduledDeliveryDates($values = []){

        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,['state','storeID']);

        if($Check!==true){ return $Check; }

        $store = EcomStoreModel::select("*");
        if(env('CMS_LANGUAGES_MODE') == "translated"){
            $store = $store->selectRaw('group_'.app()->getLocale().' AS `group`, header_message_'.app()->getLocale().' AS header_message, label_'.app()->getLocale().' AS label ');
        }

        $store = $store->where('cancelled',0)->where('id',$values->get('storeID'))->orderBy('orders','ASC')->first();
        if(!$store){
            return self::APIDisplayCustomResponse(0,  "error",  "not found", "",  "The Store with ID = ".$values->get("storeID")." was not found.");
        }

        if(isset($store) && isset($store->scheduled_delivery_id)){
            $schedule = getter('ecom_scheduled_delivery')->condition('id',$store->scheduled_delivery_id)->get();
            $country = EcomCountriesModel::where('cancelled',0)->where('id',$schedule->country_id)->get();

            $interval = '+1 month';

            if(isset($schedule->max_delivery_date)){
                $interval = $schedule->max_delivery_date;
            }

            if(isset($country->timezone)){
                try{
                    date_default_timezone_set($country->timezone);
                }catch (\Throwable $e){
                }
            }

            //TODO check country id
            $state = EcomStatesModel::where('cancelled',0)->where('code', $values->get('state'))->where('countries_id',$schedule->country_id)->first();

            if(!$state){
                return response()->json(['error'=>true ,'message' =>'Invalid State in Scheduled Delivery']);
            }

            //get Current Date
            $current_day = strtolower(date('l'));

            if(!isset($schedule->{$current_day})){
                return response()->json(['success'=>true ,'replace'=>false]);
            }

            $states_grouped = $schedule->{$current_day}->groupBY('state');

            if(!isset($states_grouped[$state->id])){
                return response()->json(['success'=>true ,'replace'=>false]);
            }

            $scheduled_dates = $states_grouped[$state->id];
            if(count($scheduled_dates) <= 0){
                return response()->json(['success'=>true ,'replace'=>false]);
            }

            $time_condition = [];
            //We check if there's a condition on time
            foreach ($scheduled_dates as $array){
                if(isset($array->start_time) || isset($array->end_time)){
                    array_push($time_condition , $array);
                }
            }

            $time_condition = collect($time_condition);

            $arrayOfDays =[];
            if(count($time_condition) >0){

                $current_time = date("H:i");
                $date1 = \DateTime::createFromFormat('H:i', $current_time);
                $i=0;
                foreach ($time_condition as $condition){
                    $i++;
                    $date2 = \DateTime::createFromFormat('H:i', $condition->start_time);
                    $date3 = \DateTime::createFromFormat('H:i', $condition->end_time);

                    if($date1 > $date2 && $date1 < $date3){
                        $delivery_status = $condition->delivery_status;
                    }
                }

            }else{
                $delivery_status = $scheduled_dates[0]->delivery_status;
            }

            if(!isset($delivery_status)){
                $delivery_status = 'same_day';
            }

            if($delivery_status == 'same_day' || $delivery_status == 'next_day'){
                //Get all days with today
                $start_date = date('d-m-Y');
                $start_time = strtotime($start_date);
                if ($delivery_status == 'next_day'){
                    $start_time = strtotime("+1 day", $start_time);
                }

                $end_time = strtotime($interval, $start_time);

                for($i=$start_time; $i<$end_time; $i+=86400)
                {
                    if($values->get('mobile')){
                        $date = date('Y-m-d H:i:s' , $i);
                    }else{
                        $date =date('d-m-Y', $i);
                    }

                    $day =  date('l',strtotime($date));
                    $list[$date] = [
                        'date'=> $date,
                        'day' =>$day
                    ];

                }
                $list = collect($list);

                $daysList = $list->groupBy('day');

                foreach ($daysList as $key => $value){
                    $valid_key = true;
                    //Check eza fi condition 3a he lday:
                    if(strtolower($key) != $current_day){

                        $conditions = $schedule->{strtolower($key)}->where('state',$state->id);
                        if($conditions){
                            $same_day = $conditions->where('delivery_status','same_day');
                            $next_day = $conditions->where('delivery_status','next_day');
                            if(count($same_day) == 0 && count($next_day) == 0){
                                $list = $list->whereNotIn('day', $key);
                            }
                        }

                    }
                }

                foreach ($list as $key => $value){
                    array_push($arrayOfDays , $key);
                }

            }else{
                //Get Only specific days
                // loop over the next 4 weeks to find Days
                for($i=1; $i<=4; $i++){
                    if($values->get('mobile')){
                        $new_date =  date("Y-m-d H:i:s", strtotime('+'.$i.$delivery_status));
                        if($new_date != date('Y-m-d H:i:s')){
                            array_push($arrayOfDays , $new_date);
                        }
                    }else{
                        $new_date =  date("d-m-Y", strtotime('+'.$i.$delivery_status));
                        if($new_date != date('d-m-Y')){
                            array_push($arrayOfDays , $new_date);
                        }
                    }

                }
            }

            //Return array of dates
            return response()->json(['success'=>true ,'replace'=>true ,'listOfDays' => $arrayOfDays]);

        }else{
            return response()->json(['error'=>true ,'replace'=>false ,'message' =>'Scheduled delivery does not exist on this store']);
        }


    }

}
