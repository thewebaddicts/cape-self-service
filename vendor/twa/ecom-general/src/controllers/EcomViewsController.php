<?php

namespace twa\ecomgeneral\controllers;

use App\Http\Controllers\Controller;
use twa\apilibs\traits\APITrait;
use twa\ecomcart\models\EcomPurchasesModel;

class EcomViewsController extends Controller
{
    use APITrait;

    public function getWaybill($id,$values = []){

        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ '' ]);
        if($Check!==true){ return $Check; }
        $Purcase = EcomPurchasesModel::where('id',$id)->where('cancelled',0)->first();
        if(!$Purcase){
            return 'purchase was not found';
        }

        if($Purcase->waybill_link == NULL || $Purcase->waybill_link == ''){
            return 'purchase does not have any waybill yet';
        }

        return redirect($Purcase->waybill_link);

    }

}
