<?php

namespace twa\ecomgeneral\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use twa\apilibs\traits\APITrait;
use twa\ecomgeneral\models\FrontEndMenuBuilderModel;
use twa\ecomgeneral\models\FrontEndMenuLinksModel;
use App\Http\Repositories\API;
use App\MenuItemModel;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class FrontEndMenuBuilder extends Controller
{
    use APITrait;
    protected $translatables;
    protected $queryString;


    function __construct(){
        $this->translatables = [
            ((new FrontEndMenuLinksModel)->getTable()) => [ "label" => "label", "link" => "link" ]
        ];

        foreach ($this->translatables AS $key => $translatables){
            foreach ($translatables AS $key2 => $translatable){
                $newColumn = $translatable."_".app()->getLocale();
                if(\Schema::hasColumn($key, $newColumn)){
                    $translatables[$key2] = $newColumn;
                }
            }
            $this->translatables[$key] = $translatables;
        }
        $string = "";
        foreach ($this->translatables AS $row){
            $strings = [];
            foreach ($row AS $key => $column){
                $strings[] = $column.' AS '.$key;
            }
            $this->queryString[] = implode(', ',$strings);
        }
    }

    public function render($id,$token){
        $menu = FrontEndMenuBuilderModel::where('id',$id)->where('cancelled',0)->first();

        $links = FrontEndMenuLinksModel::select('*');
        if(env('CMS_LANGUAGES_MODE') == "translated"){
            $links = $links->selectRaw('label_'.app()->getLocale().' AS label, link_'.app()->getLocale().' AS link');
        };
        $links = $links->selectRaw('"1" AS image')->where('00_cms_utilities_frontend_menus_id',$menu->id)->where('cancelled',0)->get();
        $structured = self::getLinksStructured($menu,$links);
        return view('pages.utilities.frontend_menu.home',[ 'structured' => $structured, 'menu' => $menu, "id" => $id, "token" => $token, "links" => $links ]);
    }

    public function get(){
        $Check = $this->CheckRequiredVariables([ 'frontEndMenuID' ]);
        if($Check!==true){ return $Check; }
        return FrontEndMenuLinksModel::select('*')->selectRaw($this->queryString[0])->selectRaw('"1" AS image')->where('id',request()->input('frontEndMenuID'))->where('cancelled',0)->first();
    }


    public function store($id,$token){
        $menu = FrontEndMenuBuilderModel::where('id',$id)->where('cancelled',0)->first();

        $links = FrontEndMenuLinksModel::select('*');
        if(env('CMS_LANGUAGES_MODE') == "translated"){
            $links = $links->selectRaw('label_'.app()->getLocale().' AS label, link_'.app()->getLocale().' AS link');
        };
        $links = $links->selectRaw('"1" AS image')->where('00_cms_utilities_frontend_menus_id',$menu->id)->where('cancelled',0)->get();

        $structured = self::getLinksStructured($menu,$links);
        $menu->fields = request()->input('nestablefield');
        $menu->save();

        $flag = MenuItemModel::where('script_id',53)->where('cancelled',0)->first();

        return redirect('/cms/grid/53/'.$flag->id.'/front_end_menus?notification=Your%20record%20has%20been%20successfully%20updated&notification_status=success');
    }

    public function getLinksStructured($menu,$links){
        $menuArray = [];
        $UsedReturn = [];
        $UnusedReturn = [];

        foreach ($links AS $link){
            $menuArray[$link->id] = [ "id" => $link->id , "data" => $link, "children" => [] ];
        }

        try{
            $structure = json_decode($menu->fields,1);
        }catch (\Throwable $e){
            $structure = [];
        }

        try{
            $count = count($structure);
        }catch (\Throwable $e){
            $count = -1;
        }

        if($count==0 || $count == -1){
            $structure = [];

        }else{
            foreach ($structure AS $key => $structureItem) {
                if(isset($menuArray[$structureItem['id']]) && isset($menuArray[$structureItem['id']]['data'])){ $structure[$key]['data'] = $menuArray[$structureItem['id']]['data']; }else{ continue; }
                unset($menuArray[$structureItem['id']]);
                if(array_key_exists("children",$structureItem) && count($structureItem['children'])){
                    $childrenReturn = self::processStructureChildren($structureItem['children'],$menuArray);
                    $structure[$key]['children'] = $childrenReturn[0];
                    $menuArray = $childrenReturn[1];
                }else{
                    $structure[$key]['children'] = [];
                    unset($menuArray[$structureItem['id']]);
                }
            }
        }

        foreach ($menuArray AS $item){
            $UnusedReturn[] = $item;
        }

        return [$structure,$UnusedReturn];

    }


    public function processStructureChildren($structureItem,$menuArray){
        foreach ($structureItem AS  $key => $children){
            if(array_key_exists("children",$children) && count($children['children'])){
                $structureItem[$key]['processed'] = 1;
                if(!array_key_exists($children['id'],$menuArray)){ continue; }
                $structureItem[$key]['data'] = $menuArray[$children['id']]['data'];

                unset($menuArray[$children['id']]);
                $childrenReturn = self::processStructureChildren($children['children'],$menuArray);
                $structureItem[$key]['children'] = $childrenReturn[0];
                $menuArray = $childrenReturn[1];
            }else{
                if(!array_key_exists($children['id'],$menuArray)){ continue; }
                $structureItem[$key]['data'] = $menuArray[$children['id']]['data'];
                $structureItem[$key]['processed'] = 2;
                unset($menuArray[$children['id']]);
            }
        }
        return [$structureItem,$menuArray];
    }




    public function renderAPI($mode,$values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ 'menulabel' ]);
        if($Check!==true){ return $Check; }

        switch (strtolower($mode)){
            case "json":
                return $this->renderMenuJson($values->get('menulabel'));
                break;
            case "html":
                return $this->renderMenuHtml($values->get('menulabel'),$values->get('listTag','ul'),$values->get('listClass'),$values->get('labelClass'),$values->get('itemClass'),$values->get('labelTag','a'),$values->get('encloseLI',true),$values->get('encloseLIClass','encloureClass'),false,$values->get('nonEcomLinkPrefix',""));
                break;
            default:
                return $this->APIDisplayCustomResponse(0, "error", "Wrong output type specified", "The output type in this API can only be a JSON or HTML", "", false);
        }
    }

    public function renderMenuJson($menuID){
        if(!isset(request()->store['id'])){ return false; }
        $menu = FrontEndMenuBuilderModel::where('label',$menuID)->where('ecom_stores_id','LIKE','%"'.request()->store['id'].'"%')->where('cancelled',0)->first();
        if(!$menu){
            $menu = FrontEndMenuBuilderModel::where('label',$menuID)->where('cancelled',0)->first();
        }
        $links = FrontEndMenuLinksModel::select('*');
        if(env('CMS_LANGUAGES_MODE') == "translated"){
            $links = $links->selectRaw('label_'.app()->getLocale().' AS label, link_'.app()->getLocale().' AS link');
        };

        try {
            $data_url = config('ecom')['endpoints']['storage_url'];
        } catch (\Throwable $th) {
            $data_url = env('DATA_URL');
        }

        try{
            $links = $links->where('00_cms_utilities_frontend_menus_id',$menu->id)
                ->selectRaw("CONCAT('".$data_url."/front_end_menus/', id , '.', extension_image,'?v=' , version) AS image")
                ->where('cancelled',0)->get();
        }catch (\Throwable $e){
            dd([$menuID,$e]);
            dd($menuID);
        }

        return $this->getLinksStructured($menu,$links)[0];
    }


    public function renderMenuHtml($menuID,$tag = 'ul',$ListClass = "",$LabelClass = "",$ItemClass = "item", $LabelTag = "a",$encloseLI = true, $encliseLiClass = "", $ecomLinkStructure = false, $nonEcomLinkPrefix = ""){
        if($encloseLI == 1){ $encloseLI = true; }
        $json = $this->renderMenuJson($menuID);
        $html = '<'.$tag.' class="'.$ListClass.' level-1">';
        $index = 2;

        foreach ($json AS $item){
            if(!isset($item['data'])){ continue; }
//            if($item['data']->link_to_ecom == 1){
            if($item['data']->mode != "static link"){
                $keywordsArr = ($item['data']->keywords);
                if(!$keywordsArr || !is_array($keywordsArr)){
                    $keywordsArr = [];
                }
//                $link = str_replace(['{id}','{slug}'],[ $item['data']->id,  Str::slug($item['data']->label,'_')],$ecomLinkStructure);
                $link = ecom('url')->prefix().'/products/'.$item['data']->slug;
            }else{
                if(substr($item['data']->link,0,1) == '/'){
                    $link = $nonEcomLinkPrefix.$item['data']->link;
                }else{
                    $link = $item['data']->link;
                }
            }
            $extraClass = '';
            if($item['data']['image']!=NULL && $item['data']['image']!=null){
                $extraClass = "imageLabel";
            }
            $html .= '<li class="'.$ItemClass.'" data-id="'.$item['id'].'"><'.$LabelTag.' href="'.$link.'"  target="'.$item['data']->target.'" class="'.$LabelClass.' '.$extraClass.'"';
            if($item['data']['image']!=NULL && $item['data']['image']!=null){
                $html .= ' style = "background-image:url('.$item['data']['image'].')"';
            }
            $html .= '>';
            if($item['data']['image']==NULL || $item['data']['image']==null){
                $html .= $item['data']->label;
            }
            $html .= '</'.$LabelTag.'>';
            $html .= $this->renderMenuHtmlChild($item['children'], $index , $tag,$ListClass,$LabelClass,$ItemClass,$LabelTag,$encloseLI,$encliseLiClass,$ecomLinkStructure, $nonEcomLinkPrefix);
            $html .= '</li>';
        }
        $html .= '</'.$tag.'>';
//        return $html;
        return ["html" => $html];
    }

    public function renderMenuHtmlChild($childrens, $index ,$tag = 'ul',$ListClass = "",$LabelClass = "",$ItemClass = "item", $LabelTag = "a",$encloseLI = false, $encliseLiClass = "", $ecomLinkStructure = false , $nonEcomLinkPrefix = ""){
        $html = '';
        if(is_array($childrens) && count($childrens)>0){
            $html .= '<'.$tag.' class="'.$ListClass.' level-m level-'.$index.'">';
            if($encloseLI === true){ $html .= '<div class="'.$encliseLiClass.' enclosed">'; }
            foreach($childrens AS $children){
                if(!array_key_exists("data",$children)){ continue; }
//                if($children['data']->link_to_ecom == 1){
                if($children['data']->mode != "static link"){
                    if(!$children['data']->keywords || !is_array($children['data']->keywords)){
                        $children['data']->keywords = [];
                    }
//                    $link = str_replace(['{id}','{slug}'],[ $children['data']->id, Str::slug($children['data']->label,'_')],$ecomLinkStructure);
                    $link = ecom('url')->prefix().'/products/'.$children['data']->slug;
                }else{
                    if(!str_contains($children['data']->link,'javascript')  && !str_contains($children['data']->link,'http://')  && !str_contains($children['data']->link,'https://')){
                        $link = $nonEcomLinkPrefix.$children['data']->link;
                    }else{
                        $link = $children['data']->link;
                    }
                }
                if($children['data']["image"] != NULL){
                    $children['data']->label = '<img loading=lazy src="'.$children['data']["image"].'" />';
                }
                $html .= '<li class="'.$ItemClass.'" data-id="'.$children['id'].'"><'.$LabelTag.'   href="'.$link.'"  target="'.$children['data']->target.'" class="'.$LabelClass.'">'.$children['data']->label.'</'.$LabelTag.'>';
                if(array_key_exists("children",$children) && is_array($children['children']) && count($children['children'])>0){
                    $html .= $this->renderMenuHtmlChild($children['children'], $index+1 ,$tag,$ListClass,$LabelClass,$ItemClass,$LabelTag,$encloseLI,$encliseLiClass,$ecomLinkStructure,$nonEcomLinkPrefix);
                }
                $html .= '</li>';
            }
            if($encloseLI === true){ $html .= '</div>'; }
            $html .= '</'.$tag.'>';
        }
        return $html;
    }


    public static function findChildren($id,$MenuObject,$count = 0){
        $count = $count + 1;
        $return = false;
        foreach ($MenuObject AS $object){
            if(isset($object['id']) && $object['id']==$id){
                if(isset($object["children"])){ $return = $object["children"]; }else{ $return = []; }
            }
        }
        if(!$return){
            $newArray = collect($MenuObject)->pluck('children')->flatten(1)->filter();
            if(collect($newArray)->count() == 0 || $count >= 20){
                return false;
            }else{
                return self::findChildren($id,$newArray,$count);
            }

        }else{
            return $return;
        }
    }

}
