<?php

namespace twa\ecomgeneral\controllers;

use App\Http\Controllers\Controller;
use twa\apilibs\traits\APITrait;
use twa\ecomgeneral\models\EComPaymentMethodsModel;
use twa\ecomgeneral\models\EComShipppingMethodsModel;
use twa\ecomgeneral\models\EcomStoreLocationsModel;
use twa\ecomgeneral\models\EcomStoreModel;

class EcomStoresLocationsController extends Controller
{
    use APITrait;

    public function getByID($values = []){

        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ 'id' ]);
        if($Check!==true){ return $Check; }

        $StoreLocations = EcomStoreLocationsModel::select("*");
        if(env('CMS_LANGUAGES_MODE') == "translated"){
            $StoreLocations = $StoreLocations->selectRaw('label_'.app()->getLocale().' AS label, address_'.app()->getLocale().' AS address');
        };
        $StoreLocations = $StoreLocations->where('cancelled',0)->where('id',$values->get('id'))->where('ecom_stores_id',$values->get('store'))->orderBy('orders','ASC')->first();
        if(!$StoreLocations){ return self::APIDisplayCustomResponse('0','error','not found','not found'); }
        $StoreLocations = self::ProcessAttributes($StoreLocations,$values);

        return collect($StoreLocations)->recursive();
    }



    public function list($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ ]);
        if($Check!==true){ return $Check; }

        $StoreLocations = EcomStoreLocationsModel::select("*");
        if(env('CMS_LANGUAGES_MODE') == "translated"){
            $StoreLocations = $StoreLocations->selectRaw('label_'.app()->getLocale().' AS label, address_'.app()->getLocale().' AS address');
        };
        $StoreLocations = $StoreLocations->where('cancelled',0)->where('ecom_stores_id',$values->get('store'))->orderBy('orders','ASC')->get();
        $StoreLocations = self::ProcessAttributes($StoreLocations,$values);

        return collect($StoreLocations)->recursive();
    }

}
