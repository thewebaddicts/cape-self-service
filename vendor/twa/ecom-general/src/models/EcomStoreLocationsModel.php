<?php

namespace twa\ecomgeneral\models;

use App\Http\Repositories\GeneralFunctions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use twa\ecomgeneral\models\EComCurrencyModel;

class EcomStoreLocationsModel extends Model
{
    use HasFactory;
    protected $table = "ecom_stores_locations";

}

