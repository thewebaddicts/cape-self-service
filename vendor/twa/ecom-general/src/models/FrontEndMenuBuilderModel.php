<?php

namespace twa\ecomgeneral\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FrontEndMenuBuilderModel extends Model
{
    use HasFactory;
    protected $table = "00_cms_utilities_frontend_menus";

    public function getDepthAttribute($value){
        if(!$value || $value == NULL || $value == 0){
            return "3";
        }else{
            return $value;
        }
    }
}
