<?php

namespace twa\ecomgeneral\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EComShipppingMethodsModel extends Model
{
    use HasFactory;
    protected $table = "ecom_stores_delivery";
    protected $casts = [
        "cms_attributes" => "array",
        'shipping_fees_ranges' => "array",
        'pricing_per_state' => "array",
        ];
}
