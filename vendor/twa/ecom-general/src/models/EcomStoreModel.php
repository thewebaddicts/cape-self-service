<?php

namespace twa\ecomgeneral\models;

use App\Http\Repositories\GeneralFunctions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use twa\ecomgeneral\models\EComCurrencyModel;
use twa\ecomprofile\models\EcomCountriesModel;
use Illuminate\Support\Str;

class EcomStoreModel extends Model
{
    use HasFactory;
    protected $table = "ecom_stores";
    protected $casts = [ 'languages' => 'array', 'cms_attributes' => 'array', 'countries_id' => 'array', 'email_notifications' => 'array', 'store_managers' => 'array', 'operation_managers' => 'array', 'ga_tracking' => 'array',  'ga_tracking_old' => 'array' ];
    protected $appends = [ 'ecom_currencies_id_object', 'countries' ];

    public function getEcomCurrenciesIDAttribute($value){
        $value = json_decode($value,true);
        if(!$value){ $value = []; }
        $AvailableCurrencies = collect($value)->push((string)$this->attributes['default_ecom_currencies_id'])->unique();
        return $AvailableCurrencies;
    }

    public function getEcomCurrenciesIDObjectAttribute($value){
        $value = json_decode($this->ecom_currencies_id,true);
        if(!$value){ $value = []; }
        $AvailableCurrencies = collect($value)->push((string)$this->attributes['default_ecom_currencies_id'])->unique();
        $AvailableCurrencies = \twa\ecomproducts\models\EComCurrencyModel::whereIn('id',$AvailableCurrencies)->where('cancelled',0)->get();
        return $AvailableCurrencies;
    }

    public function getFlagCountriesIDAttribute($value){
       return EcomCountriesModel::where('id',$value)->where('cancelled',0)->first();
    }



    public function getWebPrefixAttribute($value){
        return Str::slug($value);
    }


    public function getCountriesAttribute($value){
        if(isset($this->countries_id)){
            return EcomCountriesModel::whereIn('id',$this->countries_id)->where('cancelled',0)->get();
        }else{
            return false;
        }
    }


}

