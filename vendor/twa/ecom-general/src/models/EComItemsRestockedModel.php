<?php

namespace twa\ecomgeneral\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EComItemsRestockedModel extends Model
{
    use HasFactory;
    protected $table = "ecom_notify_users_items_restocked";

}
