<?php

namespace twa\ecomgeneral\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EComCurrencyModel extends Model
{
    use HasFactory;
    protected $table = "ecom_currencies";

}
