<?php

namespace twa\ecomgeneral\models;

use Illuminate\Database\Eloquent\Model;

class EmailTemplateModel extends Model
{
    protected $table = "email_templates";
}
