<?php

namespace twa\ecomgeneral\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FrontEndMenuLinksModel extends Model
{
    use HasFactory;
    protected $table = "00_cms_utilities_frontend_menus_links";

    protected $casts = [
        'cms_attributes' => 'array',
        'keywords' => 'array',
        'ecom_products_filters_id' => 'array',
        'ecom_subcategories_id' => 'array'
    ];
    public function getTargetAttribute($value){
        if(!$value || $value == NULL){
            return "_self";
        }else{
            return $value;
        }
    }
    public function getImageAttribute(){
        if($this->extension_image != null){
            return env('DATA_URL').'/front_end_menus/'.$this->id.'.'.$this->extension_image;
        }else{
            return null;
        }
    }
}
