<?php

namespace twa\ecomgeneral\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EComPaymentMethodsModel extends Model
{
    use HasFactory;
    protected $table = "ecom_stores_payment_methods";
}
