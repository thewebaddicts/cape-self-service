<?php

namespace twa\ecomgeneral\facades;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class EcomCurrenciesFacade extends Controller
{

    public function switch(){
        $change = (new \twa\ecomgeneral\controllers\EComCurrencyController)->switch([ 'currencyID' => request()->input('currency'), 'store' => request()->store['id'] ]);
        if(isset($_SERVER['HTTP_REFERER'])){
            return redirect($_SERVER['HTTP_REFERER']);
        }else{
            return redirect(route('home'));
        }
    }

}
