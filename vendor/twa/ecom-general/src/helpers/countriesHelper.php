<?php
namespace twa\ecomgeneral\helpers;

class countriesHelper
{

    private $parameters;
    private $class;

    function __construct(){
        if(isset(request()->store) && isset(request()->store['id'])){
            $this->parameters["store"] = request()->store['id'];
        }

        $this->class = new \twa\ecomprofile\controllers\EcomUsersAddressesController;
    }

//Route::post('/states/list', function () { return (new \twa\ecomprofile\controllers\EcomUsersAddressesController)->getStates(); });
//Route::post('/cities/list', function () { return (new \twa\ecomprofile\controllers\EcomUsersAddressesController)->getCities(); });
//Route::post('/phones/list', function () { return (new \twa\ecomprofile\controllers\EcomUsersAddressesController)->getPhoneCodes(); });
    public function getCountries(){
        $reposnse = $this->class->getCountries($this->parameters);
        if($reposnse->getStatusCode()==200){
            return $reposnse->getOriginalContent();
        }else{
            return false;
        }
    }

    public function getPhoneCodes(){
        $reposnse = $this->class->getPhoneCodes($this->parameters);
        if($reposnse->getStatusCode()==200){
            return $reposnse->getOriginalContent();
        }else{
            return false;
        }
    }

    public function getStates($countryCode){
        $this->parameters["country_code"] = $countryCode;
        $response = $this->class->getStates($this->parameters);
        if($response->getStatusCode()==200){
            return $response->getOriginalContent();
        }else{
            return false;
        }
    }
}
