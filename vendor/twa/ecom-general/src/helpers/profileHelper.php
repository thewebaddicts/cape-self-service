<?php
namespace twa\ecomgeneral\helpers;

class profileHelper
{

    private $parameters;
    private $userRegistration;

    function __construct(){
        if(isset(request()->store) && isset(request()->store['id'])){
            $this->parameters["store"] = request()->store['id'];
        }

        $this->userRegistration = new \twa\ecomauth\controllers\EcomUsersRegistration;
    }


    public function update(){
        $Update = $this->userRegistration->update([ "token" => request()->user->token, 'first_name' => request()->input('first_name'), 'last_name' => request()->input('last_name'), 'email' => request()->input('email'), 'phone' => request()->input('phone'),'phone_country_code' => request()->input('phone_country_code')]);
        $ResponseObject = $Update->getOriginalContent();
        if ($Update->getStatusCode() == "200") {
            return redirect()->route(config('ecom.default_routes.profile'),['notification'=> $ResponseObject["success"]["message"]]);
        } else {
            return redirect()->route(config('ecom.default_routes.profile'),['notification_title' => $ResponseObject["error"]["title"], 'notification_message'=> $ResponseObject["error"]["message"]])->withInput(request()->input())->withErrors($ResponseObject["error"]["message"]);
        }
    }
    public function changePassword(){
        $Update = $this->userRegistration->changepass([ "token" => request()->user->token, 'oldpassword' => request()->input('old_password'), 'password' => request()->input('password') ]);
        $ResponseObject = $Update->getOriginalContent();
        if ($Update->getStatusCode() == "200") {
            return redirect()->route(config('ecom.default_routes.profile'),['notification'=> $ResponseObject["success"]["message"]]);
        } else {
            return redirect()->route(config('ecom.default_routes.profile'),['notification_title' => $ResponseObject["error"]["title"], 'notification_message'=> $ResponseObject["error"]["message"]])->withInput(request()->input())->withErrors($ResponseObject["error"]["message"]);
        }
    }

    public function get(){
        return request()->user->user;
    }

    public function render(){
        return view(config('ecom.default_views.profile_edit'),[ "profile" => request()->user->user ]);
    }



}
