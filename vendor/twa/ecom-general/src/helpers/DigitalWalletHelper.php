<?php
namespace twa\ecomgeneral\helpers;

use twa\ecomcart\controllers\EcomGiftCardController;
use twa\ecomcart\controllers\EcomLoyaltyController;
use twa\ecomprofile\controllers\EcomUsersLoyaltyController;

class DigitalWalletHelper
{

    private $parameters;

    function __construct(){
        if(isset(request()->store) && isset(request()->store['id'])){
            $this->parameters["store"] = request()->store['id'];
        }
    }

    public function redeemGiftCard($number){
        return (new EcomGiftCardController())->redeemGiftCard([ "serial" => $number, "store" => ecom('stores')->getCurrent()['id'] ], request()->user['user']['id']);
    }

//    public function redeem($points){
//        return (new EcomUsersLoyaltyController())->redeem([ "value" => $points, "store" => ecom('stores')->getCurrent()['id'] ], request()->user['user']['id']);
//    }
}
