<?php
namespace twa\ecomgeneral\helpers;

use twa\ecomgeneral\models\EcomStoreModel;
use twa\ecomprofile\models\EcomCountriesModel;

class menuHelper
{

//    private $parameters;
//    private $addresses;
//    private $lastValue;

    function __construct(){
//        if(isset(request()->store) && isset(request()->store['id'])){
//            $this->parameters["store"] = request()->store['id'];
//        }
    }

    public function list(){
        return request()->menus;
    }

    public function get($menu){
        if(isset(request()->menus[$menu])){ return request()->menus[$menu]; }else{ return false; }
    }

}
