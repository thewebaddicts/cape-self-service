<?php
namespace twa\ecomgeneral\helpers;

class paymentMethodsHelper
{

    private $parameters;
    private $paymentMethods;
    private $lastValue;

    function __construct(){
        if(isset(request()->store) && isset(request()->store['id'])){
            $this->parameters["store"] = request()->store['id'];
        }

        $this->paymentMethods = new \twa\ecomgeneral\controllers\EcomStoresController;
    }

    public function list(){
        return $this->paymentMethods->getPaymentMethods($this->parameters)->getOriginalContent();
    }
}
