<?php
namespace twa\ecomgeneral\helpers;

use twa\apilibs\traits\APITrait;
use twa\ecomgeneral\models\EcomStoreModel;
use twa\ecomprofile\models\EcomCountriesModel;

class addressesHelper
{
    use APITrait;
    private $parameters;
    
    function __construct(){
        if(isset(request()->store) && isset(request()->store['id'])){
            $this->parameters["store"] = request()->store['id'];
        }

        $this->addresses = new \twa\ecomprofile\controllers\EcomUsersAddressesController;
    }

    public function condition($key,$value){
        $this->parameters[$key] = $value;
        return $this;
    }

    public function list(){
        $response = $this->addresses->list($this->parameters);
        if($response->getStatusCode() == 200){
            return $response->getOriginalContent();
        }else{
            dd($response->getOriginalContent());
        }
    }

    public function get(){
        $response = $this->addresses->get($this->parameters);
        if($response->getStatusCode() == 200){
            return $response->getOriginalContent();
        }else{
            dd($response->getOriginalContent());
        }
    }


    public function getBilling(){
        $response = $this->addresses->getBilliing();
        if($response->getStatusCode() == 200){
            return $response->getOriginalContent();
        }else{
            dd($response->getOriginalContent());
        }
    }

    public function renderList(){
        return view(config('ecom.default_views.address_listing'),[ "addresses" => $this->list() ]);
    }


    public function renderAdd(){
        $CountryCodes = (new \twa\ecomprofile\controllers\EcomUsersAddressesController)->getPhoneCodes([ "store" => request()->store['id'] ]);
        if($CountryCodes->getStatusCode() != 200){ $CountryCodes = response([],200); }

        $Countries = (new \twa\ecomprofile\controllers\EcomUsersAddressesController)->getCountries([ "store" => request()->store['id'] ]);
        if($Countries->getStatusCode() != 200){ $CountryCodes = response([],200); }
        return view(config('ecom.default_views.address_form'),[ "CountryCodes" => $CountryCodes->getOriginalContent(), "Countries" => $Countries->getOriginalContent() ]);
    }

    public function getPhoneCodes($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ ],[]);
        if($Check!==true){ return $Check; }

        $Countries = EcomCountriesModel::select('code')->selectRaw('CAST(phone_code AS UNSIGNED) AS phone_code')->where('cancelled',0)->orderBy('phone_code','ASC')->groupBy('phone_code')->get();
        return response($Countries,200);
    }

    public function getCountries($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ ],[ 'store' ]);
        if($Check!==true){ return $Check; }

        $Store = EcomStoreModel::where('id',$values->get('store'))->where('cancelled',0)->first();
        if(!$Store){
            return $this->APIDisplayResponse(42);
        }

        $Countries = EcomCountriesModel::where('cancelled',0)->orderBy('name','ASC')->where('display',1);
        if(is_array($Store->countries_id) && $Store->countries_id && count($Store->countries_id) > 0){
            $Countries = $Countries->whereIn('id',$Store->countries_id);
        }
        $Countries = $Countries->get();
        return response($Countries,200);
    }

    public function getStates($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ 'country_code' ],[ 'store' ]);
        if($Check!==true){ return $Check; }

        return (new \twa\omnidelivery\facades\OmniDeliveryFacade($values->get('store'),NULL))->getStates($values->get('country_code'));
    }

    public function getCities($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ 'country_code' ],[ 'store' ]);
        if($Check!==true){ return $Check; }
        return (new \twa\omnidelivery\facades\OmniDeliveryFacade($values->get('store'),NULL))->getCities(request()->input('country_code'),$values->get('state'));
    }

    public function validateAddress($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ 'country_code', 'city' ],[ 'store' ]);
        if($Check!==true){ return $Check; }

        $Check = TWAAramexFunctions::validateAddress(request()->input('country_code'),request()->input('city'),request()->input('postal_code'));
        if($Check["status"] == "fail"){
            return $this->APIDisplayResponse(40,". ".$Check["message"], ". The Delivery integration company returned this error: ".$Check["message"]);
        }else{
            return $this->APIDisplayResponse(41);
        }
    }
}
