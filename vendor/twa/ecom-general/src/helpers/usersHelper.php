<?php
namespace twa\ecomgeneral\helpers;


use Laravel\Socialite\Facades\Socialite;
use twa\ecomauth\models\EcomUsersVerificationChallengesModel;

class usersHelper
{

    private $parameters;
    private $mobile = false;

    function __construct(){
        if(isset(request()->store) && isset(request()->store['id'])){
            $this->parameters["store"] = request()->store['id'];
        }
    }

    public function loginCheck(){
        if(isset(request()->user['login']) && request()->user['login'] == true){
            return true;
        }else{
            return false;
        }
    }

    public function mobile(){
        $this->mobile = true;
        return $this;
    }

    public function renderLogin(){
        return view('pages.auth.login');
    }

    public function renderPin($store,$lang,$challenge,$customView = false){
        $Check = EcomUsersVerificationChallengesModel::where('challenge',$challenge)->first();
        if(!$Check){ abort(404); }
        if($customView){
            return view($customView,[ "challenge" => $challenge ]);
        }else{
            return view('pages.auth.pin',[ "challenge" => $challenge ]);
        }
    }

    public function processLogin(){
        if(!$this->mobile){
            $response =  (new \twa\ecomauth\controllers\EcomUsersRegistration)->login( [ "email" => request()->input('email'), "password" => request()->input('password') ] );
            if($response->getStatusCode() != 200){
                return \twa\ecomgeneral\controllers\HelpersController::processAPIError($response->getOriginalContent());
            }else{
                session(["login" => $response->getOriginalContent()]);
                if(request()->input('customRouteName')){
                    return redirect()->route(request()->input('customRouteName'));
                }else{
                    return redirect('/');
                }
            }
        }else{
            $challenge = (new \twa\ecomauth\controllers\EcomUsersRegistration)->loginMobile( [ "phone_country_code" => request()->input('phone_country_code'), "phone" => request()->input('phone') ] );
            if($challenge->getStatusCode() != 200){
                return \twa\ecomgeneral\controllers\HelpersController::processAPIError($challenge->getOriginalContent());
            }else{
                $response = $challenge->getOriginalContent();
                if(!isset($response["challenge"])){ return back(); }
                if(request()->input('customRouteName')){
                    return redirect()->route(request()->input('customRouteName'),[ "challenge" => $response["challenge"] ]);
                }else{
                    return redirect()->route(config('ecom.default_routes.login-pin'),[ "challenge" =>$response["challenge"] ]);
                }

            }
        }
    }


    public function processVerify($NextRouteName = false){
        if(!$this->mobile){
            //no action until now
        }else{
            $challenge = (new \twa\ecomauth\controllers\EcomUsersRegistration)->verifyMobile( [ "challenge" => request()->input('challenge'), "pin" => implode(request()->input('code')) ] );
            if($challenge->getStatusCode() != 200){
                return \twa\ecomgeneral\controllers\HelpersController::processAPIError($challenge->getOriginalContent());
            }else{
                $response = $challenge->getOriginalContent();
                if(request()->user->login === false && isset(request()->user->user['id'])){
                    //we move the offline to the online cart
                    \twa\ecomcart\controllers\EcomCartController::offlineToOnline(request()->user->user['id'], $response['id']);
                }

                session(["login" => $response]);
                if(!$NextRouteName){
                    return redirect()->intended('/');
                }else{
                    return redirect()->route($NextRouteName);
                }
            }
        }
    }



    public function updateProfile(){
        if($this->mobile){
            $update_array = [ "token" => request()->user->token ];
            $possible_updates = [ "first_name", "last_name", "email", "countries_id", "gender", "birthdate" ];
            foreach ($possible_updates AS $possible_update){
                if(request()->input($possible_update)){
                    $update_array[$possible_update] = request()->input($possible_update);
                }
            }
            $Update = (new \twa\ecomauth\controllers\EcomUsersRegistration)->updateMobile($update_array);
            $Update = $Update->getOriginalContent();
            self::reloadSession();
            return \twa\ecomgeneral\controllers\HelpersController::processAPIError($Update);
        }

    }



    public function SocialLogin($network){
        return Socialite::driver($network)->redirect();
    }



    public static function reloadSession(){
        if(session('login') && isset(session('login')->token)){
            $Profile = (new \twa\ecomauth\controllers\EcomUsersRegistration)->tokentouser([ "token" => session('login')->token]);
            session([ 'login' => collect($Profile)->recursive() ]);
        }
    }
}
