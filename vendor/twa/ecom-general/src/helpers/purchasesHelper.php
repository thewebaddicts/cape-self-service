<?php
namespace twa\ecomgeneral\helpers;

class purchasesHelper
{

    private $parameters;
    private $purchasesController;

    function __construct(){
        if(isset(request()->store) && isset(request()->store['id'])){
            $this->parameters["store"] = request()->store['id'];
        }
        $this->purchasesController = new \twa\ecomcart\controllers\EcomPurchasesController();

    }

    public function placeOrder($guest_id = false){
        if($guest_id)
            $user_id = $guest_id;
        else
            $user_id = request()->user->user['id'];

        $response = $this->purchasesController->placeOrder([ 'store' => $this->parameters['store'],'paymentMethodID' => request()->input('payment_method'),'shippingMethodID' => request()->input('shipping_method'),'shippingAddressID' => request()->input('shipping_address'),'billingAddressID' => request()->input('billing_address'), 'delivery_note' => request()->input('purchase_delivery_note'), 'date_delivery' => request()->input('delivery_date') ,'user_id'=>$guest_id]);

        try{ $responseCode = $response->getStatusCode(); }catch (\Throwable $e){ $responseCode = 200; }
        if($responseCode != 200){
            $ResponseObject = $response->getOriginalContent();
            return \twa\ecomgeneral\controllers\HelpersController::processAPIError($ResponseObject);
        }else{
            $response = collect($response->getOriginalContent());
            if(isset($response["redirect"])){
                return redirect($response["redirect"]["link"]);
            }
            if(isset($response["json"])){
                return response()->json($response , 200);
            }

            $url = config('omnipay.success.redirect',config('ecom.default_paths.order_success','/?payment=success'));
            $url = str_replace(['{store}','{lang}'],[request()->segment(1),request()->segment(2)],$url);
            if(str_contains($url,"?")){
                $url .= "&";
            }else{
                $url .= "?";
            }
            $url .= "pid=".$response['pid'];
            return redirect($url);
        }
    }


    public function get($id){
        $response = $this->purchasesController->get($id);
        return $response;
    }

    public function list(){
        $response = $this->purchasesController->list();
        return $response;
    }
}
