<?php
namespace twa\ecomgeneral\helpers;

class sectionsHelper
{

    private $parameters;

    function __construct(){
        if(isset(request()->store) && isset(request()->store['id'])){
            $this->parameters["store"] = request()->store['id'];
        }
    }

    public function condition($key,$value){
        $this->parameters[$key] = $value;
        return $this;
    }

    public function get($limit = false){
        if($limit){ $this->parameters["get"] = $limit; }
        return (new \twa\ecomproducts\controllers\EcomSectionsController)->list($this->parameters);
    }
}
