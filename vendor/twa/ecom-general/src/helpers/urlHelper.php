<?php
namespace twa\ecomgeneral\helpers;

class urlHelper
{

    private $parameters;

    function __construct(){
        if(isset(request()->store) && isset(request()->store['id'])){
            $this->parameters["store"] = request()->store['id'];
        }
    }

    public function prefix($full = false){
        $add = "";
        if($full){
            $add = request()->getSchemeAndHttpHost();
        }
        if(isset(request()->store['web_prefix'])){
            return $add.'/'.request()->store['web_prefix'].'/'.app()->getLocale();
        }else{
            return false;
        }
    }
}
