<?php
namespace twa\ecomgeneral\helpers;

class shippingMethodsHelper
{

    private $parameters;
    private $addresses;
    private $lastValue;

    function __construct(){
        if(isset(request()->store) && isset(request()->store['id'])){
            $this->parameters["store"] = request()->store['id'];
        }

        $this->addresses = new \twa\ecomgeneral\controllers\EcomStoresController;
    }

    public function list(){
        return $this->addresses->getShippingMethods($this->parameters)->getOriginalContent();
    }
}
