<?php
use Illuminate\Support\Arr;

function ecom($entity){

    $helpers = [
        [ "entity" => "collections", "class" => \twa\ecomgeneral\helpers\collectionsHelper::class, ],
        [ "entity" => "sections", "class" => \twa\ecomgeneral\helpers\sectionsHelper::class, ],
        [ "entity" => "settings", "class" => \twa\ecomgeneral\helpers\settingsHelper::class, ],
        [ "entity" => "addresses", "class" => \twa\ecomgeneral\helpers\addressesHelper::class, ],
        [ "entity" => "payment_methods", "class" => \twa\ecomgeneral\helpers\paymentMethodsHelper::class, ],
        [ "entity" => "shipping_methods", "class" => \twa\ecomgeneral\helpers\shippingMethodsHelper::class, ],
        [ "entity" => "products", "class" => \twa\ecomgeneral\helpers\productsHelper::class, ],
        [ "entity" => "cart", "class" => \twa\ecomgeneral\helpers\cartHelper::class, ],
        [ "entity" => "purchases", "class" => \twa\ecomgeneral\helpers\purchasesHelper::class, ],
        [ "entity" => "profile", "class" => \twa\ecomgeneral\helpers\profileHelper::class, ],
        [ "entity" => "menus", "class" => \twa\ecomgeneral\helpers\menuHelper::class, ],
        [ "entity" => "stores", "class" => \twa\ecomgeneral\helpers\storesHelper::class, ],
        [ "entity" => "url", "class" => \twa\ecomgeneral\helpers\urlHelper::class, ],
        [ "entity" => "countries", "class" => \twa\ecomgeneral\helpers\countriesHelper::class, ],
        [ "entity" => "users", "class" => \twa\ecomgeneral\helpers\usersHelper::class, ],
        [ "entity" => "notifications", "class" => \twa\ecomgeneral\helpers\notificationsHelper::class, ],
        [ "entity" => "locations", "class" => \twa\ecomgeneral\helpers\locationsHelper::class, ],
        [ "entity" => "favorites", "class" => \twa\ecomgeneral\helpers\favoritesHelper::class, ],
        [ "entity" => "loyalty", "class" => \twa\ecomgeneral\helpers\loyaltyHelper::class, ],
        [ "entity" => "digitalwallet", "class" => \twa\ecomgeneral\helpers\DigitalWalletHelper::class, ],
    ];



    foreach ($helpers AS $helper){
        if($entity == $helper["entity"]){
            return (new $helper['class']());
        }
    }

    dd(["error" => "the property you are trying to access with the ecom helper \"$entity\" was not found","available options" => Arr::pluck($helpers, 'entity')]);

}

?>
