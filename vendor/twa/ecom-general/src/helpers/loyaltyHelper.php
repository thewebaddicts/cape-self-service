<?php
namespace twa\ecomgeneral\helpers;

use twa\ecomcart\controllers\EcomLoyaltyController;
use twa\ecomprofile\controllers\EcomUsersLoyaltyController;

class loyaltyHelper
{

    private $parameters;

    function __construct(){
        if(isset(request()->store) && isset(request()->store['id'])){
            $this->parameters["store"] = request()->store['id'];
        }
    }

    public function get(){
        return (new EcomUsersLoyaltyController())->getBalances([ "store" => ecom('stores')->getCurrent()['id'] ], request()->user['user']['id']);
    }

    public function redeem($points){
        return (new EcomUsersLoyaltyController())->redeem([ "value" => $points, "store" => ecom('stores')->getCurrent()['id'] ], request()->user['user']['id']);
    }
}
