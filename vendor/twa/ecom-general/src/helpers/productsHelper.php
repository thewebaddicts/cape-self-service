<?php
namespace twa\ecomgeneral\helpers;

use Illuminate\Database\Eloquent\Model;
use twa\ecomgeneral\models\FrontEndMenuBuilderModel;
use twa\ecomgeneral\models\FrontEndMenuLinksModel;
use twa\ecomproducts\models\EcomProductsCollectionModel;
use Illuminate\Support\Facades\DB;
class productsHelper
{

    private $parameters;
    private $productsClass;
    public $products;
    private $pageTitle;
    private $menu;
    private $ids;

    function __construct(){
        if(isset(request()->store) && isset(request()->store['id'])){
            $this->parameters["store"] = request()->store['id'];
        }
        $this->productsClass = new \twa\ecomproducts\controllers\EcomProductsController;
    }

    public function condition($key,$value){
        if($key == "id"){ $key = "productID"; }
        $this->parameters[$key] = $value;
        return $this;
    }


    public function includeFilters(){
        $this->parameters["includeFilters"] = 1;
        return $this;
    }

    public function setMinPrice($value){
        $this->parameters["minPrice"] = $value;
        return $this;
    }

    public function setMaxPrice($value){
        $this->parameters["maxPrice"] = $value;
        return $this;
    }

    public function byKeyword($ids){
        $this->parameters["keywords"] = $ids;
        $return = $this->productsClass->query($this->parameters);
        return collect($return);
    }

    public function bySubcategories($ids){
        $this->parameters["subcategories"] = $ids;
        $return = $this->productsClass->query($this->parameters);
        return collect($return);
    }


    function byCollection($id){
        $menu = \twa\ecomproducts\models\EcomProductsCollectionModel::where('id',$id)->first();
        if(!$menu){ abort(404); }
        switch ($menu->mode){
            case "static link":
                return redirect($menu->link);
                break;
            case "products using filters":
                $this->parameters["ecom_basic_filters"] = implode(',',$menu->ecom_products_filters_id);
                $return = $this->productsClass->query($this->parameters);
                $return["menu"] = $menu;
                return collect($return);
                break;
            case "products using keyword":
                $return = self::byKeyword(implode(',',$menu->keywords));
                $return["menu"] = $menu;
                return $return;
                break;
            case "products using categories":
                $return = self::bySubcategories(implode(',',$menu->ecom_subcategories_id));
                $return["menu"] = $menu;
                return $return;
                break;
            default:
                return abort(403,'The Mode set in the Collection (id = '.$menu->id.') is not supported. Please use the CMS to fill this link properly.');
        }
    }


    function addField($field){
        if(isset($this->parameters["extraFields"])) { $fields = explode(',',$this->parameters["extraFields"]); }else{ $fields = []; }
        $fields[] = $field;
        $this->parameters["extraFields"] = implode(',',$fields);
        return $this;
    }


    public function addFilter($id){
        if(isset($this->parameters["ecom_filters"])){ $availableFilters = collect(explode(',',$this->parameters["ecom_filters"])); }else{ $availableFilters = collect([]); }
        $availableFilters[] = $id;
        $availableFilters = $availableFilters->unique();
        $this->parameters["ecom_filters"] = implode(',',$availableFilters->toArray());
        return $this;
    }

    public function addRecommendations(){
        $this->parameters["includeRecommendations"] = 1;
        return $this;
    }

    public function addPeopleAlsoBought(){
        $this->parameters["includePeopleAlsoBought"] = 1;
        return $this;
    }

    public function addNotifications(){
        $this->parameters["includeNotifications"] = 1;
        return $this;
    }

    public function addFilters($ids){
        if(is_array($ids)) {
            foreach ($ids as $id) {
                self::addFilter($id);
            }
        }
        return $this;
    }


    public function get(){
//        dd($this->parameters);
        $this->parameters = collect($this->parameters);
        $return = $this->productsClass->query($this->parameters);
        $return["menu"] = new \stdClass(); $return["menu"]->label = "All Products";

        $this->products = $return;
        return $this;
    }


    public function toArray(){
        return collect($this->products);
    }



    public function ByID($productID){
        $this->parameters = collect($this->parameters);
        $this->parameters["productID"] = $productID;
        if (!isset($this->parameters['includeTitle'])) {
            $this->parameters['includeTitle'] = 0;
        }

        $this->products = $this->productsClass->query($this->parameters);

        if($this->parameters['includeTitle'] > 0 && $this->parameters['includeTitle'] != NULL && $this->parameters['includeTitle']){
            try{
                $this->menu = json_decode($this->parameters["includeTitle"]);
            }catch (\Throwable $e){
                $this->menu = false;
            }
        }else{
            $this->menu = false;
        }

        try{
            $this->pageTitle = $Products->title;
        }catch (\Throwable $e){
            $this->pageTitle = "";
        }

//        if (isset($this->parameters['includeRecommendations'])) {
//            $this->products['data'][0]['recommendations'] = "assaad";
//        }
        try{
            return collect($this->products['data'][0],200);
        }catch (\Throwable $e){
            return false;
        }

    }




    public function ByIDs($productIDs){
        $this->parameters = collect($this->parameters);
        $this->parameters["productIDs"] = $productIDs;
        if (!isset($this->parameters['includeTitle'])) {
            $this->parameters['includeTitle'] = 0;
        }
        $this->products = $this->productsClass->query($this->parameters);

        if($this->parameters['includeTitle'] > 0 && $this->parameters['includeTitle'] != NULL && $this->parameters['includeTitle']){
            try{
                $this->menu = json_decode($this->parameters["includeTitle"]);
            }catch (\Throwable $e){
                $this->menu = false;
            }
        }else{
            $this->menu = false;
        }

        try{
            $this->pageTitle = $Products->title;
        }catch (\Throwable $e){
            $this->pageTitle = "";
        }
        try{
            return collect($this->products['data'],200);
        }catch (\Throwable $e){
            dd($e);
        }

    }




    public function ByTerm($term){
        $this->parameters["search"] = $term;
        $return = $this->productsClass->query($this->parameters);
        $return["menu"] = json_decode(json_encode([ "label" => __('ecom.searching').' "'.$term.'"' ]));
        return $return;

    }


    public function ByMenuQuery($id){
        //we get the frontend menu
        $menu = \twa\ecomgeneral\models\FrontEndMenuLinksModel::where('id',$id)->first();
//        dd($menu);
        if(!$menu){ abort(404); }
        switch ($menu->mode){
            case "static link":
                return redirect($menu->link);
                break;
            case "products using filters":
                $this->parameters["ecom_basic_filters"] = implode(',',$menu->ecom_products_filters_id);
                $return = $this->productsClass->query($this->parameters);
                $return["menu"] = $menu;
                return collect($return);
                break;
            case "products using keyword":
                $return = self::byKeyword(collect($menu->keywords)->implode(','));
                $return["menu"] = $menu;
                return $return;
                break;
            case "products using categories":
                $return = self::bySubcategories(implode(',',$menu->ecom_subcategories_id));
                $return["menu"] = $menu;
                return $return;
                break;
            default:
                return abort(403,'The Mode set in the Navigation link (id = '.$menu->id.') is not supported. Please use the CMS to fill this link properly.');
        }
    }



    function byCollectionOrMenuSlug($slug){
        //we check if there's a collection wiwth th
        $Collection = EcomProductsCollectionModel::where('slug',$slug)->where('ecom_stores_id','LIKE','%"'.request()->store['id'].'"%')->where('display',1)->where('cancelled',0)->first();
        if($Collection){
            return $this->byCollection($Collection['id']);
        }else{
//            DB::enableQueryLog();
            $FrontEnd = FrontEndMenuLinksModel::where('slug',$slug)->whereIn('00_cms_utilities_frontend_menus_id',function($query){
                $query->select('id')
                    ->from(with(new FrontEndMenuBuilderModel())->getTable())
                    ->where('ecom_stores_id','LIKE','%"'.request()->store['id'].'"%')
                    ->where('cancelled', 0);
            })->where('cancelled',0)->first();
//            dd(DB::getQueryLog());
//            dd($FrontEnd);
            if($FrontEnd){
                return $this->ByMenuQuery($FrontEnd['id']);
            }else{
                return abort(404);
            }
        }
    }

    public static function getBreadcrumbs($product){
        $slug = str_replace(ecom('url')->prefix(true).'/products/','',explode('?',url()->previous())[0]);
        if($slug == env('APP_URL')){
            return [
                [
                    "label" => __('ecom.all_products'),
                    "link" => ecom('url')->prefix(true).'/products/'
                ],
                [
                    "label" => $product['label'],
                    "link" => $product['link'],
                ],
            ];
        }

        $add = false;
        $Collection = EcomProductsCollectionModel::where('slug',$slug)->where('ecom_stores_id','LIKE','%"'.request()->store['id'].'"%')->where('display',1)->where('cancelled',0)->first();
        if($Collection){
            $add = [
                "label" => $Collection['label'],
                "link" => $Collection['link']
            ];
        }else{
            $FrontEnd = FrontEndMenuLinksModel::where('slug',$slug)->whereIn('00_cms_utilities_frontend_menus_id',function($query){
                $query->select('id')
                    ->from(with(new FrontEndMenuBuilderModel())->getTable())
                    ->where('ecom_stores_id','LIKE','%"'.request()->store['id'].'"%')
                    ->where('cancelled', 0);
            })->where('cancelled',0)->first();
            if(env('CMS_LANGUAGES_MODE')=="translated"){
                $labelfield = "label_".app()->getLocale();
            }else{
                $labelfield = "label";
            }
            if($FrontEnd) {
                $add = [
                    "label" => $FrontEnd[$labelfield],
                    "link" => ecom('url')->prefix(true) . '/products/' . $FrontEnd['slug']
                ];
            }
        }

        $return[] =
            [
                "label" => __('ecom.all_products'),
                "link" => ecom('url')->prefix(true).'/products/'
            ];
        if($add){
            $return [] = $add;
        }
        $return [] =  [
            "label" => $product['label'],
            "link" => env("APP_URL").$product['link'],
        ];

        return $return;

    }


    public function render(){
        if(!isset($this->products['data'][0])){
            abort(404);
        }
        if(isset($this->parameters['productID']) || isset($this->parameters['slug'])) {
            //this means this is a single product query, we return the products details
            return view(config('ecom.default_views.product', 'pages.products.details'), [ 'product' => collect($this->products['data'][0]), "Menu" => collect($this->menu), "IDs" => collect($this->ids), "recommendations" => (isset($this->products["data"][0]["recommendations"]['recommended'])) ? $this->products["data"][0]["recommendations"]['recommended'] : false, "peopleAlsoBought" => (isset($this->products["data"][0]["recommendations"]['also_bought'])) ? $this->products["data"][0]["recommendations"]['also_bought'] : false, "productNotification" => (isset($this->products["data"][0]["productNotification"])) ? $this->products["data"][0]["productNotification"] : false, "load_adjuster" => (isset($this->products["data"][0]["load_adjuster"])) ? $this->products["data"][0]["load_adjuster"] : false, "slug" => self::getBreadcrumbs(collect($this->products['data'][0]))]);
        } else {
            return view(config('ecom.default_views.product_listing', 'pages.products.listing'), ['products' => collect($this->products) ]);
        }
    }


}

