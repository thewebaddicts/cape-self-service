<?php
namespace twa\ecomgeneral\helpers;

class favoritesHelper
{

    private $parameters;
    // private $favorites;
    private $favoritesController;

    function __construct(){
        if(isset(request()->store) && isset(request()->store['id'])){
            $this->parameters["store"] = request()->store['id'];
        }

        // $this->favorites = new \twa\ecomcart\facades\EcomFavoritesFacade;
        $this->favoritesController = new \twa\ecomcart\controllers\EcomFavoritesController;

    }

    public function get(){
        return $this->favoritesController->query();
    }

    public function getIDs(){
        return $this->favoritesController->getIDs();
    }


    public function condition($key,$value){
        $this->parameters[$key] = $value;
        return $this;
    }


    public function removeProduct($productID ){
        $response =  $this->favoritesController->removeProduct(['productID'=> $productID ]);

        if($response->getStatusCode() != 200){

        }

        return $response;

    }

    public function addProduct($productID){
        $response =  $this->favoritesController->addProduct(['productID'=> $productID ]);


        if($response->getStatusCode() != 200){

        }

       return $response;
    }

    public function check($productID){
        return $this->favoritesController->checkActive(['productID'=> $productID ]);
    }





}
