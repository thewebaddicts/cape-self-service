<?php
namespace twa\ecomgeneral\helpers;

class notificationsHelper
{

    private $parameters;

    function __construct(){
        if(isset(request()->store) && isset(request()->store['id'])){
            $this->parameters["store"] = request()->store['id'];
        }
    }

    public function showPopup(){
        if(request()->input('title') && request()->input('message')){
            echo '<script language="javascript">function NotificationFunctions(){ ShowMessage("'.request()->input('title').'","'.request()->input('message').'"); }</script>';
        }
    }

    public function get($limit = false){
        if($limit){ $this->parameters["get"] = $limit; }
        return (new \twa\ecomproducts\controllers\EcomCollectionsController)->list($this->parameters);
    }
}
