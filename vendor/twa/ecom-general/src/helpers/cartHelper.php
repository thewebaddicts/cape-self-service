<?php
namespace twa\ecomgeneral\helpers;

class cartHelper
{

    private $parameters;
    private $cart;
    private $lastValue;
    private $cartController;

    function __construct(){
        if(isset(request()->store) && isset(request()->store['id'])){
            $this->parameters["store"] = request()->store['id'];
        }

        $this->cart = new \twa\ecomcart\facades\EcomCartFacade;
        $this->cartController = new \twa\ecomcart\controllers\EcomCartController;
    }

    public function render(){
        return $this->cart->render();
    }


    public function condition($key,$value){
        $this->parameters[$key] = $value;
        return $this;
    }

    public function get(){
        $this->lastValue = $this->cart->query();
        return $this;
    }

    public function getAsObject(){
        $this->lastValue = $this->cart->query();
        $this->lastValue = json_decode(json_encode($this->lastValue));
        return $this->lastValue;
    }

    public function getAsArray(){
        $this->lastValue = collect($this->lastValue)->recursive();
        return $this->lastValue;
    }

    public function addToCart($productID , $quantity, $json = [], $modifiers = null){
        if(!request()->user || (isset(request()->user['token']) && !request()->user['token'])){
            $response = \twa\ecomauth\facades\EcomUsersFacade::checkOfflineToken();
        }
        try {
            $response =  $this->cartController->updateCart([ 'store'=> request()->store['id'] , 'productID' => $productID, 'quantity' => $quantity , 'json' => $json, 'modifiers' => $modifiers ] );
        } catch (\Throwable $th) {
            return response([], 404);
        }

        return response($response , 200);
    }


    public function updateCartItem($cartItemID , $fields = []){


        try {
            $response =  $this->cartController->updateCartItem([ 'store'=> request()->store['id'] , 'cartItemID' => $cartItemID, 'fields' => $fields ]);
        } catch (\Throwable $th) {
            return response([], 404);
        }


        return $response;
    }


    public function removeItem($cartItemID){
        $response =  $this->cartController->removeItem(['cartItemID'=> $cartItemID ]);
        $return =  $this->getAsObject();

        if($response->getStatusCode() != 200){
            $resonseContent =  $response->getOriginalContent();
            $return->message = $resonseContent['error']['title'];
       }

       return $return;
    }

    public function removeInvalidItems($invalidItems){
        $response = $this->cartController->removeInvalidItems(['invalidItems'=> $invalidItems ]);
        $return = $this->getAsObject();

        if($response->getStatusCode() != 200){
            $resonseContent =  $response->getOriginalContent();
            $return->message = $resonseContent['error']['title'];
        }
        return $return;
    }

    public function updateQuantity($productID ,$direction){
       $response = $this->cartController->updateQuantity(['productID'=> $productID , 'direction' => $direction ]);
       $return = $this->getAsObject();

       if($response->getStatusCode() != 200){
            $resonseContent =  $response->getOriginalContent();
            $return->message = $resonseContent['error']['title'];
       }

       return $return;
    }

    public function applyVoucher($code){
        $response =  $this->cartController->voucherApply(['code'=> $code ]);

        $return =  $this->getAsObject();

        if($response->getStatusCode() != 200){
            $resonseContent =  $response->getOriginalContent();
            $return->error = $resonseContent['error'];
        }else{
            $resonseContent =  $response->getOriginalContent();
            $return->success = $resonseContent['success'];
        }

       return $return;
    }


    public function removeVoucher(){
        $response =  $this->cartController->voucherRemove();


        $return =  $this->getAsObject();

        if($response->getStatusCode() != 200){
            $resonseContent =  $response->getOriginalContent();
            $return->error = $resonseContent['error'];
        }else{
            $resonseContent =  $response->getOriginalContent();
            $return->success = $resonseContent['success'];
        }


       return $return;
    }

}
