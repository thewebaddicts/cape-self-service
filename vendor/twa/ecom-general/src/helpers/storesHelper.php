<?php
namespace twa\ecomgeneral\helpers;

use twa\ecomgeneral\models\EcomStoreModel;
use twa\ecomprofile\models\EcomCountriesModel;

class storesHelper
{

//    private $parameters;
//    private $addresses;
//    private $lastValue;

    function __construct(){
//        if(isset(request()->store) && isset(request()->store['id'])){
//            $this->parameters["store"] = request()->store['id'];
//        }
    }

    public function getCurrent(){
        return request()->store;
    }

    public function getPayments(){
        return collect((new \twa\ecomgeneral\controllers\EcomStoresController)->getPaymentMethods([ "store" => request()->store['id'] ])->getOriginalContent())->recursive();
    }

    public function getShipping($AddressID = false, $CountryID = false){
        return collect((new \twa\ecomgeneral\controllers\EcomStoresController)->getShippingMethods([ "store" => request()->store['id'], "addressID" => $AddressID, "countryID" => $CountryID  ])->getOriginalContent())->recursive();
    }

    public function list(){
        return collect((new \twa\ecomgeneral\controllers\EcomStoresController)->list())->recursive();
    }

    public function find($prefix){
        return collect((new \twa\ecomgeneral\controllers\EcomStoresController)->find([ "web_prefix" => $prefix ])->getOriginalContent())->recursive();
    }

    public function listGrouped($prefix){
        return collect((new \twa\ecomgeneral\controllers\EcomStoresController)->listGroupedByRegion()->getOriginalContent())->recursive();
    }

}
