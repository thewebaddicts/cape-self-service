<?php
namespace twa\ecomgeneral\helpers;

class settingsHelper
{

    private $parameters;

    function __construct(){
        if(!request()->settings){
            dd('setttings is not available in the request');
        }
    }

    public function get($value){
        return request()->settings->get($value);
    }
}
