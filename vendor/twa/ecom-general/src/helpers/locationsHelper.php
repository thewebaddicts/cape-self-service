<?php
namespace twa\ecomgeneral\helpers;

class locationsHelper
{

    private $parameters;
    private $addresses;
    private $lastValue;

    function __construct(){
        if(isset(request()->store) && isset(request()->store['id'])){
            $this->parameters["store"] = request()->store['id'];
        }

        $this->locations = new \twa\ecomgeneral\controllers\EcomStoresLocationsController();
    }

    public function list(){
        return $this->locations->list($this->parameters);
    }

    public function get($id){
        $this->parameters["id"] = $id;
        return $this->locations->getByID($this->parameters);
    }
}
