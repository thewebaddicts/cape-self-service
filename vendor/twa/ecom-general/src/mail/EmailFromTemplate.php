<?php

namespace twa\ecomgeneral\mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use twa\ecomgeneral\models\EmailTemplateModel;

class EmailFromTemplate extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $subject;
    public $dictionary;

    public function __construct($subject,$dictionary)
    {
        $this->subject=$subject;
        $this->dictionary=$dictionary;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = EmailTemplateModel::where('location', $this->subject)->orWhere('location', strtolower($this->subject))->first();
        return $this->from(env('MAIL_USERNAME'), env('MAIL_FROM'))->subject($template->subject)->view(config('ecom.default_views.email_template','ecom.emails.email-template'), ["template"=> $template, "dictionary"=> $this->dictionary ]);
    }
}
