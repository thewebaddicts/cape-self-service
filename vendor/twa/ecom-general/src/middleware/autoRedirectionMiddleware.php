<?php
namespace twa\ecomgeneral\middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Support\Facades\URL;
use twa\ecomgeneral\models\EcomStoreModel;
use twa\ecomprofile\models\EcomCountriesModel;


class autoRedirectionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */


    public function __construct()
    {
    }


    public function handle(Request $request, Closure $next)
    {


        if(request()->input('no_redirect')){ return $next($request); }
        //we check if the url has a query string
        if(strlen(str_replace(env('APP_URL'),'',url()->current())) > 3){  return $next($request); }

        //we check if the domain is available in one of the stores'domains
        try{
            $domain = $request->getHost();
            if(request()->input('debug')){
                dd($domain);
            }
            if($request->getSchemeAndHttpHost() != env('APP_URL')){
                $PossibleStore = EcomStoreModel::where('cancelled',0)->where('domains','LIKE','%"'.$request->getHost().'"%')->where('published',1)->first();
                if($PossibleStore){
                    return redirect(env('APP_URL').'/'.$PossibleStore->web_prefix.'/'.$PossibleStore->default_language);
                }
            }
        }catch (\Throwable $e){}

        $code = country()->code();

        if(!$code){
           return $next($request);
        }else{
           //we ge the country ID first
           $country = EcomCountriesModel::where('code',$code)->where('cancelled',0)->first();
           if(!$country){ return $next($request); }

           //we get the store covered the code provided
           $store = EcomStoreModel::select('*')->where('published',1)->where('show_menu',1)->selectRaw('group_'.app()->getLocale().' AS `group`')->selectRaw('label_'.app()->getLocale().' AS label')->where('cancelled',0)->where('countries_id','LIKE','%"'.$country->id.'"%')->orderBy('orders','ASC')->get();
           if(!$store){ return $next($request); }
           //if we find the store, we redirect

            if( isset($store) && isset($store[0]) && isset($store[0]->redirection_link)){
                return redirect($store[0]->redirection_link);
            }

            if(isset($store) && collect($store)->count() > 1){
//                return redirect(env('APP_URL').'/'.app()->getLocale().'?no_redirect=1&no_popup=1&city_popup=1&city_id='.$store[0]->id);
                return redirect(env('APP_URL').'/'.$store[0]->web_prefix.'/'.$store[0]->default_language.'?city_popup=1&city_id='.$store[0]->id);
           }elseif (isset($store)  && collect($store)->count() == 1){
               return redirect(env('APP_URL').'/'.$store[0]->web_prefix.'/'.$store[0]->default_language);
           }else{
                return $next($request);
            }

        }
    }
}
