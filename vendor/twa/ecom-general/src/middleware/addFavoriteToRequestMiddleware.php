<?php

namespace twa\ecomgeneral\middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\UrlGenerator;

class addFavoriteToRequestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    private $url;

    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
    }


    public function handle(Request $request, Closure $next)
    {
        if(!isset($request->user)){
            $request->user = [];
        }
        $request->user['favorites']=ecom('favorites')->getIDs();
        return $next($request);
    }
}
