<?php
namespace twa\ecomgeneral\middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Support\Facades\URL;


class storeAPIRouteDataMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    private $url;

    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
    }


    public function handle(Request $request, Closure $next)
    {
        $store = (new \twa\ecomgeneral\controllers\EcomStoresController)->getByID([ "storeID" => $request->header('store') ]);
        if(!$store){ abort(404); }

        //this will attach to the request the settings + currencies + store information
        $request = \twa\ecomgeneral\middleware\repository\StoreDefaultDataRepo::attachBasicInfo($request,$store);

        return $next($request);
    }
}
