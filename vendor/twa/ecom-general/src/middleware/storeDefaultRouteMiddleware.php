<?php

namespace twa\ecomgeneral\middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\UrlGenerator;

class storeDefaultRouteMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    private $url;

    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
    }


    public function handle(Request $request, Closure $next)
    {
        $stores = (new \twa\ecomgeneral\controllers\EcomStoresController)->list();
        if($stores->count() == 0){
            return response()->json(['status' => 'error', 'message' => 'You do not have any store defined in your CMS yet.'],500);
        }elseif($stores->count() == 1){
            return redirect('/'.$stores[0]['web_prefix'].'/'.$stores[0]['default_language']);
        }else{
            switch (config('ecom.default_landing.mode')){
                case "view":
                    return response()->view(config('ecom.default_landing.view'));
                    break;
                case "route":
                    return response()->redirectTo(config('ecom.default_landing.route'));
                    break;
                case "none":
                    return $next($request);
                    break;
                default:
                    return response()->json(['status' => 'error', 'message' => 'You have to set the default action & store selection insdide config/ecom.php'],500);
            }
        }

    }
}
