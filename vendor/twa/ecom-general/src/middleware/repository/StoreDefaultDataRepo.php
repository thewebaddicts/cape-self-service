<?php
namespace twa\ecomgeneral\middleware\repository;

use Illuminate\Http\Request;
use twa\ecomgeneral\models\EcomStoreModel;

class StoreDefaultDataRepo{
    public static function attachBasicInfo(Request $request, $store){
        $request->store = $store;
        try{
            if($store->getStatusCode() != 200){
                $store = EcomStoreModel::where('cancelled',0)->where('published',1)->first();
                $request->store = $store;
            }

        }catch (\Throwable $e){
        }
        $request->store['currencies'] = (new \twa\ecomgeneral\controllers\EComCurrencyController)->list([ "store" => $store["id"] ]);
        $request->store['preferred_currency'] = $request->store['currencies'][0]; //this will be overriden at the next statement. it's a fallback
        foreach ($request->store['currencies'] AS $currency){
            if($currency->selected){ $request->store['preferred_currency'] = $currency; }
        }
        $request->settings = new \twa\apilibs\controllers\SettingsController;

        return $request;
    }
}

?>
