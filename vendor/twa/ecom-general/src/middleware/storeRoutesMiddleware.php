<?php
namespace twa\ecomgeneral\middleware;

use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Support\Facades\URL;
use twa\ecomgeneral\middleware\repository\StoreDefaultDataRepo;
use twa\ecomgeneral\models\EcomStoreModel;
use Closure;

class storeRoutesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    private $url;

    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
    }


    public function handle(Request $request, Closure $next)
    {

        $languages = collect(json_decode(str_replace("'",'"',env('CMS_LANGUAGES'))))->pluck('prefix')->values()->toArray();
        if(!in_array(app()->getLocale(),$languages)){
            return redirect('/');
        }

        $store = EcomStoreModel::select("*");
        if(env('CMS_LANGUAGES_MODE') == "translated"){
            $store = $store->selectRaw('header_message_'.app()->getLocale().' AS header_message, label_'.app()->getLocale().' AS label ')->where('web_prefix',$request->segment(1));
        };
        $store = $store->where('cancelled',0)->where('published',1)->first();

        if(!$store){ abort(404); }
        if(!$store['languages'] || !in_array($request->segment(2),$store['languages'])){ abort(404); }

        URL::defaults([ 'store_prefix' => $request->segment(1), 'language_prefix' => $request->segment(2) ]);

        //this will attach to the request the settings + currencies + store information
        $request = StoreDefaultDataRepo::attachBasicInfo($request,$store);

        return $next($request);
    }
}
