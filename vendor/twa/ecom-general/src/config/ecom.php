<?php

return [

    'default_landing' => [
        "mode" => "view",  //this can be either view or route
        "view" => "pages.select-store",
        "route" => "/stores/select",
    ],

    'menus' => [
        [ "name" => "menuHtml", "parameters" => [ "menu" => 4, "listTag" => "ul", "listClass" => "main-menu", "labelClass" => "label", "itemClass" => "item", "encloseLI" => true, "encloseLIClass" => "encloureClass", 'ecomLinkStructure' => env('APP_URL').'/{store_prefix}/{language_prefix}/shop/menu/{slug}/{id}', 'nonEcomLinkPrefix' => '/{store_prefix}/{language_prefix}' ] ],
        [ "name" => "extraMenuHtml", "parameters" => [ "menu" => 5, "listTag" => "ul", "listClass" => "main-menu", "labelClass" => "label", "itemClass" => "item", "encloseLI" => true, "encloseLIClass" => "encloureClass", 'ecomLinkStructure' => env('APP_URL').'/{store_prefix}/{language_prefix}/shop/menu/{slug}/{id}', 'nonEcomLinkPrefix' => '/{store_prefix}/{language_prefix}' ] ],
        [ "name" => "menuHtmlMobile", "parameters" => [ "menu" => 6, "listTag" => "ul", "listClass" => "main-menu-mobile", "labelClass" => "label", "itemClass" => "item", "encloseLI" => true, "encloseLIClass" => "", 'ecomLinkStructure' => env('APP_URL').'/{store_prefix}/{language_prefix}/shop/menu/{slug}/{id}', 'nonEcomLinkPrefix' => '/{store_prefix}/{language_prefix}' ] ]
    ],

    //please fill your route names for the following pages
    'default_routes' =>
        [
            //authentication routes
            'forgot' => "forgot-password",  'login' => "login",  'register' => "register",
            //user profile routes
            'addresses' => "account-addresses"
        ]
    ,

    'default_views' =>
        [
            'product_details' => "pages.product-details",
            'address_listing' => "pages.account.account-addresses",
            'address_form' => "pages.account.account-address-form",
            'address_states' => "addresses.states",
            'address_cities' => "addresses.cities",
        ],

    //endpoints are the basepath for the apis in case we are using microservices & APIs
    'endpoints' => [
        'storage_url' => env('DATA_URL',env('BASE_DATA_URL',env('APP_URL'))),
        'utilities' => env('API_UTILITIES',env('API_URL')),
        'authentication' => env('API_AUTHENTICATION',env('API_URL')),
        'products' => env('API_PRODUCTS',env('API_URL')),
        'purchases' => env('API_PURCHASE',env('API_URL')),
        'profile' => env('API_PROFILE',env('API_URL')),
    ],

    'caching_tags' => [
        "*/products/query" => "product"
    ]
];
