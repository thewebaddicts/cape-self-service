<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => [ \twa\apilibs\middleware\TWAApiLocaleMiddleware::class, twa\ecomgeneral\middleware\storeAPIRouteDataMiddleware::class ] ,'prefix'=>'/api/v1/ecom'], function(){
    Route::group(['prefix'=>'/stores'], function() {
        Route::post('/get/id', [ twa\ecomgeneral\controllers\EcomStoresController::class , 'getByID' ]);
        Route::post('/get/prefix', [ twa\ecomgeneral\controllers\EcomStoresController::class , 'getByPrefix' ]);
        Route::post('/list', [ twa\ecomgeneral\controllers\EcomStoresController::class , 'list' ]);
        Route::post('/payment/list', [ twa\ecomgeneral\controllers\EcomStoresController::class , 'getPaymentMethods' ]);
        Route::post('/payment/get', [ twa\ecomgeneral\controllers\EcomStoresController::class , 'getPaymentMethod' ]);
        Route::post('/shipping/list', [ twa\ecomgeneral\controllers\EcomStoresController::class , 'getShippingMethods' ]);
        Route::post('/shipping/get', [ twa\ecomgeneral\controllers\EcomStoresController::class , 'getShippingMethod' ]);
    });

    Route::group(['prefix'=>'/stores'], function() {
        Route::post('/get/id', [ twa\ecomgeneral\controllers\EcomStoresController::class , 'getByID' ]);
        Route::post('/get/prefix', [ twa\ecomgeneral\controllers\EcomStoresController::class , 'getByPrefix' ]);
        Route::post('/list', [ twa\ecomgeneral\controllers\EcomStoresController::class , 'list' ]);

        Route::group(['prefix'=>'/locations'], function() {
            Route::post('/list', [ twa\ecomgeneral\controllers\EcomStoresLocationsController::class , 'list' ]);
        });
    });

    Route::group(['middleware' => [ twa\ecomauth\middleware\TranslateToken::class] , 'prefix'=>'/currencies'], function() {
        Route::post('/list', [ twa\ecomgeneral\controllers\EComCurrencyController::class , 'list' ]);
        Route::post('/switch', [ twa\ecomgeneral\controllers\EComCurrencyController::class , 'switch' ]);
    });

    Route::group(['prefix'=>'/utilities'], function() {
        Route::group(['prefix'=>'/frontendmenu'], function() {
            Route::post('/generate/{output}', function ($output) { return (new twa\ecomgeneral\controllers\FrontEndMenuBuilder)->renderAPI($output,[]); });
            Route::post('/get', function () { return \App\Http\Controllers\FrontEndMenuBuilder::get(); });
        });
    });
});

Route::group(['middleware' => [  \twa\apilibs\middleware\TWAApiLocaleMiddleware::class, twa\ecomgeneral\middleware\storeAPIRouteDataMiddleware::class ] ,'prefix'=>'/api/v1/ecom'], function(){
    Route::post('/get/scheduled/delivery/{state}', function ($state) {
        return (new \twa\ecomgeneral\controllers\EcomStoresController())->getScheduledDeliveryDates(['state'=>$state , 'mobile' => true]);
    });
    Route::post('/outofstock/notify', function (){
        return (new \twa\ecomgeneral\controllers\EComStockController())->storeUser();
    });
});

