<?php

use Illuminate\Support\Facades\Route;
use twa\translationssync\translationsSyncController;
use twa\ecomgeneral\controllers\EComStockController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/ecom/order/{id}/{token}', function ($id,$token) {
    $Purchase = \twa\ecomcart\models\EcomPurchasesModel::where('id',$id)->first();
    $User = \twa\ecomgeneral\models\EcomUsersModel::where('id',$Purchase->ecom_users_id)->first();
//    $data = \twa\ecomcart\controllers\EcomPurchasesController::generateReceiptData($Purchase,$User);
    return view('EcomGeneralViews::receipts.default',[ "data" => $Purchase , "User"=>$User, "hide_prices" => 0 ]);
});
Route::get('/ecom/order/pickup/{id}/{token}', function ($id,$token) {
    $Purchase = \twa\ecomcart\models\EcomPurchasesModel::where('id',$id)->first();
    $User = \twa\ecomgeneral\models\EcomUsersModel::where('id',$Purchase->ecom_users_id)->first();
//    $data = \twa\ecomcart\controllers\EcomPurchasesController::generateReceiptData($Purchase,$User);
    return view('EcomGeneralViews::receipts.default',[ "data" => $Purchase , "User"=>$User, "hide_prices" => 1 ]);
});

Route::get('/ecom/order/waybill/{id}/{token}', function ($id,$token) {
    return (new \twa\ecomgeneral\controllers\EcomViewsController)->getWaybill($id);
});
Route::get('/ecom/order/fulfill/{id}/{token}', function ($ids,$token) {
    return view('EcomGeneralViews::fulfillment.default',[ "ids" => explode(',',$ids), "token" => $token ]);
});
Route::get('/ecom/order/fulfill/single/{id}/{token}', function ($id,$token) {
    return view('EcomGeneralViews::fulfillment.single',[ "id" => $id, "token" => $token ]);
});
Route::post('/ecom/order/fulfill/single/{id}/{token}', function ($id,$token) {
    $Purchase = \twa\ecomcart\models\EcomPurchasesModel::where('id',$id)->where('cancelled',0)->first();
    $Purchase->status = request()->input('status');
    $Purchase->save();
    return view('EcomGeneralViews::fulfillment.single',[ "id" => $id, "token" => $token ]);

});


Route::get('/ecom/search/export', [\twa\ecomgeneral\controllers\EComSearchController::class, 'productsExportJson']);
Route::get('/twa/discount/test', [\twa\ecomproducts\controllers\EcomDiscountController::class, 'ApplyDiscount']);

Route::group([
    'prefix'     => '/{store_prefix}/{language_prefix}/',
    'middleware' => [twa\ecomgeneral\middleware\storeRoutesMiddleware::class],
], function () {
    Route::get('/get/scheduled/delivery/{state}', function ($store_prefix,$language_prefix , $state) {
        return (new \twa\ecomgeneral\controllers\EcomStoresController())->getScheduledDeliveryDates(['state'=>$state]);
    });
    Route::post('/outofstock/notify', function ($store_prefix,$language_prefix){
        return (new \twa\ecomgeneral\controllers\EComStockController())->storeUser();
    });
});
