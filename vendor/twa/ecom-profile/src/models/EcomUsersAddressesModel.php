<?php

namespace twa\ecomprofile\models;
use \twa\apilibs\controllers\PhoneFunctionsController;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EcomUsersAddressesModel extends Model
{
    use HasFactory;
    protected $table = "ecom_users_addresses";
    protected $appends = ['country', 'phone_country_code','phone_original','state_info'];

   public function getCountryAttribute($value){
       return EcomCountriesModel::where('id',$this->countries_id)->where('cancelled',0)->first();
   }
    public function getPhoneCountryCodeAttribute($value){
        return PhoneFunctionsController::getCountry($this->phone);
    }
    public function getPhoneOriginalAttribute($value){
        return PhoneFunctionsController::formatNational($this->phone, PhoneFunctionsController::getCountry($this->phone));
    }
    public function getStateInfoAttribute($value){
        $State = EcomStatesModel::where('code',$this->state)->where('countries_id',$this->countries_id)->first();
        return $State;
    }
}
