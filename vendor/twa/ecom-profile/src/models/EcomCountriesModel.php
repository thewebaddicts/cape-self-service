<?php

namespace twa\ecomprofile\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EcomCountriesModel extends Model
{
    use HasFactory;
    protected $table = "countries";
}
