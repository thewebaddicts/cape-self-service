<?php

namespace twa\ecomprofile\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EcomStatesModel extends Model
{
    use HasFactory;
    protected $table = "countries_states";
}
