<?php

namespace twa\ecomprofile\models;
use \twa\apilibs\controllers\PhoneFunctionsController;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EcomUsersWalletModel extends Model
{
    use HasFactory;
    protected $table = "ecom_users_digitalwallet_balances";

}
