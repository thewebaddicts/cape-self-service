<?php

namespace twa\ecomprofile\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use twa\ecomgeneral\models\EcomStoreModel;
use twa\ecomprofile\models\EcomCountriesModel;
use twa\apilibs\controllers\PhoneFunctionsController;
use twa\ecomprofile\models\EcomUsersAddressesModel;
use twa\apilibs\traits\APITrait;
use App\Http\Repositories\TWAAramexFunctions;
use App\Http\Repositories\TWAPhoneFunctions;
use Illuminate\Http\Request;

class EcomUsersAddressesController extends Controller
{
    use APITrait;

    public function list($values = [] , $user_id = false){
        if(!$user_id){
            $user_id = request()->user['user']['id'];
        }
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[  ],['store']);
        if($Check!==true){ return $Check; }

        if(!$user_id){
            return response([ "error" => [ "message" => "you are required to login in order to access this api" ]]);
        }

        $Addresses = EcomUsersAddressesModel::where('ecom_users_id',$user_id)->where('ecom_stores_id',$values->get('store'))->where('cancelled',0)->get();
        return response($Addresses,200);
    }

    public function get($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ 'addressID' ],['store']);
        if($Check!==true){ return $Check; }

        if(!isset(request()->user['login']) || (isset(request()->user['login']) && request()->user['login'] == false)){
            $Address = EcomUsersAddressesModel::where('id',$values->get('addressID'))->where('ecom_stores_id',$values->get('store'))->where('cancelled',0)->first();
        }else{
            $Address = EcomUsersAddressesModel::where('ecom_users_id',request()->user['user']['id'])->where('id',$values->get('addressID'))->where('ecom_stores_id',$values->get('store'))->where('cancelled',0)->first();
        }

        if(!$Address){
            return $this->APIDisplayResponse('12');
        }
        return response($Address,200);
    }

    public function delete($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ 'addressID' ],['store']);
        if($Check!==true){ return $Check; }

        $Address = EcomUsersAddressesModel::where('ecom_users_id',request()->user['user']['id'])->where('id',$values->get('addressID'))->where('ecom_stores_id',$values->get('store'))->where('cancelled',0)->first();
        if(!$Address){
            return $this->APIDisplayResponse('12');
        }
        $Address->cancelled = 1;
        $Address->save();
        return $this->APIDisplayResponse('25');
    }


    public function store($values = [] , $guest_id = false){
        if($guest_id){
            $user_id = $guest_id;
        }else{
            $user_id = request()->user['user']['id'];
        }

        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ 'label','first_name','last_name','phone', 'phone_country_code','country_code', 'city', 'street' ],['store']);
        if($Check!==true){ return $Check; }

        $PhoneValidation = PhoneFunctionsController::validatePhoneNumber($values->get('phone'),$values->get('phone_country_code'));
        if(!$PhoneValidation['valid']){
            return $this->APIDisplayResponse(38);
        }

        $Country = EcomCountriesModel::where('code',$values->get('country_code'))->where('cancelled',0)->first();
        if(!$Country){
            return $this->APIDisplayResponse(39);
        }

        $AddressValidation = (new \twa\omnidelivery\facades\OmniDeliveryFacade($values->get('store'),NULL))->validateAddress($values->get('country_code'),$values->get('city'),$values->get('postal_code'));
        if($AddressValidation->getStatusCode() != 200){
            return $AddressValidation;
        }
        if($values->get('type')){
            $Address_type = $values->get('type');
        }else{
            $Address_type = "shipping";
        }

        if($values->get('default') && (int) $values->get('default') == 1){
            EcomUsersAddressesModel::select('id')->where('ecom_users_id',$user_id)->where('type',$Address_type)->where('cancelled',0)->update(['default' => 0]);
            $default = 1;
        }else{
            $all_Addresses = EcomUsersAddressesModel::select('id')->where('ecom_users_id',$user_id)->where('type',$Address_type)->where('cancelled',0)->count();
            $default = (int) $all_Addresses==0;
        }

        if($values->get('id')){
            $Address = EcomUsersAddressesModel::where('id',$values->get('id'))->where('ecom_users_id',$user_id)->first();

            if(!$Address)
                $Address = new EcomUsersAddressesModel();
        }else
            $Address = new EcomUsersAddressesModel();

        $Address->ecom_users_id = $user_id;
        $Address->ecom_stores_id = $values->get('store');
        $Address->label = $values->get('label');
        $Address->first_name = $values->get('first_name');
        $Address->last_name = $values->get('last_name');
        $Address->phone = $PhoneValidation['phone'];
        $Address->weight = 0;
        $Address->countries_id = $Country->id;
        $Address->state = $values->get('state');
        $Address->city = $values->get('city');
        $Address->street = $values->get('street');
        $Address->floor = $values->get('floor');
        $Address->building = $values->get('building');
        $Address->postal_code = $values->get('postal_code');
        $Address->longitude = $values->get('longitude');
        $Address->latitude = $values->get('latitude');
        $Address->type = $Address_type;
        $Address->default = $default;
        $Address->delivery_note = $values->get('delivery_note',NULL);
        $Address->save();

        return response([ "success" => $this->APIDisplayResponse('10')->getOriginalContent()['success'], "data" => self::list([ "store" => $values->get('store') ] , $user_id)->getOriginalContent() ],200);
    }


    public function getPhoneCodes($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ ],[]);
        if($Check!==true){ return $Check; }

        $Countries = EcomCountriesModel::select('code')->selectRaw('CAST(phone_code AS UNSIGNED) AS phone_code, 0 AS `selected`')->where('cancelled',0)->orderBy('phone_code','ASC')->groupBy('phone_code')->get();
        foreach ($Countries AS $country){
            if($country->code == country()->code()){
                $country->selected = 1;
            }
        }
        return response($Countries,200);
    }

    public function getCountries($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ ],[ 'store' ]);
        if($Check!==true){ return $Check; }

        $Store = EcomStoreModel::where('id',$values->get('store'))->where('cancelled',0)->first();
        if(!$Store){
            return $this->APIDisplayResponse(42);
        }

        $Countries = EcomCountriesModel::where('cancelled',0)->orderBy('name','ASC')->where('display',1);
        if(is_array($Store->countries_id) && $Store->countries_id && count($Store->countries_id) > 0){
            $Countries = $Countries->whereIn('id',$Store->countries_id);
        }
        $Countries = $Countries->get();
        return response($Countries,200);
    }

    public function getStates($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ 'country_code' ],[ 'store' ]);
        if($Check!==true){ return $Check; }

        return (new \twa\omnidelivery\facades\OmniDeliveryFacade($values->get('store'),NULL))->getStates($values->get('country_code'));
    }

    public function getCities($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ 'country_code' ],[ 'store' ]);
        if($Check!==true){ return $Check; }
        return (new \twa\omnidelivery\facades\OmniDeliveryFacade($values->get('store'),NULL))->getCities(request()->input('country_code'),$values->get('state'));
    }

    public function validateAddress($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ 'country_code', 'city' ],[ 'store' ]);
        if($Check!==true){ return $Check; }

        $Check = TWAAramexFunctions::validateAddress(request()->input('country_code'),request()->input('city'),request()->input('postal_code'));
        if($Check["status"] == "fail"){
            return $this->APIDisplayResponse(40,". ".$Check["message"], ". The Delivery integration company returned this error: ".$Check["message"]);
        }else{
            return $this->APIDisplayResponse(41);
        }
    }

    public function setAddressAsDefault($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ 'addressID', 'type' ],['store']);

        EcomUsersAddressesModel::where('ecom_users_id',request()->user['user']['id'])->where('type', $values->get('type'))->where('cancelled',0)->update(['default' => 0]);

        $Address = EcomUsersAddressesModel::where('id',$values->get('addressID'))->where('ecom_users_id',request()->user['user']['id'])->where('type', $values->get('type'))->first();
        if($Address) {
            $Address->default = 1;
            $Address->save();
        }

        return response()->json(['address' => $Address], 200);
    }

    public function setAsDefault($id, $type){

        EcomUsersAddressesModel::where('ecom_users_id',request()->user['user']['id'])->where('type', $type)->where('cancelled',0)->update(['default' => 0]);

        $Address = EcomUsersAddressesModel::where('id',$id)->where('ecom_users_id',request()->user['user']['id'])->where('type', $type)->first();
        if($Address) {
            $Address->default = 1;
            $Address->save();
        }

        return $Address;
    }


    public function getBilling(){

        EcomUsersAddressesModel::where('ecom_users_id',request()->user['user']['id'])->where('type', "billing")->orderBY('id',"DESC")->where('cancelled',0)->first();
        if(!$Address){
            return $this->APIDisplayResponse('12');
        }

        return response($Address,200);
    }
}
