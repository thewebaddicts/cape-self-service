<?php

namespace twa\ecomprofile\controllers;

use App\Http\Controllers\Controller;
use http\Env\Response;
use Illuminate\Database\Eloquent\Model;
use twa\apilibs\traits\APITrait;
use Barryvdh\Debugbar\Facade as Debugbar;
use twa\ecomcart\models\EcomLoyaltySettingsModel;
use twa\ecomcart\models\EcomLoyaltyTierModel;
use twa\ecomgeneral\models\EcomUsersModel;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;
use twa\ecomprofile\models\EcomUsersLoyaltyTierModel;

class EcomUsersLoyaltyController extends Controller
{
    use APITrait;


    public function get($values = [], $UserID = false){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ ],[ "store" ]);
        if($Check!==true){ return $Check; }

        //we check if the user is authenticated
        if(!request()->user || !isset(request()->user['login']) || !request()->user['login']){ return false; }

        //we check that the schema has wallet enabled
        if(!Schema::hasTable('ecom_users_loyalty_balances') || !isset(request()->user['user']['id'])){
            return false;
        }


        $Group = \twa\ecomcart\models\EcomLoyaltySettingsModel::where('ecom_stores_id','LIKE','%"'.$values->get('store').'"%')->first();
        $Balance = \twa\ecomcart\models\EcomLoyaltyBalanceModel::where('ecom_users_id',request()->user['user']['id'])->whereIn('ecom_stores_id',$Group['ecom_stores_id'])->sum('balance');
        return $Balance;
    }


    public static function getUserTier($UserID,$StoreID){
        $CurrentUserTier = EcomUsersLoyaltyTierModel::where('ecom_users_id',$UserID)->where('ecom_stores_id',$StoreID)->where('cancelled',0)->orderBy('id','DESC')->first();
        if(!$CurrentUserTier){ return false; }
        $Tier = EcomLoyaltyTierModel::where('id',$CurrentUserTier['ecom_loyalty_tiers_id'])->first();
        if(!$Tier){ return false; }

        return $Tier;
    }


    public static function getBalancesRaw($UserID,$StoreID){
        $Group = \twa\ecomcart\models\EcomLoyaltySettingsModel::where('ecom_stores_id','LIKE','%"'.$StoreID.'"%')->first();
        $Balance = \twa\ecomcart\models\EcomLoyaltyBalanceModel::where('ecom_users_id',$UserID)->whereIn('ecom_stores_id',$Group['ecom_stores_id'])->orderBy('expiry_at','ASC')->get();
        return $Balance;
    }

    public function getBalances($values = [], $UserID = false){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ ],[ "store" ]);
        if($Check!==true){ return $Check; }

        //we check if the user is authenticated
        if(!request()->user || !isset(request()->user['login']) || !request()->user['login']){ return false; }

        //we check that the schema has wallet enabled
        if(!Schema::hasTable('ecom_users_loyalty_balances') || !isset(request()->user['user']['id'])){
            return false;
        }

        $Balance = self::getBalancesRaw(request()->user['user']['id'],$values->get('store'));
        $NewBalance = [];
        foreach ($Balance AS $row){
            $NewBalance[]= [ "balance" => $row['balance_available'], "description" => $row['description'], "expiry_at" => $row['expiry_at'], "expiry_at_human" => Carbon::parse($row['expiry_at'])->diffForHumans()  ];
        }
        return response(["Total" => $this->get($values,request()->user['user']['id']), "Balances" => $NewBalance, "Tier" => self::getUserTier(request()->user['user']['id'],$values->get('store')) ],200);
    }


    public function redeem($values = [], $UserID = false){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ "value" ],[ "store" ]);
        if($Check!==true){ return $Check; }

        if(!Schema::hasTable('ecom_users_loyalty_balances') || !isset(request()->user['user']['id'])){
            return false;
        }

        //we check if the user is authenticated
        if(!request()->user || !isset(request()->user['login']) || !request()->user['login']){ return false; }

        $Balance = $this->get($values);
        if($values->get('value') > $Balance){
            return self::APIDisplayResponse(50);
        }

        $Tier = self::getUserTier(request()->user['user']['id'],$values->get('store'));
        $RedeemValue = floor($Tier['redeem'] * $values->get('value'));

        $Transaction = EcomUsersWalletController::addTransaction(request()->user['user']['id'],$RedeemValue,"Redeem of ".$values->get('value')." into the digital wallet",$values->get('store'));

        self::removeBalance(request()->user['user']['id'],$values->get('store'),$values->get('value'));

        return response($Transaction,200);
    }


    public static function removeBalance($UserID,$StoreID,$Value){
        $Balances = self::getBalancesRaw($UserID,$StoreID);
        foreach ($Balances AS $balance){
            if($balance->balance_available >= $Value){
                $balance->balance_available =  $balance->balance_available - $Value;
                $balance->save();
                $Value = 0;
                break;
            }else{
                $Value = $Value - $balance->balance_available;
                $balance->balance_available =  0;
                $balance->save();
            }
        }

        return true;

    }

}
