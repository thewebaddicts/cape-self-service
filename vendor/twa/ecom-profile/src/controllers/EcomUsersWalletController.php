<?php

namespace twa\ecomprofile\controllers;

use App\Http\Controllers\Controller;
use twa\apilibs\traits\APITrait;
use Barryvdh\Debugbar\Facade as Debugbar;
use twa\ecomgeneral\models\EcomUsersModel;
use Illuminate\Support\Facades\Schema;

class EcomUsersWalletController extends Controller
{
    use APITrait;

    public static function get($UserID,$store = false){
        //we check that the schema has wallet enabled
        if(!Schema::hasTable('ecom_users_digitalwallet_balances')){
            return false;
        }

        $Balance = \twa\ecomprofile\models\EcomUsersWalletModel::where('ecom_users_id',$UserID)->sum('balance');
        return $Balance;
    }

    public static function addTransaction($UserID,$Balance,$Description,$StoreID = false){
        //we check that the schema has wallet enabled
        if(!Schema::hasTable('ecom_users_digitalwallet_balances')){
            return false;
        }

        $BalanceRow = new \twa\ecomprofile\models\EcomUsersWalletModel();
        $BalanceRow->ecom_users_id = $UserID;
        $BalanceRow->balance = $Balance;
        $BalanceRow->description = $Description;
        $BalanceRow->ecom_stores_id = $StoreID;
        $BalanceRow->save();
        return $BalanceRow;
    }


}
