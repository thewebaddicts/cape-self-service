<?php

namespace twa\ecomprofile\providers;

use Illuminate\Support\ServiceProvider;

Class WebsiteServiceProvider extends ServiceProvider{
    public function boot(){
//        $this->publishes([
//            __DIR__.'/../config/ecom.php' => config_path('ecom.php'),
//        ], 'config');
    }

    public function register(){
//        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->loadRoutesFrom(__DIR__.'/../routes/api.php');
//        $this->app->make('twa\ecomAuth\');
    }


}
