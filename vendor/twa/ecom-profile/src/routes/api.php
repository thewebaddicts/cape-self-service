<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => [ \twa\apilibs\middleware\TWAApiLocaleMiddleware::class ] ,'prefix'=>'/api/v1'], function(){
    Route::group(['middleware' => [] ,'prefix'=>'/users'], function(){
        Route::group(['middleware' => [ \twa\ecomauth\middleware\AuthenticationRequiredToken::class ] ], function() {
            Route::post('/addresses/list', function () { return (new \twa\ecomprofile\controllers\EcomUsersAddressesController)->list(); });
            Route::post('/addresses/get', function () { return (new \twa\ecomprofile\controllers\EcomUsersAddressesController)->get(); });
            Route::post('/addresses/create', function () { return (new \twa\ecomprofile\controllers\EcomUsersAddressesController)->store(); });
            Route::post('/addresses/delete', function () { return (new \twa\ecomprofile\controllers\EcomUsersAddressesController)->delete(); });
            Route::post('/addresses/validate', function () { return (new \twa\ecomprofile\controllers\EcomUsersAddressesController)->validateAddress(); });
            Route::post('/address/set/default', function () { return (new \twa\ecomprofile\controllers\EcomUsersAddressesController)->setAddressAsDefault(); });

            Route::post('/loyalty/balance', function () { return (new \twa\ecomprofile\controllers\EcomUsersLoyaltyController())->getBalances(); });
            Route::post('/loyalty/redeem', function () { return (new \twa\ecomprofile\controllers\EcomUsersLoyaltyController())->redeem(); });
        });
    });

    Route::group(['middleware' => [] ,'prefix'=>'/utilities/countries'], function(){
        Route::post('/list', function () { return (new \twa\ecomprofile\controllers\EcomUsersAddressesController)->getCountries(); });
        Route::post('/states/list', function () { return (new \twa\ecomprofile\controllers\EcomUsersAddressesController)->getStates(); });
        Route::post('/cities/list', function () { return (new \twa\ecomprofile\controllers\EcomUsersAddressesController)->getCities(); });
        Route::post('/phones/list', function () { return (new \twa\ecomprofile\controllers\EcomUsersAddressesController)->getPhoneCodes(); });
    });


});
