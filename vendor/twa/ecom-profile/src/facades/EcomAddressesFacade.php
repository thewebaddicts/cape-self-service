<?php

namespace twa\ecomprofile\facades;

use App\Http\Controllers\Controller;
use twa\apilibs\traits\APITrait;
use Illuminate\Support\Facades\Validator;
use twa\ecomprofile\controllers\EcomUsersAddressesController;

class EcomAddressesFacade extends Controller
{
    public function renderList($store_prefix,$lang){
        $Addresses = (new \twa\ecomprofile\controllers\EcomUsersAddressesController)->list([ "store" => request()->store['id'] ]);
        if($Addresses->getStatusCode() == "200"){
            return view(config('ecom.default_views.address_listing'),[ "addresses" => $Addresses->getOriginalContent() ]);
        }else{
            if(!property_exists($ResponseObject,"error")){
                return redirect()->back();
            }
            return redirect()->route('account-profile',['notification_title' => $ResponseObject->error->title, 'notification_message' => $ResponseObject->error->message] )->withInput(request()->input())->withErrors($ResponseObject->error->message);
        }
    }




    public function renderAdd($store_prefix,$lang){
        $CountryCodes = (new \twa\ecomprofile\controllers\EcomUsersAddressesController)->getPhoneCodes([ "store" => request()->store['id'] ]);
        if($CountryCodes->getStatusCode() != 200){ $CountryCodes = response([],200); }

        $Countries = (new \twa\ecomprofile\controllers\EcomUsersAddressesController)->getCountries([ "store" => request()->store['id'] ]);
        if($Countries->getStatusCode() != 200){ $CountryCodes = response([],200); }
        return view(config('ecom.default_views.address_form'),[ "CountryCodes" => $CountryCodes->getOriginalContent(), "Countries" => $Countries->getOriginalContent() ]);
    }

    public function renderStates($store_prefix,$lang){
        $States = (new \twa\ecomprofile\controllers\EcomUsersAddressesController)->getStates([ "store" => request()->store['id'], 'country_code'  => request()->input('country_code') ]);
        if($States->getStatusCode() != 200){ $States = response([],200); }
        return view(config('ecom.default_views.address_states'),[ "States" => $States->getOriginalContent() ]);
    }

    public function renderCities($store_prefix,$lang){

        $Cities = (new \twa\ecomprofile\controllers\EcomUsersAddressesController)->getCities([ "store" => request()->store['id'], 'country_code'  => request()->input('country_code'), 'state'  => request()->input('state') ]);
        if($Cities->getStatusCode() != 200){ $Cities = response([],200); }
        return view(config('ecom.default_views.address_cities'),[ "Cities" => $Cities->getOriginalContent() ]);
    }

    public static function saveAddress($store_prefix,$lang){
        $validator = Validator::make(request()->all(),
            [
                'label' => 'required',
//                'zip_code' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
//                'state' => 'required',
                'city' => 'required',
                'street' => 'required',
                'phone' => 'required',
                'country' => 'required',
//                'type' => 'required',
            ])->validate();

        $Response = (new \twa\ecomprofile\controllers\EcomUsersAddressesController)->store([ "store" => request()->store['id'],'label' => request()->input('label'), 'first_name' => request()->input('first_name'),'last_name' => request()->input('last_name'), 'country_code' => request()->input('country'), 'state' => request()->input('state'), 'city' => request()->input('city'), 'street' => request()->input('street'), 'floor' => request()->input('floor'), 'building' => request()->input('building'), 'phone_country_code' => request()->input('phone_country_code'), 'phone' => request()->input('phone'),  'postal_code' => request()->input('zip_code'), 'delivery_note' => request()->input('delivery_note'), 'type' => request()->input('type'), 'default' => 0 ]);
        $ResponseObject = $Response->getOriginalContent();
        if($Response->getStatusCode() == "200"){
            if(request()->input('redirect','account/addresses/list') == "account/addresses/list"){
                return redirect()->route('account-addresses',['notification_title'=>$ResponseObject['success']['title'],'notification_message'=>$ResponseObject['success']['message']]);
            }else{
                return redirect(request()->input('redirect','account/addresses/list').'?notification_title='.$ResponseObject['success']['title'].'&notification_message='.$ResponseObject['success']['message']);
            }
        }else{
            return redirect(ecom('url')->prefix().'/account/addresses/add?redirect='.\request()->input('redirect').'&notification_title='.$ResponseObject['error']['title'].'&notification_message='.$ResponseObject['error']['message'])->withInput(request()->input())->withErrors($ResponseObject['error']['message']);
        }
    }


    public static function deleteAddress($store_prefix,$lang,$address_id){
        $Response = (new \twa\ecomprofile\controllers\EcomUsersAddressesController)->delete([ "store" => request()->store['id'], "addressID" => $address_id ]);
        $ResponseObject = $Response->getOriginalContent();
        if($Response->getStatusCode() == "200"){
            return redirect()->route(config('ecom.default_routes.addresses'),['notification_title'=>$ResponseObject['success']['title'], 'notification_message'=>$ResponseObject['success']['message']]);
        }else{
            return redirect()->route(config('ecom.default_routes.addresses'),['notification_title'=>$ResponseObject['error']['title'], 'notification_message'=>$ResponseObject['error']['message']]);
        }
    }

    public static function setDefaultAddress($store_prefix,$lang,$address_id){
        $address = (new \twa\ecomprofile\controllers\EcomUsersAddressesController)->setAsDefault($address_id , 'shipping');

        if($address) {
            return response()->json(['address' => $address], 200);
        }
        return response()->json([], 400);
    }

}
