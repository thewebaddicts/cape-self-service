<?php

namespace twa\ecomprofile\facades;

use App\Http\Controllers\Controller;
use twa\apilibs\traits\APITrait;
use Illuminate\Support\Facades\Validator;
use twa\ecomprofile\controllers\EcomUsersAddressesController;

class EcomProfileFacade extends Controller
{
    public function renderProfile($store_prefix,$lang){
        return view(config('ecom.default_views.profile','pages.account.account-profile'),[ "profile" => request()->user->user ]);
    }

    public static function updateProfile($store_prefix,$lang){
        $validator = Validator::make(request()->all(),
            [
                'email' => 'email',
            ])->validate();

        $Update = (new \twa\ecomauth\controllers\EcomUsersRegistration)->update([ "token" => request()->user->token, 'first_name' => request()->input('first_name'), 'last_name' => request()->input('last_name'), 'email' => request()->input('email'), 'phone' => request()->input('phone'),'phone_country_code' => request()->input('phone_country_code')]);
        $ResponseObject = $Update->getOriginalContent();
        if ($Update->getStatusCode() == "200") {
            return redirect()->route('account-profile',['notification'=> $ResponseObject["success"]["message"]]);
        } else {
            return redirect()->route('account-profile',['notification_title' => $ResponseObject["error"]["title"], 'notification_message'=> $ResponseObject["error"]["message"]])->withInput(request()->input())->withErrors($ResponseObject["error"]["message"]);
        }
    }

    public static function changePassword($store_prefix,$lang){
        $validator = Validator::make(request()->all(),
            [
                'old_password' => 'required',
                'password' => 'required|min:7',
                'confirm_password' => ['same:password'],
            ])->validate();
        $Response = \Illuminate\Support\Facades\Http::withHeaders(['token' => session('login')->token ])->post(env('API_URL') . '/users/changepass', ['oldpassword' => request()->input('old_password'), 'password' => request()->input('password')]);
        $ResponseObject = json_decode($Response->body());
        if ($Response->status() == "200") {
            session(['login' => false]);
            Session::flush();
            return redirect()->route('login',['notification_title'=>$ResponseObject->success->title,'notification_message'=>$ResponseObject->success->message]);
        } else {
            return redirect()->route('account-profile',['notification_title' => $ResponseObject->error->title, 'notification_message'=> $ResponseObject->error->message])->withInput(request()->input())->withErrors($ResponseObject->error->message);
        }
    }



    public static function updateProfileMobile($store_prefix,$lang){
        $validator = Validator::make(request()->all(),
            [
                'email' => 'email',
            ])->validate();

        $Update = (new \twa\ecomauth\controllers\EcomUsersRegistration)->update([ "token" => request()->user->token, 'first_name' => request()->input('first_name'), 'last_name' => request()->input('last_name'), 'email' => request()->input('email'), 'phone' => request()->input('phone'),'phone_country_code' => request()->input('phone_country_code')]);
        $ResponseObject = $Update->getOriginalContent();
        if ($Update->getStatusCode() == "200") {
            return redirect()->route('account-profile',['notification'=> $ResponseObject["success"]["message"]]);
        } else {
            return redirect()->route('account-profile',['notification_title' => $ResponseObject["error"]["title"], 'notification_message'=> $ResponseObject["error"]["message"]])->withInput(request()->input())->withErrors($ResponseObject["error"]["message"]);
        }
    }

}
