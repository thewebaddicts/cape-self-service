<?php

namespace twa\httprequests;

use App\Http\Controllers\Controller;
use Illuminate\Http\Client\Pool;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;
use Barryvdh\Debugbar\Facade AS Debugbar;

class httpRequest extends Controller
{
    protected $requests;
    protected $info;
    protected $from_cache = [];

    public function __construct(){

    }

    public function add($object){
        $this->requests[] = $object;
    }

    function generatePool($pool){
        $arr = [];
        foreach ($this->requests AS $request){
            if(!$request['headers']){
                $request['headers'] = [];
            }

            $this->info[$request['key']]=$request;

            $data = false;
            //we check if this item is cached
            if(array_key_exists("cache",$request) && $request["cache"]==1){
                $data = Cache::get(self::createCacheKey($request));
                if($data){
                    $this->from_cache[$request['key']] = $data;
                    Debugbar::addMessage("Loading the request = ".$request['key']." From the cache","TWA Requests");
                }
            }
            if(!$data){
                Debugbar::addMessage("Adding the request = ".$request['key']." to the HTTP Requests pool","TWA Requests");
                if(strtoupper($request['method']) == "POST"){
                    $arr[] = $pool->as($request['key'])->withHeaders($request['headers'])->post($request['url'], $request['parameters']);
                }else{
                    $arr[] = $pool->as($request['key'])->withHeaders($request['headers'])->get($request['url'], $request['parameters']);
                }
            }
        }
        return $arr;
    }


    public function execute(){
        $responses = Http::pool(function (Pool $pool) {
            return self::generatePool($pool);
        });

        $return = [];
        foreach ($responses AS $key => $reponse){
            if(!$reponse->ok()){
                $return[$key]['error'] = true;
            }else{
                try{
                    $body = json_decode($reponse->body());
                    //check if $body has error and return an error object
                    $return[$key]['error'] = false;
                    $return[$key]['body'] = $body;

                    //we check in the info if this key got cache
                    if(array_key_exists("cache",$this->info[$key]) && $this->info[$key]["cache"]==1){
                        Cache::put(self::createCacheKey($this->info[$key]), $return[$key], 3600*24*30);
                    }
                }catch (\Throwable $e){
                    $return[$key]['error'] = true;
                }
            }
        }
        foreach($this->from_cache AS $key => $response){
            if(!array_key_exists($key,$return)){
                $return[$key] = $response;
            }
        }
        return $return;
    }


    public static function createCacheKey($info){
        return $info["url"]."|".http_build_query($info['parameters'])."|".http_build_query($info['headers']);
    }

}
