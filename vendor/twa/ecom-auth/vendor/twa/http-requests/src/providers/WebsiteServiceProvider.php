<?php

namespace twa\httprequests\providers;

use Illuminate\Support\ServiceProvider;

Class WebsiteServiceProvider extends ServiceProvider{
    public function boot(){
//        dd('im here');
    }

    public function register(){
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->app->make('twa\httprequests\httpRequest');
    }

}
