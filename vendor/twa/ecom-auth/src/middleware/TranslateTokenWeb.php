<?php

namespace twa\ecomauth\middleware;

use twa\ecomauth\models\EComTemporaryUsersModel;
use twa\ecomauth\models\EcomUsersModel;
use twa\ecomauth\models\EcomCartModel;
use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode as Middleware;
use Closure;
use function PHPUnit\Framework\stringContains;
use twa\apilibs\traits\APITrait;
use Cookie;

class TranslateTokenWeb extends Middleware
{
    /**
     * The URIs that should be reachable while maintenance mode is enabled.
     *
     * @var array
     */
    use APITrait;
    protected $except = [
        //
    ];

    public function handle($request, Closure $next)
    {
        /*
         * Ultimately by the end of this Middleware, we are aiming to return an Collection of User info
         * This colleciton got the following structure
         * |-- user => The user information object, for both temporary users and registered users
         * |-- token => The token of the user
         * |-- column => can be either ecom_users_id or ecom_users_temporary_id depending on the type of the user
         *
         * on a side note, if the user is not registered and no temporary session is there, we return NULL for all thos information
         */
        $User = false;
        $return['column'] = NULL;
        $return['user'] = NULL;
        $return['token'] = NULL;

        if(\twa\ecomauth\facades\EcomUsersFacade::loginCheck()){
            $User = collect(EcomUsersModel::where('token',session('login')->token)->where('verified',1)->where('cancelled',0)->first());
            if(!$User || count($User) == 0){
                \Session::flush();
                $return['column'] = NULL;
                $return['user'] = NULL;
                $return['token'] = NULL;
            }else{
                $return['column'] = "ecom_users_id";
                $return['user'] = $User;
                $return['token'] = $User["token"];
            }
            $column = "ecom_users_id";
        }
        if(!$User){
            $User = \twa\ecomauth\facades\EcomUsersFacade::checkOfflineToken($request);
            $return['column'] = "ecom_users_temporary_id";
            $return['user'] = $User;
            $return['token'] = $User["token"];
            $column = "ecom_users_temporary_id";
        }
        unset($User->password);
        $request->user = collect($return)->recursive();
        if($column == "ecom_users_temporary_id"){
//            Cookie::queue(Cookie::make('offlineToken', $User["token"], 129600*129600));
            setcookie('offlineToken', $User['token'], time()+60*24*60*3600,"/");
            return $next($request);
        }else{
            return $next($request);
        }

    }

}
