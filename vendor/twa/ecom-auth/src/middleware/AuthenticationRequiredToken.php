<?php

namespace twa\ecomauth\middleware;

use twa\ecomauth\models\EComTemporaryUsersModel;
use twa\ecomauth\models\EcomUsersModel;
use twa\ecomauth\models\EcomCartModel;
use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode as Middleware;
use Closure;
use function PHPUnit\Framework\stringContains;
use twa\apilibs\traits\APITrait;

class AuthenticationRequiredToken extends Middleware
{
    /**
     * The URIs that should be reachable while maintenance mode is enabled.
     *
     * @var array
     */
    use APITrait;
    protected $except = [
        //
    ];

    public function handle($request, Closure $next)
    {
        $session_token = false;
        if(session('login') && is_object(session('login')) && property_exists(session('login'),"token")){ $session_token = session('login')->token; }
        $token = request()->header('token',$session_token);


        if(!request()->header('token') && !$session_token){
            return $this->APIDisplayCustomResponse(0, "error", "token is missing", "This API requires an authentication token in order to run properly", "");
        }

        $User = EcomUsersModel::where('token',$token)->where('verified',1)->where('cancelled',0)->first();
        if(!$User){
            return $this->APIDisplayCustomResponse(0, "error", "invalid token", "The token provided is not valid", "");
        }

        unset($User->password);
        $return['column'] = "ecom_users_id";
        $return['user'] = $User;
        $return['token'] = $User["token"];
        $request->user = collect($return)->recursive();
        return $next($request);
    }

}
