<?php

namespace twa\ecomauth\middleware;

use Illuminate\Support\Facades\URL;
use twa\ecomauth\models\EComTemporaryUsersModel;
use twa\ecomauth\models\EcomUsersModel;
use twa\ecomauth\models\EcomCartModel;
use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode as Middleware;
use Closure;
use function PHPUnit\Framework\stringContains;
use twa\apilibs\traits\APITrait;

class AuthenticationRequiredCheck extends Middleware
{
    /**
     * The URIs that should be reachable while maintenance mode is enabled.
     *
     * @var array
     */
    use APITrait;
    protected $except = [
        //
    ];

    public function handle($request, Closure $next)
    {
        if(!request()->user || !request()->user->login || !property_exists(request()->user,'column') || request()->user->column != "ecom_users_id" || !request()->user->login){
            return redirect()->route(config('ecom.default_routes.login'));
        }
        return $next($request);
    }

}
