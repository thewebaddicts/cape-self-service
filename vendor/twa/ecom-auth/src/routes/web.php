<?php

use Illuminate\Support\Facades\Route;
use twa\translationssync\translationsSyncController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/twa/translations/sync', function () {
//    return translationsSyncController::FetchLanguagesFromAPI();
//});

Route::get('/ecom/users/forgot/form/{id}/{token}', function ($id,$token) {
    $Challenge = \twa\ecomauth\models\EcomUsersPasswordResetChallengesModel::where('id',$id)->first();
    if(!$Challenge){ abort(404); }
    if($token != md5($Challenge->challenge)){ abort(404); }
    return view('EcomAuthViews::changepass.default',[ "challenge" => $Challenge ]);
});

Route::get('/ecom/users/login/{newtwork}/callback',function($network){ return (new \twa\ecomauth\facades\EcomUsersRegistrationFacade())->socialLogin($network); } );


Route::post('/ecom/users/forgot/form/{id}/{token}', function ($id,$token) {
    $Challenge = \twa\ecomauth\models\EcomUsersPasswordResetChallengesModel::where('id',$id)->first();
    if(!$Challenge){ abort(404); }
    if($token != md5($Challenge->challenge)){ abort(404); }
    $User = \twa\ecomauth\models\EcomUsersModel::where('id',$Challenge->ecom_users_id)->where('cancelled',0)->first();
    if(!$User){ abort(404); }
    if(request()->input('confirm_password') != request()->input('password')){ echo "passwords does not match"; exit(); }
    $User->password = md5(request()->input('password'));
    $User->save();
    echo "password successfully changed. <a href='".env('APP_URL')."'>proceed to the website</a>";
});

