<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => [ \twa\apilibs\middleware\TWAApiLocaleMiddleware::class ] ,'prefix'=>'/api/v1'], function(){
    Route::group(['middleware' => [] ,'prefix'=>'/users'], function(){
        Route::post('/create', function () { return (new \twa\ecomauth\controllers\EcomUsersRegistration)->store(); });
        Route::post('/login', function () { return (new \twa\ecomauth\controllers\EcomUsersRegistration)->login(); });
        Route::post('/login/social', function () { return (new \twa\ecomauth\controllers\EcomUsersSocialLogin)->socialLogin(); });

        Route::get('/verify/{id}/{token}', function ($id,$token) { return (new \twa\ecomauth\controllers\EcomUsersRegistration)->verify($id,$token); });
        Route::post('/check', function () { return (new \twa\ecomauth\controllers\EcomUsersRegistration)->tokentouser(); });
        Route::post('/forgot', function () { return (new \twa\ecomauth\controllers\EcomUsersRegistration)->forgot(); });
        Route::post('/update', function () { return (new \twa\ecomauth\controllers\EcomUsersRegistration)->update(); });
        Route::post('/changepass', function () { return (new \twa\ecomauth\controllers\EcomUsersRegistration)->changepass(); });
        Route::group(['middleware' => [] ,'prefix'=>'/mobile'], function(){
            Route::post('/login', function () { return (new \twa\ecomauth\controllers\EcomUsersRegistration)->loginMobile(); });
            Route::post('/verify', function () { return (new \twa\ecomauth\controllers\EcomUsersRegistration)->verifyMobile(); });
            Route::post('/update', function () { return (new \twa\ecomauth\controllers\EcomUsersRegistration)->updateMobile(); });
        });
    });



    Route::group(['middleware' => [] ,'prefix'=>'/users/temporary'], function(){
        Route::post('/create', function () { return (new \twa\ecomauth\controllers\EcomTemporaryUsersController)->createTemporarySession(); });
        Route::group(['middleware' => [ twa\ecomauth\middleware\CheckTemporaryToken::class ] ], function() {
            Route::post('/get', function () { return (new \twa\ecomauth\controllers\EcomTemporaryUsersController)->retrieveTemporarySession(); });
        });
    });
});
