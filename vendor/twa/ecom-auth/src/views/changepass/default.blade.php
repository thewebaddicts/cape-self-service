<!DOCTYPE html>
<html lang="{{str_replace('_', '-', app()->getLocale())}}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/images/FavIcon.png" type="image/png" >
    <!-- Icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@300;400;700&display=swap" rel="stylesheet">
    <script language="javascript" src="https://code.jquery.com/jquery-3.6.0.slim.min.js"></script>
    <style>
        *{
            font-family: 'Rubik', sans-serif; font-size: 13px; color:#000; font-weight: 300;
        }
        body{
            background-color: #f6f6f6; display: flex; min-height: 100vh; align-items: center; justify-content: center;
        }
        .content{
            width: 100%; max-width: 600px; background-color: #fff; padding: 40px;
        }
        h1{ font-size:18px; font-weight: 300; margin-bottom: 20px; }
        input[type=text],input[type=password]{
            border:0; border-bottom: 1px solid #CCC; height: 45px; width: 100%; padding: 0px; margin:5px 0; position: relative; z-index:0; transition: 0.3s all;
        }
        input[type=text]:focus,input[type=password]:focus{
            border-bottom: 1px solid #000;
        }
        button{
            background-color: transparent; float:right; border:0; height: 45px; margin-top: 20px; display: block; transition: 0.3s all;
        }
        button i{
            font-size: 11px; padding-right: 7px; transition: 0.3s all;
        }
        button:hover i{
           transform: translateX(5px);
        }
        #message{ float:left; height: 45px; line-height: 45px; }
        *:focus{
            outline: none;
        }
    </style>
</head>
<body>
<div class="content">
    <form method="post">
        @csrf
        <h1>Change your password</h1>
        <input type="password" id="password" name="password" placeholder="Your new password" minlength="6">
        <input type="password" id="confirm_password" name="confirm_password" placeholder="Your new password again"  minlength="6">
        <div id="message"></div>
        <button type="submit"> <i class="fas fa-chevron-right"></i> Change Password </button>
    </form>
</div>
<script language="javascript">
    $('#password, #confirm_password').on('keyup', function () {
        if ($('#password').val() == $('#confirm_password').val()) {
            $('#message').html('Matching').css('color', 'green');
        } else
            $('#message').html('Not Matching').css('color', 'red');
    });
</script>
</body>
</html>



