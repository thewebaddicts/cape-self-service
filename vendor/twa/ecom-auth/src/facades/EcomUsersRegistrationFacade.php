<?php

namespace twa\ecomauth\facades;

use App\Http\Controllers\Controller;
use twa\apilibs\traits\APITrait;
use twa\ecomauth\controllers\EcomUsersRegistration;
use Illuminate\Support\Facades\Validator;

class EcomUsersRegistrationFacade extends Controller
{
    use APITrait;

    public function login()
    {
        if(isset($_COOKIE['offlineToken'])){
            $OfflineToken = $_COOKIE['offlineToken'];
        }else{
            $OfflineToken = false;
        }

        $Response = (new EcomUsersRegistration)->login();
        $ResponseObject = $Response->getData();

        if ($Response->status() == "200") {
            session(["login" => $ResponseObject]);
//            if(request()->input('redirect','profile') == "profile"){
//                return redirect()->route('account-profile');
//            }else{
//                return redirect(request()->input('redirect','profile'));
//            }
            return redirect()->intended('/');

        } else {
            return redirect()->route(config('ecom.authentication.login','login'),['notification_title' => $ResponseObject->error->title, 'notification_message'=> $ResponseObject->error->message])->withInput(request()->input())->withErrors($ResponseObject->error->message);
        }
    }

    public function forgot(){
        $Response = (new EcomUsersRegistration)->forgot();
        $ResponseObject = $Response->getData();
        if ($Response->status() == "200") {
            return redirect()->route(config('ecom.default_routes.login','forgot'),['notification'=> $ResponseObject->success->message]);
        } else {
            return redirect()->route(config('ecom.default_routes.forgot','forgot'),['notification_title'=> $ResponseObject->error->title ,'notification_message'=>$ResponseObject->error->message])->withInput(request()->input())->withErrors($ResponseObject->error->message);
        }
    }

    public function logout(){
        session(['login' => false]);
        \Session::flush();
        return redirect('/');
    }

    public function flushCookies(){
        if (isset($_SERVER['HTTP_COOKIE'])) {
            $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
            foreach($cookies as $cookie) {
                $parts = explode('=', $cookie);
                $name = trim($parts[0]);
                setcookie($name, '', time()-1000);
                setcookie($name, '', time()-1000, '/');
            }
        }
    }

    public function store(){
        $validator = Validator::make(request()->all(),
            [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email',
                'phone' => 'required',
                'password' => 'required|min:7',
                'confirm_password' => ['same:password'],
            ])->validate();

        $Response = (new EcomUsersRegistration)->store();
        $ResponseObject = $Response->getData();

        if ($Response->status() == "200") {
            return redirect()->route(config('ecom.default_routes.login','login'),['notification' =>  $ResponseObject->success->message]);
        } else {
            return redirect()->route(config('ecom.default_routes.register','register'),['notification_title' => $ResponseObject->error->title , 'notification_message' => $ResponseObject->error->message])->withInput(request()->input())->withErrors($ResponseObject->error->message);
        }
    }



    function socialLogin($network){

        switch ($network){
            case "facebook":
                $response = (new \twa\ecomauth\controllers\EcomUsersSocialLogin)->handleCallBackFacebook();
                break;
            case "google":
                $response = (new \twa\ecomauth\controllers\EcomUsersSocialLogin)->handleCallBackGoogle();
                break;
        }
        if($response->getStatusCode() == 200){
            session([ "login" => collect($response->getOriginalContent())->recursive() ]);
            try {
                return redirect()->route('profile');
            }catch (\Throwable $e){
                return redirect('/');
            }

        }else{
            return \twa\ecomgeneral\controllers\HelpersController::processAPIError($response->getOriginalContent());
        }

    }
}
