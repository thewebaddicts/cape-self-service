<?php

namespace twa\ecomauth\facades;

use App\Http\Controllers\Controller;
use twa\ecomauth\controllers\EcomTemporaryUsersController;
use twa\ecomauth\controllers\EcomUsersController;
use Illuminate\Support\Facades\Cookie;
class EcomUsersFacade extends Controller
{

    public static function checkOfflineToken(): \Illuminate\Support\Collection
    {
        if (!isset($_COOKIE['offlineToken'])) {
            return self::syncToken();
        } else {
            $OfflineToken = $_COOKIE['offlineToken'];
            return self::validateOfflineToken($OfflineToken);
        }
    }

    public static function syncToken(): \Illuminate\Support\Collection
    {
        $array = (new EcomTemporaryUsersController)->createTemporarySession();
        $minutes = 3000 * 24;
//        Cookie::queue('offlineToken', $array['token'], $minutes);
        setcookie('offlineToken', $array['token'], time()+60*24*60*3600,"/");
        return collect($array)->recursive();
    }

    public static function validateOfflineToken($token): \Illuminate\Support\Collection
    {
        $Check = EcomTemporaryUsersController::sessionToTemporaryUser($token);
        if(!$Check){
            return self::syncToken();
        }else{
            return $Check;
        }
    }


    public static function loginCheck(): bool
    {
        if (!session('login') || !is_object(session("login")) || !property_exists(session('login'), "token")) {
            return false;
        } else {
            return true;
        }
    }


}
