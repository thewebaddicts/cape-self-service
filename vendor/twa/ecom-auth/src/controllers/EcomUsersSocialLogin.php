<?php

namespace twa\ecomauth\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use twa\apilibs\traits\APITrait;
use twa\apilibs\controllers\PhoneFunctionsController;
use twa\ecomauth\models\EcomUsersModel;
use twa\ecomauth\models\EcomUsersVerificationChallengesModel;
use twa\ecomauth\models\EcomUsersPasswordResetChallengesModel;
use Illuminate\Support\Facades\Mail;
use twa\ecomcart\mail\Receipt;
use twa\ecomgeneral\mail\EmailFromTemplate;
use Carbon\Carbon;

class EcomUsersSocialLogin extends Controller
{
    use APITrait;

    public static function loginOrRegister($email,$first_name,$last_name,$attributes){
        $UserCheck = \twa\ecomauth\models\EcomUsersModel::where('email',$email)->where('verified',1)->where('cancelled',0)->first();
        if(!$UserCheck){
            $UserCheck = new \twa\ecomauth\models\EcomUsersModel();
        }
        $UserCheck->email = $email;
        $UserCheck->first_name = $first_name;
        $UserCheck->last_name = $last_name;
        $UserCheck->verified = 1;
        $UserCheck->token = md5(uniqid());
        if(is_array($attributes)){ $attributes = json_encode($attributes); }
        $UserCheck->cms_attributes = $attributes;
        $UserCheck->save();

        return $UserCheck;

    }

    public function handleCallBackFacebook(){
        try{
            $user = \Socialite::driver('facebook')->user();
        }catch (\Throwable $e){
            return self::APIDisplayResponse(48);
        }
        $name = collect(explode(' ',$user->name));
        return response(self::loginOrRegister($user->email,$name[0],$name->forget(0)->implode(' '),[ "facebook" => [ "id" => $user->id, "avatar" => $user->avatar ] ]),200);
    }

    public function handleCallBackGoogle(){
        try{
            $user = \Socialite::driver('google')->user();
        }catch (\Throwable $e){
            return self::APIDisplayResponse(48);
        }
        $name = collect(explode(' ',$user->name));
        return response(self::loginOrRegister($user->email,$name[0],$name->forget(0)->implode(' '),[ "facebook" => [ "id" => $user->id, "avatar" => $user->avatar ] ]),200);
    }


    public function socialLogin($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ 'first_name', 'last_name', 'email', 'token', 'provider', 'signature'  ]);
        if($Check!==true){ return $Check; }

        if($values->get('signature') != md5($values->get('provider').$values->get('token').$values->get('email'))){
            return $this->APIDisplayResponse(2);
        }

        $UserCheck = \twa\ecomauth\models\EcomUsersModel::where('email',$values->get('email'))->where('cancelled',0)->first();
        if(!$UserCheck){
            $UserCheck = new \twa\ecomauth\models\EcomUsersModel();
        }
        $UserCheck->email = $values->get('email');
        $UserCheck->first_name = $values->get('first_name');
        $UserCheck->last_name = $values->get('last_name');
        $UserCheck->verified = 1;
        $UserCheck->token = md5(uniqid());
        $UserCheck->save();

        return $UserCheck;
    }


}
