<?php

namespace twa\ecomauth\controllers;

use App\Http\Controllers\Controller;
use twa\apilibs\traits\APITrait;
use twa\ecomauth\models\EComTemporaryUsersModel;
use Illuminate\Support\Facades\Mail;
use twa\ecomgeneral\mail\EmailFromTemplate;
use Carbon\Carbon;

class EcomTemporaryUsersController extends Controller
{
    use APITrait;

    public static function createTemporarySession(): \Illuminate\Support\Collection
    {
        $User = new EComTemporaryUsersModel();
        $User->token = 't_'.md5(uniqid());
        $User->cart = json_encode([]);
        $User->ip = request()->ip();
        $User->save();
        return collect($User);
    }

    public static function sessionToTemporaryUser($offlineToken)
    {
        $user = EComTemporaryUsersModel::where('token',$offlineToken)->where('cancelled',0)->first();
        if($user){ return collect($user)->recursive(); }else{ return false; }
    }

    public static function retrieveTemporarySession($values = []){
        return response()->json(request()->TemporaryUser);
    }

}
