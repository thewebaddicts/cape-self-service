<?php

namespace twa\ecomauth\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use twa\apilibs\controllers\PhoneFunctionsController;
use twa\apilibs\traits\APITrait;
use Barryvdh\Debugbar\Facade as Debugbar;
use twa\ecomauth\models\EcomUsersVerificationChallengesModel;
use twa\ecomgeneral\mail\EmailFromTemplate;
use twa\ecomgeneral\models\EcomUsersModel;


class EcomGuestController extends Controller
{
    use APITrait;

    public function store($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ 'first_name', 'last_name', 'email','phone','phone_country_code']);
        if($Check!==true){ return $Check; }
        $PhoneValidation = PhoneFunctionsController::validatePhoneNumber($values->get('phone'),$values->get('phone_country_code'));
        if(!$PhoneValidation['valid']){
            return $this->APIDisplayResponse(38);
        }


        $email = strtolower(str_replace(' ','',strtolower($values->get('email'))));

        $user = new EcomUsersModel();
        $user->first_name = ucwords(strtolower($values->get('first_name')));
        $user->last_name = ucwords(strtolower($values->get('last_name')));
        $user->countries_id = ucwords(strtolower($values->get('country_id')));
        $user->phone = $PhoneValidation['phone'];
        $user->phone_country_code = $values->get('phone_country_code');
        $user->phone_original = $values->get('phone');
        $user->email = $email;
        $user->verified = 0;
        $user->guest_user = 1;
        $user->save();

        return $user;
    }
}
