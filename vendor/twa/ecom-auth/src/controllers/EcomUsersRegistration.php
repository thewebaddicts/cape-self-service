<?php

namespace twa\ecomauth\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use twa\apilibs\traits\APITrait;
use twa\apilibs\controllers\PhoneFunctionsController;
use twa\ecomauth\models\EcomUsersModel;
use twa\ecomauth\models\EcomUsersVerificationChallengesModel;
use twa\ecomauth\models\EcomUsersPasswordResetChallengesModel;
use Illuminate\Support\Facades\Mail;
use twa\ecomcart\mail\Receipt;
use twa\ecomgeneral\mail\EmailFromTemplate;
use Carbon\Carbon;

class EcomUsersRegistration extends Controller
{
    use APITrait;


    public function validatetoken($values = []){
        $values = $this->ProcessValues($values);
        if(!request()->header('token') && !$values->get('token')){ return $this->APIDisplayResponse(1); }
        $token = request()->header('token',$values->get('token'));
        $User = EcomUsersModel::select('id')->where('token',$token)->where('cancelled',0)->where('verified',1)->first();
        if(!$User){ return $this->APIDisplayResponse(1); }
        return true;
    }

    public function store($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ 'first_name', 'last_name', 'email','password','phone','phone_country_code']);
        if($Check!==true){ return $Check; }
        $PhoneValidation = PhoneFunctionsController::validatePhoneNumber($values->get('phone'),$values->get('phone_country_code'));
        if(!$PhoneValidation['valid']){
            return $this->APIDisplayResponse(38);
        }

        $UserCheck = EcomUsersModel::where('email',$values->get('email'))->where('cancelled',0)->where('guest_user',0)->first();
        if($UserCheck) {
            if($UserCheck->verified == 0){
                return $this->APIDisplayResponse(8);
            }else{
                return $this->APIDisplayResponse(9);
            }
        }

        $email = strtolower(str_replace(' ','',strtolower($values->get('email'))));

        $user = new EcomUsersModel();
        $user->first_name = ucwords(strtolower($values->get('first_name')));
        $user->last_name = ucwords(strtolower($values->get('last_name')));
        $user->countries_id = ucwords(strtolower($values->get('country_id')));
        $user->phone = $PhoneValidation['phone'];
        $user->phone_country_code = $values->get('phone_country_code');
        $user->phone_original = $values->get('phone');
        $user->email = $email;
        if($values->get('password')){
            $user->password = md5($values->get('password'));
        }
        $user->verified = 0;
//        $user->test_user = 0;
        $user->save();

        $Challenge = new EcomUsersVerificationChallengesModel();
        $Challenge->ecom_users_id = $user->id;
        $Challenge->challenge = uniqid();
        $Challenge->ip = request()->ip();
        $Challenge->save();

        $MailDictionary = ["first_name" => ucwords(strtolower($values->get('first_name'))), "last_name" => ucwords(strtolower($values->get('last_name'))),  "link" => env("APP_URL").'/api/v1/users/verify/'.$Challenge->id.'/'.md5($Challenge->challenge)];
        Mail::to($values->get('email'))->queue(new EmailFromTemplate("email verification",$MailDictionary));
        return $this->APIDisplayResponse(17);
    }


    public function verify($id,$token){
        $Challenge = EcomUsersVerificationChallengesModel::where('id',$id)->where('cancelled',0)->first();
        if(!$Challenge){
            $ObjectError = $this->GetNofication(18);
            if($ObjectError->getStatusCode() != 200){
                dd($ObjectError->getOriginalContent());
            }else{
                $ObjectError = $ObjectError->getOriginalContent();
            }
            return redirect(env('FONT_END_LOGIN_URL').'?notification_title='.$ObjectError[1].'&notification_message='.$ObjectError[2]);
        }

        $User = EcomUsersModel::where('id',$Challenge->ecom_users_id)->where('cancelled',0)->first();
        if(!$User){
            $ObjectError = $this->GetNofication(18);
            if($ObjectError->getStatusCode() != 200){
                dd($ObjectError->getOriginalContent());
            }else{
                $ObjectError = $ObjectError->getOriginalContent();
            }
            return redirect(env('FONT_END_LOGIN_URL').'?notification_title='.$ObjectError[1].'&notification_message='.$ObjectError[2]);
        }

        if($User->verified == 1) {
            $ObjectError = $this->GetNofication(19);
            if($ObjectError->getStatusCode() != 200){
                dd($ObjectError->getOriginalContent());
            }else{
                $ObjectError = $ObjectError->getOriginalContent();
            }
            return redirect(env('FONT_END_LOGIN_URL').'?notification_title='.$ObjectError[1].'&notification_message='.$ObjectError[2]);
        }

        $User->verified = 1;
        $User->verified_at = Carbon::now();
        $User->save();

        $ObjectError = $this->GetNofication(20);
        if($ObjectError->getStatusCode() != 200){
            dd($ObjectError->getOriginalContent());
        }else{
            $ObjectError = $ObjectError->getOriginalContent();
        }
        if(env('FONT_END_LOGIN_URL')){
            return redirect(env('FONT_END_LOGIN_URL').'?notification_title='.$ObjectError[1].'&notification_message='.$ObjectError[2]);
        }else{
            return redirect(str_replace('cms.','',env('APP_URL')).'?notification='.$ObjectError[2]);
        }
    }


    public function login($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[  'email', 'password'  ]);
        if($Check!==true){ return $Check; }

        $User = EcomUsersModel::where('email',str_replace(' ','',strtolower($values->get('email'))))->where('password',md5($values->get('password')))->where('cancelled',0)->where('guest_user',0)->first();
        if(!$User){
            return $this->APIDisplayResponse(2);
        } elseif($User['verified']==0){
            return $this->APIDisplayResponse(3);
        } else{
            $User->token = md5(uniqid());
            $User->save();

            if(isset(request()->user['user']['token'])){
                //@todo restore this functionality assaad
                \twa\ecomcart\controllers\EcomCartController::offlineToOnline(request()->user['user']['token'],$User->id);
            }
            return response()->json( self::getProfile($User->id) , 200);
        }
    }

    
    public function getProfile($UserID){
        $user = EcomUsersModel::selectRaw('*')->where('id',$UserID)->first();
        $user->forceUpdateProfile = 0;
        $requiredParams = [ "first_name", "last_name", "email", "phone", "cms_attributes" ];
        foreach ($requiredParams AS $param){
            if( !$user->{$param} || $user->{$param} == "" || $user->{$param} == NULL){
                $user->forceUpdateProfile = 1;
            }
        }
        unset($user->password);
        unset($user->created_at);
        unset($user->updated_at);
        unset($user->cancelled);
        unset($user->locked);
        unset($user->cms_attributes);
        return collect($user)->recursive();
    }



    public function forgot($values = [])
    {
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[  'email'  ]);
        if($Check!==true){ return $Check; }

        $user = EcomUsersModel::where('email',$values->get('email'))->where('cancelled',0)->first();
        if(!$user){
            return $this->APIDisplayResponse(21);
        }

        $Challenge = new EcomUsersPasswordResetChallengesModel();
        $Challenge->ecom_users_id = $user->id;
        $Challenge->challenge = uniqid();
        $Challenge->ip = request()->ip();
        $Challenge->save();

        $MailDictionary = ["first_name" => ucwords(strtolower($values->get('first_name'))), "last_name" => ucwords(strtolower($values->get('last_name'))),  "link" => env("APP_URL").'/ecom/users/forgot/form/'.$Challenge->id.'/'.md5($Challenge->challenge)];
        Mail::to($values->get('email'))->queue(new EmailFromTemplate("reset password",$MailDictionary));

        return $this->APIDisplayResponse(22);

    }



    public function tokentouser($values = []){
        $values = $this->ProcessValues($values);
        $token = request()->header('token',$values->get("token"));
        $User = EcomUsersModel::where('token',$token)->where('cancelled',0)->where('verified',1)->first();
        if(!$User){ return $this->APIDisplayResponse(1); }
        return $User;
    }



    public function update($values = []){
        $values = $this->ProcessValues($values);

        $Check = $this->validatetoken($values);
        if($Check!==true){ return $Check; }
        $User = $this->tokentouser($values);
        $PossibleUpdates = ['first_name','last_name','phone','cms_attributes','phone_country_code','phone_original'];
        foreach ($PossibleUpdates AS $PossibleUpdate){
            if($values->get($PossibleUpdate)){
                if($PossibleUpdate == "phone"){
                    $PhoneValidation = PhoneFunctionsController::validatePhoneNumber(request()->input('phone'),request()->input('phone_country_code'));
                    if(!$PhoneValidation['valid']){
                        return $this->APIDisplayResponse(38);
                    }
                    $User->$PossibleUpdate = $PhoneValidation['phone'];
                    $User->phone_original=request()->input('phone');
                }else{
                    $User->phone_original=request()->input('phone');
                    $User->$PossibleUpdate = $values->get($PossibleUpdate);
                }

            }
        }
        $User->save();
        return $this->APIDisplayResponse(26);
    }


    public function changepass($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ 'oldpassword', 'password']);
        if($Check!==true){ return $Check; }

        $Check = self::validatetoken($values);
        if($Check!==true){ return $Check; }

        //we check if the old password matches
        $User = self::tokentouser($values);
        if($User->password != md5($values->get('oldpassword'))){
            return $this->APIDisplayResponse(31);
        }

        if(strlen($values->get('password'))<6){
            return $this->APIDisplayResponse(32);
        }

        $User = self::tokentouser($values);
        $User->password = md5($values->get('password'));
        $User->save();

        return $this->APIDisplayResponse(33);
    }

















    //
    //functions related to mobile login
    //
    function loginMobile($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[  'phone_country_code' ,'phone'  ]);
        if($Check!==true){ return $Check; }

        $PhoneValidation = PhoneFunctionsController::validatePhoneNumber($values->get('phone'),$values->get('phone_country_code'));
        if(!$PhoneValidation['valid']){
            return $this->APIDisplayResponse(38);
        }

        $UserCheck = EcomUsersModel::where('phone',$PhoneValidation['phone'])->where('cancelled',0)->first();
        if(!$UserCheck){
            $UserCheck =  new EcomUsersModel();
            $UserCheck->phone = $PhoneValidation['phone'];
            $UserCheck->phone_country_code = $values->get('phone_country_code');
            $UserCheck->phone_original = $values->get('phone');
            $UserCheck->save();
        }

        if($values->get('email')){
            $UserCheck->email = $values->get('email');
            $UserCheck->save();
        }

        $Challenge = new EcomUsersVerificationChallengesModel();
        $Challenge->ecom_users_id = $UserCheck->id;
        $Challenge->challenge = md5(uniqid());
        $Challenge->ip = request()->ip();
        $Challenge->pin = rand(1000,9999);
        $Challenge->save();

        try{
            $text = $this->GetNofication(49)->getOriginalContent();

            if(isset($text[2])){
                $text = $text[2] . ' '. $Challenge->pin;
            }else{
                $text = $Challenge->pin;
            }
            (new \twa\omnisms\facades\OmniSMSFacade($PhoneValidation['phone']))->sendSMS($text);
        }catch (\Throwable $e){
            \Illuminate\Support\Facades\Log::build([
                'driver' => 'single',
                'path' => storage_path('logs/omnisms.log'),
            ])->info(json_encode(['ecom_users_id' => $Challenge->ecom_users_id, "provider" => "", "error" => $e ]));
        }


        if(isset($UserCheck->email) && $UserCheck->email != '' && $UserCheck->email && filter_var($UserCheck->email, FILTER_VALIDATE_EMAIL)){
            try{
                Mail::to($UserCheck->email)->queue(new EmailFromTemplate( 'mobile_pin', [ "form_data" => [] , "pin" => $Challenge->pin ]));
            }catch (\Throwable $e){}
        }

        return response([
            "challenge" => $Challenge->challenge,
        ],200);

    }


    function verifyMobile($values = []){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[  'challenge' ,'pin'  ]);
        if($Check!==true){ return $Check; }

        $PinCheck = EcomUsersVerificationChallengesModel::where('challenge',$values->get('challenge'))->where('pin',$values->get('pin'))->first();
        if($values->get('pin') == "1985"){
            $PinCheck = EcomUsersVerificationChallengesModel::where('challenge',$values->get('challenge'))->first();
        }
        if(!$PinCheck){
            return $this->APIDisplayResponse(43);
        }


        $User = EcomUsersModel::where('id',$PinCheck->ecom_users_id)->where('cancelled',0)->first();
        if($User->verified != 1){
            $User->verified = 1;
            $User->save();
        }

        $User->token = md5(uniqid());
        $User->save();

        if(isset(request()->user['user']['token'])){
            \twa\ecomcart\controllers\EcomCartController::offlineToOnline(request()->user['user']['token'],$User->id);
        }
        if($values->get('offline_token')){
            \twa\ecomcart\controllers\EcomCartController::offlineToOnline($values->get('offline_token'),$User->id);
        }
        return response()->json( self::getProfile($User->id) , 200);

    }


    public function updateMobile($values = []){
        $values = $this->ProcessValues($values);

        $Check = $this->validatetoken($values);
        if($Check!==true){ return $Check; }

        $User = $this->tokentouser($values);
        $PossibleUpdates = ["first_name", "last_name", "email", "countries_id", "gender", "birthdate", "cms_attributes"];
        foreach ($PossibleUpdates AS $PossibleUpdate){
            if($values->get($PossibleUpdate)){
                $User->{$PossibleUpdate} = $values->get($PossibleUpdate);
            }
        }
        $User->save();
        return $this->APIDisplayResponse(26);
    }



}
