<?php

namespace twa\ecomauth\controllers;

use App\Http\Controllers\Controller;
use twa\apilibs\traits\APITrait;
use Barryvdh\Debugbar\Facade as Debugbar;
use twa\ecomgeneral\models\EcomUsersModel;


class EcomUsersController extends Controller
{
    use APITrait;

    public function login(){
        $values = $this->ProcessValues($values);
        $Check = $this->ProcessRequired($values,[ 'email', 'password' ]);
        if($Check!==true){ return $Check; }

        $User = \twa\ecomgeneral\models\EcomUsersModel::where('email',str_replace(' ','',strtolower(request()->input('email'))))->where('password',md5(request()->input('password')))->where('cancelled',0)->first();
        if(!$User){
            return API::APIDisplayResponse(2);
        } elseif($User['verified']==0){
            return API::APIDisplayResponse(3);
        } else{
            $User->token = md5(uniqid());
            $User->save();

            if(request()->input('offline_token')){
                try{
                    \twa\ecomcart\controllers\EcomCartController::offlineToOnline(request()->input('offline_token'),$User->id);
                }catch (\Throwable $e){}
            }
            return response()->json( self::getProfile($User->id) , 200);
        }
    }



}
