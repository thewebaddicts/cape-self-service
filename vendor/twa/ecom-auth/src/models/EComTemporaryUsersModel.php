<?php

namespace twa\ecomauth\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EComTemporaryUsersModel extends Model
{
    use HasFactory;
    protected $table = "ecom_users_temporary";
    protected $casts = [
        'cart' => 'array',
        'favorites' => 'array',
    ];
}
