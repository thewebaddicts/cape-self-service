<?php

namespace twa\ecomauth\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EcomUsersVerificationChallengesModel extends Model
{
    use HasFactory;
    protected $table = "ecom_users_challenges_verification";
}
