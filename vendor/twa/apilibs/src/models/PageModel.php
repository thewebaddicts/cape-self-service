<?php

namespace twa\apilibs\models;

use Illuminate\Database\Eloquent\Model;

class PageModel extends Model
{
    protected $table = "00_cms_structure_entities";
}
