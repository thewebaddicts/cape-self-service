<?php

namespace twa\apilibs\models;

use Illuminate\Database\Eloquent\Model;

class NotificationModel extends Model
{
    protected $table = "notifications";
}
