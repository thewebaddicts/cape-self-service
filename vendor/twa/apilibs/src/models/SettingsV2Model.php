<?php

namespace twa\apilibs\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SettingsV2Model extends Model
{
    use HasFactory;
    protected $table = "settings_v2";
}
