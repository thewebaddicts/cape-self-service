<?php

namespace twa\apilibs\models;

use Illuminate\Database\Eloquent\Model;

class SEOByRouteModel extends Model
{
    protected $table = "00_cms_seo_by_route";
}
