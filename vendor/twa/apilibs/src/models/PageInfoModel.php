<?php

namespace twa\apilibs\models;

use Illuminate\Database\Eloquent\Model;

class PageInfoModel extends Model
{
    protected $table = "00_cms_structure_entities_fields";

    public function getContainerClassAttribute($value){

        if(!$value || $value==NULL){ $value = "eight"; }

        if(!str_contains($value,'column')){ $value = 'column '.$value; }
        if(!str_contains($value,'wide')){ $value = $value.' wide'; }

        $value = str_replace(['s12','s11','s10','s9','s8','s7','s6','s5','s4','s3','s2','s1'],['sixteen','fourteen','thirteen','twelve','ten','nine','eight','seven','four','four','two','one'],$value);
        return $value;
    }


    public function getValueFromQueryAttribute($value){
        if(($value == "" || $value == NULL) && $this->name == "filter_field_value" && env('PERMISSION_QUERY_FIELD_1_QUERY')){
            return strtolower(env('PERMISSION_QUERY_FIELD_1_QUERY'));
        }else{
            return $value;
        }
    }

    public function getQueryValueAttribute($value){
        if(($value == "" || $value == NULL) && $this->name == "filter_field_value" && env('PERMISSION_QUERY_FIELD_1_VALUE')){
            return strtolower(env('PERMISSION_QUERY_FIELD_1_VALUE'));
        }else{
            return $value;
        }
    }
}
