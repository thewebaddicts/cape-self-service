<?php

namespace twa\apilibs\models;

use Illuminate\Database\Eloquent\Model;

class SEOSettingsModel extends Model
{
    protected $table = "00_cms_seo_settings";
}
