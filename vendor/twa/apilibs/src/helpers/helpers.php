<?php
use Illuminate\Support\Arr;

function getter($entity){
    if(strlen($entity) <= 3){ $entity = (int)$entity; }
    if(gettype($entity)=="integer"){
    }else{
        //we try to find the table that matches the $entityID
        $table = \twa\apilibs\models\PageModel::where('main_table',$entity)->where('cancelled',0)->first();
        if(!$table){ dd(["status" => "error", "message" => "the table '$entity' you are trying to query from was not found" ]); }
        $entity = $table->id;
    }
    return new \twa\apilibs\helpers\getterHelper($entity);
}


function lang(){
    return new \twa\apilibs\helpers\langHelpers();
}

?>
