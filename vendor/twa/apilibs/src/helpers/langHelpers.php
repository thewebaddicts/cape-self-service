<?php
namespace twa\apilibs\helpers;

use twa\ecomgeneral\models\EcomStoreModel;
use twa\ecomprofile\models\EcomCountriesModel;

class langHelpers
{

    private $languages;

    function __construct(){
        if(env('CMS_LANGUAGES')){
            $this->languages = collect(json_decode(str_replace("'",'"',env('CMS_LANGUAGES'))));
        }else{
            $this->languages = collect([ 'language' => 'English','prefix' => 'en','direction' => 'ltr' ]);
        }
    }


    public function list(){
        return $this->languages;
    }



    public function current(){
        $ActiveLanguage = $this->languages->where('prefix',app()->getLocale())->values()->all();
        if(count($ActiveLanguage) == 0){
            return collect([ 'language' => 'English','prefix' => 'en','direction' => 'ltr' ]);
        }else{
            return collect($ActiveLanguage[0]);
        }
        return $ActiveLanguage;
    }

    public function currentDirection(){
        $ActiveLanguage = self::current();
        if(isset($ActiveLanguage['direction']) && ($ActiveLanguage['direction'] == "ltr" || $ActiveLanguage['direction'] == "rtl")) {
            return $ActiveLanguage['direction'];
        }else{
            return "ltr";
        }
    }
}
