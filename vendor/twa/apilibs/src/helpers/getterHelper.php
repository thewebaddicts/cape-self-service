<?php
namespace twa\apilibs\helpers;

use twa\ecomgeneral\models\EcomStoreModel;
use twa\ecomprofile\models\EcomCountriesModel;

class getterHelper
{

    private $parameters;
    private $entity;
    private $lang = NULL;
    private $single = false;

    function __construct($entity){
        if(isset(request()->store) && isset(request()->store['id'])){
            $this->parameters["store"] = request()->store['id'];
        }
        $this->entity = $entity;
    }


    public function orCondition($key,$operand,$value = NULL){
        if(!$value){ $value = $operand; $operand = NULL;}
        if($key == "id"){ $this->single = true; }
        $this->parameters['orcolumn'][] = $key;
        $this->parameters['oroperand'][] = $operand;
        $this->parameters['orvalue'][] = $value;
        return $this;
    }

    public function condition($key,$operand,$value = NULL){
        if(!$value){ $value = $operand; $operand = NULL;}
        if($key == "id"){ $this->single = true; }
        $this->parameters['column'][] = $key;
        $this->parameters['operand'][] = $operand;
        $this->parameters['value'][] = $value;
        return $this;
    }

    public function limit($value){
        $this->parameters['limit'] = $value;
        return $this;
    }
    public function setLang($value){
        $this->lang = $value;
        return $this;
    }

    public function orderBy($column,$direction = "ASC"){
        $this->parameters['orderByColumn'] = $column;
        $this->parameters['orderByDirection'] = $direction;
        return $this;
    }

    public function get($limit = NULL){
        if(!$this->lang){
            $this->lang = app()->getLocale();
        }
        if($limit){
            $this->parameters['limit'] = $limit;
        }
        $response =  (new \twa\apilibs\controllers\GetterController)->get($this->entity ,$this->parameters, $this->lang );
        if($response->getStatusCode()==200){
            $return = $response->getOriginalContent();
            if($this->single && count($return)==1){
                return $return[0];
            }else{
                return $response->getOriginalContent();;
            }
        }else{
            dump($response->getOriginalContent());
        }
    }

    public function test(){
        return $this->parameters;
    }
}
