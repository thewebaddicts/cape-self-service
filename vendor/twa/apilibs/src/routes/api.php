<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => [ \twa\apilibs\middleware\TWAApiLocaleMiddleware::class, \twa\ecomgeneral\middleware\storeAPIRouteDataMiddleware::class ] ,'prefix'=>'/api/v1/ecom'], function(){
    Route::group(['prefix'=>'/sections'], function() {
        Route::post('/get', [ twa\ecomproducts\controllers\EcomSectionsController::class , 'get' ]);
        Route::post('/list', [ twa\ecomproducts\controllers\EcomSectionsController::class , 'list' ]);
    });
    Route::group(['prefix'=>'/products'], function() {
            Route::any('/query', [ twa\ecomproducts\controllers\EcomProductsController::class , 'query' ]);
//            Route::any('/query/debug', function () { return \App\Http\Controllers\EcomProductController::debug(); });
//            Route::any('/searches', function () { return \App\Http\Controllers\EcomProductController::popularSearches(); });
//            Route::post('/variations', function () { return \App\Http\Controllers\EcomProductController::getVariations(); });
        Route::group(['prefix'=>'/collections'], function() {
            Route::post('/get', [ twa\ecomproducts\controllers\EcomCollectionsController::class , 'get' ]);
            Route::post('/list', [ twa\ecomproducts\controllers\EcomCollectionsController::class , 'list' ]);
        });
    });
    Route::post('/notifications/get/{id}', [ twa\ecomproducts\controllers\EcomCollectionsController::class , 'get' ]);
});

Route::group(['middleware' => []  ,'prefix'=>'/api/v1'], function(){
    Route::group(['prefix'=>'/utilities'], function() {
        Route::post('/getter/{id}', [ \twa\apilibs\controllers\GetterController::class, 'get' ]);
        Route::post('/getter/{id}/{lang}', function($id,$lang){ return (new \twa\apilibs\controllers\GetterController)->get($id,[],$lang); });
    });
});


