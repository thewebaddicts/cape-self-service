<?php

namespace twa\apilibs\providers;

use Illuminate\Support\ServiceProvider;

Class WebsiteServiceProvider extends ServiceProvider{
    public function boot(){
        $this->publishes([
            __DIR__.'/../config/apilibs.php' => config_path('apilibs.php'),
        ], 'config');

        self::TWAExtendCollection();
    }

    public function register(){
        include_once(__DIR__.'/../helpers/helpers.php');
        $this->loadRoutesFrom(__DIR__.'/../routes/api.php');
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->app->make('twa\apilibs\apilib');
    }


    public function TWAExtendCollection(){
        \Illuminate\Support\Collection::macro('recursive', function () {
            return $this->whenNotEmpty($recursive = function ($item) use (&$recursive) {
                if (is_array($item)) {
                    return $recursive(new static($item));
                } elseif ($item instanceof Collection) {
                    $item->transform(static function ($collection, $key) use ($recursive, $item) {
                        return $item->{$key} = $recursive($collection);
                    });
                } elseif (is_object($item)) {
                    foreach ($item as $key => &$val) {
                        $item->{$key} = $recursive($val);
                    }
                }
                return $item;
            });
        });
    }

}
