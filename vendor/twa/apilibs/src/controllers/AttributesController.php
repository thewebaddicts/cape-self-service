<?php

namespace twa\apilibs\controllers;

use Illuminate\Support\Facades\DB;
use Throwable;
use Propaganistas\LaravelPhone\PhoneNumber;


class AttributesController
{
    public static function ProcessCMSAttributesModel($value){
        try{
            $return = json_decode($value,1);
        }catch (\Throwable $e){
            $return =  $value;
        }
        if(is_array($return)){
            foreach($return AS $key => $item){
                try{
                    if(json_decode($item,1)){
                        $return[$key] = json_decode($item,1);
                    }else{
                        $return[$key] =  $item;
                    }

                }catch (\Throwable $e){
                    $return[$key] =  $item;
                }
            }
        }
        return $return;
    }
}
