<?php

namespace twa\apilibs\controllers;

use App\Http\Controllers\Controller;
use twa\apilibs\models\SettingsV2Model;

class SettingsController
{
    public $values;

    public function __construct(){
        $settings = SettingsV2Model::where('cancelled',0)->get();
        $array = [];
        foreach ($settings AS $key => $setting){
            $array[$setting->key] = collect($setting)->toArray();
            if($setting->type == "image"){
                $array[$setting->key]["value"] = json_decode($array[$setting->key]["value"],1);
                if($array[$setting->key]["value"]!= NULL && array_key_exists("link",$array[$setting->key]["value"])){
                    $array[$setting->key]["value"]["link"] = env('DATA_URL')."/".$array[$setting->key]["value"]["link"];
                }
            }
        }
        $this->values = collect($array)->recursive();
    }

    public function get($value = false, $default = NULL){
        if(!$value){ return $this->values; }else if(isset($this->values[$value])){ return $this->values[$value]["value"]; }else{ return $default; }
    }

    public function desribe($value){
        if(isset($this->values[$value])){ return $this->values[$value]["description"]; }else{ return false; }
    }
}
