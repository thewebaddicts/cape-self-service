<?php

namespace twa\apilibs\controllers;

use App\Http\Controllers\Controller;

class ValuesProcessingController
{
    public $values;

    public function __construct($values){
        $this->values = collect($values);
    }

    public function get($value = false, $default = NULL){
        if(!$value){ return $this->values; }else if(isset($this->values[$value])){ return $this->values[$value]; }else{ return $default; }
    }
    public function add($key, $value){
        $this->values[$key] = $value;
        return $this;
    }
}
