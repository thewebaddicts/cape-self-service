<?php

namespace twa\apilibs\controllers;

use Illuminate\Support\Facades\DB;
use Throwable;
use Propaganistas\LaravelPhone\PhoneNumber;


class PhoneFunctionsController
{
    public static function validatePhoneNumber($phone, $country_code)
    {
        $valid = true;
        try {
            $phone_number = (string)PhoneNumber::make($phone)->ofCountry($country_code);
        } catch (\Throwable $th) {
            $phone_number = false;
            $valid = false;
        }
        if (!strpos(' ' . $phone_number, '+') && $valid) {
            $valid = false;
        }
        return [
            'valid' => $valid,
            'phone' => $phone_number
        ];
    }


    public static function getCountry($phone)
    {
        try {
            return PhoneNumber::make($phone, 'LB')->getCountry();
        } catch (Throwable $e) {
            return NULL;
        }
    }

    public static function formatNational($phone, $country)
    {
        try {
            return PhoneNumber::make($phone, $country)->formatForMobileDialingInCountry($country);
        } catch (Throwable $e) {
            return NULL;
        }

    }
}
