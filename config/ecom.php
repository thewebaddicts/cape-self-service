<?php

return [

    'default_landing' => [
        "mode" => "route",  //this can be either view or route
//        "view" => "pages.select-store",
        "route" => "/stores/select",
    ],


    'menus' => [
         ['name' => 'footerSocialMenu', 'parameters' => ['menulabel' => 'Footer Social', 'listTag' => 'ul', 'listClass' => 'footer-menu2', 'labelClass' => 'label', 'itemClass' => 'item', 'encloseLI' => true, 'encloseLIClass' => 'encloureClass', 'ecomLinkStructure' => env('APP_URL') . '/{store_prefix}/{language_prefix}/products/list/{id}/{slug}', 'nonEcomLinkPrefix' => '/{store_prefix}/{language_prefix}']],
        ['name' => 'mainMenu', 'parameters' => ['menulabel' => 'Main Menu', 'listTag' => 'ul', 'listClass' => 'main-menu', 'labelClass' => 'label', 'itemClass' => 'item', 'encloseLI' => true, 'encloseLIClass' => 'encloureClass', 'ecomLinkStructure' => env('APP_URL') . '/{store_prefix}/{language_prefix}/products/list/{id}/{slug}', 'nonEcomLinkPrefix' => '/{store_prefix}/{language_prefix}']],
         ['name' => 'footerMenu', 'parameters' => ['menulabel' => 'Footer Menu', 'listTag' => 'ul', 'listClass' => 'footer-menu', 'labelClass' => 'label', 'itemClass' => 'item', 'encloseLI' => true, 'encloseLIClass' => 'encloureClass', 'ecomLinkStructure' => env('APP_URL') . '/{store_prefix}/{language_prefix}/products/list/{id}/{slug}', 'nonEcomLinkPrefix' => '/{store_prefix}/{language_prefix}']],
    ],
    //please fill your route names for the following pages
    'default_routes' =>
        [
            //authentication routes
            'forgot' => "login",
            'login' => "login",
            'register' => "login",
            //user profile routes
            'addresses' => "addresses"
        ]
    ,
    'authentication' =>
        [
            'login' => "login",
        ]
    ,

    'default_views' =>
        [
            'product_details' => "pages.product",
            'product' => "pages.products.product",
            'address_listing' => "pages.account",
            'address_form' => "pages.account",
            'address_states' => "addresses.states",
            'address_cities' => "addresses.cities",
            'client_receipts' => 'ecom.receipts.client_default',
            'email_template'=>'ecom.emails.email-template'
        ],

    //endpoints are the basepath for the apis in case we are using microservices & APIs
    'endpoints' => [
        'storage_url' => env('DATA_URL',env('BASE_DATA_URL',env('APP_URL'))),
        'utilities' => env('API_UTILITIES',env('API_URL')),
        'authentication' => env('API_AUTHENTICATION',env('API_URL')),
        'products' => env('API_PRODUCTS',env('API_URL')),
        'purchases' => env('API_PURCHASE',env('API_URL')),
        'profile' => env('API_PROFILE',env('API_URL')),
    ],

    'caching_tags' => [
        "*/products/query" => "product"
    ],

];
