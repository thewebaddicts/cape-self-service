<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\AdressesController;
use App\Http\Controllers\DashBoardController;
use App\Http\Controllers\EditStagingTable;
use App\Http\Controllers\HomePageController;
use App\Http\Controllers\LoginPageController;
use App\Http\Controllers\PersonelInformationController;
use App\Http\Controllers\PoliciesController;
use Illuminate\Support\Facades\Route;
use twa\ecomauth\facades\EcomUsersRegistrationFacade;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('pages.underconstruction');
//})->name('construction');
Route::get('/login', [LoginPageController::class, 'loginPage'])->name('login');
Route::post('/loginto', [EcomUsersRegistrationFacade::class, 'login'])->name('loginto');
Route::get('/logout', [EcomUsersRegistrationFacade::class, 'logout'])->name('logout');
Route::post('/forgot', [EcomUsersRegistrationFacade::class, 'forgot'])->name('forgot');

//Route::get('/DashBoard', function () {
//    return view('pages.dashboard');
//})->name('dashboard');

Route::get('/', [DashBoardController::class, 'dashboardInfo'])->name('dashboard')->middleware(App\Http\Middleware\AuthCheck::class);

//Route::get('/Policies', function () {
//    return view('pages.policies');
//})->name('policies');

Route::get('/Policies', [PoliciesController::class, 'getPoliciesData'])->name('policies');
Route::post('/Policies/dlt', [PoliciesController::class, 'dltPoliciesData'])->name('policies_dlt');

//Route::get('/Addresses', function () {
//    return view('pages.address');
//})->name('address');

Route::get('/Addresses', [AdressesController::class, 'getAddresses'])->name('address');
Route::post('/Addresses/post', [AdressesController::class, 'updateAddress'])->name('address_post');
Route::post('/Addresses/dlt', [AdressesController::class, 'deleteAddress'])->name('address_dlt');

//Route::get('/Account', function () {
//    return view('pages.account');
//})->name('account');

Route::get('/Account', [AccountController::class, 'accountPage'])->name('account');
Route::post('/Account/post', [AccountController::class, 'updateProfile'])->name('account_post');

Route::get('/test', [EditStagingTable::class, 'insertIntoDatabase'])->name('test');




